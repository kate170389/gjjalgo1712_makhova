package com.getjavajob.training.util;

import java.util.Scanner;

import static java.lang.System.out;

public class DataInput {
    public static int inputInt() {
        Scanner input = new Scanner(System.in, "UTF-8");
        while (!input.hasNextInt()) {
            out.println("Your number not valid, try again!");
            input.next();
        }
        return input.nextInt();
    }

    public static int inputInt(int from) {
        int data = inputInt();
        while (data <= from) {
            out.println("Your number not valid, try again!");
            data = inputInt();
        }
        return data;
    }

    public static int inputInt(int from, int to) {
        int data = inputInt();
        while (data <= from || data >= to) {
            out.println("Your number not valid, try again!");
            data = inputInt();
        }
        return data;
    }

    public static double inputDouble() {
        Scanner input = new Scanner(System.in, "UTF-8");
        while (!input.hasNextDouble()) {
            out.println("Your number not valid, try again!");
            input.next();
        }
        return input.nextDouble();
    }

    public static double inputDouble(int from) {
        double data = inputDouble();
        while (data <= from) {
            out.println("Your number not valid, try again!");
            data = inputDouble();
        }
        return data;
    }

    public static String inputString() {
        Scanner input = new Scanner(System.in, "UTF-8");
        String str = input.nextLine();
        while (str.equals("")) {
            out.println("Your data not valid, try again!");
            str = input.nextLine();
        }
        return str;
    }

    public static String inputWord() {
        Scanner input = new Scanner(System.in, "UTF-8");
        String str = input.nextLine();
        while (!str.matches("[a-zA-Z0-9]+")) {
            out.println("Your data not valid, try again!");
            str = input.nextLine();
        }
        return str;
    }

    public static double[] inputDoubleArray(int length) {
        double[] arrayDouble = new double[length];
        Scanner input = new Scanner(System.in, "UTF-8");
        for (int i = 0; i < length; i++) {
            out.printf("The %d element: ", i + 1);
            while (!input.hasNextDouble()) {
                out.printf("Your data not valid, try again!");
                input.next();
            }
            arrayDouble[i] = input.nextDouble();
        }
        return arrayDouble;
    }

    public static int[] inputIntArray(int length) {
        int[] arrayInt = new int[length];
        Scanner input = new Scanner(System.in, "UTF-8");
        for (int i = 0; i < length; i++) {
            out.printf("The %d element: ", i + 1);
            while (!input.hasNextInt()) {
                out.printf("Your data not valid, try again!");
                input.next();
            }
            arrayInt[i] = input.nextInt();
        }
        return arrayInt;
    }

    public static int[][] inputIntTwoDimensionArray(int lines, int columns) {
        int[][] arrayInt = new int[lines][columns];
        for (int i = 0; i < arrayInt.length; i++) {
            out.printf("Enter %d elements in %d line: ", columns, i + 1);
            out.println();
            arrayInt[i] = inputIntArray(arrayInt[i].length);
        }
        return arrayInt;
    }
}