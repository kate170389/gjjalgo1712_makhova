package com.getjavajob.training.util;

public class StopWatch {
    static long startTime;

    public static void start() {
        startTime = System.currentTimeMillis();
    }

    public static long getElapsedTime() {
        return System.currentTimeMillis() - startTime;
    }
}