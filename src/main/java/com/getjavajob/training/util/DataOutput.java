package com.getjavajob.training.util;

import static java.lang.System.out;

public class DataOutput {
    public static void outputIntArray(int[] intArray) {
        for (int i : intArray) {
            out.printf("%4d", i);
        }
    }

    public static void outputDoubleArray(double[] intArray) {
        for (double i : intArray) {
            out.print(i + " ");
        }
    }

    public static void outputIntTwoDimensionalArray(int[][] intArray) {
        for (int[] i : intArray) {
            for (int j : i) {
                out.printf("%4d", j);
            }
            out.println();
        }
    }
}