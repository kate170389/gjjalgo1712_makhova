package com.getjavajob.training.util;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;

import static com.getjavajob.training.util.DataOutput.outputIntTwoDimensionalArray;
import static java.lang.System.out;

public class Assert {
    public static void assertEquals(String testName, int expected, int actual) {
        if (expected == actual) {
            out.println(testName + " passed");
        } else {
            out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, double expected, double actual) {
        if (expected == actual) {
            out.println(testName + " passed");
        } else {
            out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, double expected, double actual, String doubleFormat) {
        DecimalFormat formattedDouble = new DecimalFormat(doubleFormat);
        if (formattedDouble.format(expected).equals(formattedDouble.format(actual))) {
            out.println(testName + " passed");
        } else {
            out.println(testName + " failed: expected " + formattedDouble.format(expected) + ", actual " +
                    formattedDouble.format(actual));
        }
    }

    public static void assertEquals(String testName, boolean expected, boolean actual) {
        if (expected == actual) {
            out.println(testName + " passed");
        } else {
            out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, String expected, String actual) {
        if (expected == null && actual == null) {
            out.println(testName + " passed");
        } else {
            if (expected != null && actual != null && expected.equals(actual)) {
                out.println(testName + " passed");
            } else {
                out.println(testName + " failed: expected " + expected + ", actual " + actual);
            }
        }
    }

    public static void assertEquals(String testName, int[] expected, int[] actual) {
        if (Arrays.equals(expected, actual)) {
            out.println(testName + " passed");
        } else {
            out.println(testName + " failed: expected " + Arrays.toString(expected) + ", actual " +
                    Arrays.toString(actual));
        }
    }

    public static void assertEquals(String testName, byte[] expected, byte[] actual) {
        if (Arrays.equals(expected, actual)) {
            out.println(testName + " passed");
        } else {
            out.println(testName + " failed: expected " + Arrays.toString(expected) + ", actual " +
                    Arrays.toString(actual));
        }
    }

    public static void assertEquals(String testName, double[] expected, double[] actual, String doubleFormat) {
        DecimalFormat formattedDouble = new DecimalFormat(doubleFormat);
        String[] actualString = new String[actual.length];
        String[] expectedString = new String[expected.length];
        for (int i = 0; i < actual.length; i++) {
            expectedString[i] = formattedDouble.format(expected[i]);
            actualString[i] = formattedDouble.format(actual[i]);
        }
        if (expectedString.length == actualString.length) {
            if (Arrays.equals(expectedString, actualString)) {
                out.println(testName + " passed");
            } else {
                out.println(testName + " failed:" + "\n" + " expected " + Arrays.toString(expectedString) + "\n" +
                        ", actual " + Arrays.toString(actualString));
            }
        } else {
            out.println(testName + " failed:" + "\n" + "expected " + Arrays.toString(expectedString) + "\n" +
                    ", actual " + Arrays.toString(actualString));
        }
    }

    public static void assertEquals(String testName, double[] expected, double[] actual) {
        if (Arrays.equals(expected, actual)) {
            out.println(testName + " passed");
        } else {
            out.println(testName + " failed: expected " + Arrays.toString(expected) + ", actual " +
                    Arrays.toString(actual));
        }
    }

    public static void assertEquals(String testName, int[][] expected, int[][] actual) {
        if (Arrays.deepEquals(expected, actual)) {
            out.println(testName + " passed");
        } else {
            out.println(testName + " failed: expected ");
            outputIntTwoDimensionalArray(expected);
            out.println(", actual ");
            outputIntTwoDimensionalArray(actual);
        }
    }

    public static void assertEquals(String testName, char expected, char actual) {
        if (expected == actual) {
            out.println(testName + " passed");
        } else {
            out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, List expected, List actual) {
        if (expected.equals(actual)) {
            out.println(testName + " passed");
        } else {
            out.println(testName + " failed: expected ");
            for (int j = 0; j < expected.size(); j++) {
                out.print(expected.get(j).toString());
                out.println();
            }
            out.println("actual:");
            for (int j = 0; j < actual.size(); j++) {
                out.print(actual.get(j).toString());
                out.println();
            }
        }
    }

    public static void assertEquals(String testName, Object[] expected, Object[] actual) {
        if (Arrays.equals(expected, actual)) {
            out.println(testName + " passed");
        } else {
            out.println(testName + " failed: expected " + Arrays.toString(expected) + ", actual " +
                    Arrays.toString(actual));
        }
    }

    public static void assertEquals(String testName, Object expected, Object actual) {
        if (expected == null && actual == null) {
            out.println(testName + " passed");
        } else {
            if (expected != null && actual != null && expected.equals(actual)) {
                out.println(testName + " passed");
            } else {
                out.println(testName + " failed: expected " + expected.toString() + ", actual " +
                        actual.toString());
            }
        }
    }

    public static void fail(String msg) {
        throw new AssertionError(msg);
    }
}