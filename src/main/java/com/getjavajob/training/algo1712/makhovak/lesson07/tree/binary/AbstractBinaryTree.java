package com.getjavajob.training.algo1712.makhovak.lesson07.tree.binary;

import com.getjavajob.training.algo1712.makhovak.lesson07.tree.AbstractTree;
import com.getjavajob.training.algo1712.makhovak.lesson07.tree.Node;

import java.util.ArrayList;
import java.util.Collection;

public abstract class AbstractBinaryTree<E> extends AbstractTree<E> implements BinaryTree<E> {
    @Override
    public Node<E> sibling(Node<E> n) throws IllegalArgumentException {
        if (n == null || n.equals(root())) {
            return null;
        }
        return n == right(parent(n)) ? left(parent(n)) : right(parent(n));
    }

    @Override
    public Collection<Node<E>> children(Node<E> n) throws IllegalArgumentException {
        Collection<Node<E>> collection = new ArrayList<>();
        if (left(n) != null) {
            collection.add(left(n));
        }
        if (right(n) != null) {
            collection.add(right(n));
        }
        return collection;
    }

    @Override
    public int childrenNumber(Node<E> n) throws IllegalArgumentException {
        return children(n).size();
    }

    /**
     * @return an iterable collection of nodes of the tree in inorder
     */
    public Collection<Node<E>> inOrder() {
        Collection<Node<E>> inOrderCollection = new ArrayList<>();
        inOrderNode(inOrderCollection, root());
        return inOrderCollection;
    }

    private void inOrderNode(Collection<Node<E>> collection, Node<E> node) {
        if (node != null) {
            inOrderNode(collection, left(node));
            collection.add(node);
            inOrderNode(collection, right(node));
        }
    }
}