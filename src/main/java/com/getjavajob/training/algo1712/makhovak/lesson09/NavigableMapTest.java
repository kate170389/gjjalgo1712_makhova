package com.getjavajob.training.algo1712.makhovak.lesson09;

import java.util.*;

import static com.getjavajob.training.util.Assert.assertEquals;

public class NavigableMapTest {
    public static void main(String[] args) {
        testCeilingEntry();
        testCeilingKey();
        testDescendingKeySet();
        testDescendingMap();
        testFirstEntry();
        testFloorEntry();
        testFloorKey();
        testHeadMap();
        testHigherEntry();
        testHigherKey();
        testLastEntry();
        testLowerEntry();
        testLowerKey();
        testNavigableKeySet();
        testPollFirstEntry();
        testPollLastEntry();
        testSubMap();
        testTailMap();
    }

    private static void testTailMap() {
        NavigableMap<Integer, String> tree = new TreeMap<>();
        tree.put(5, "a");
        tree.put(10, "d");
        tree.put(12, "e");
        tree.put(1, "a");
        NavigableMap<Integer, String> expected = new TreeMap<>();
        expected.put(10, "d");
        expected.put(12, "e");
        assertEquals("NavigableMapTest.testTailMapNotInclude", expected, tree.tailMap(5, false));
        expected.put(5, "a");
        assertEquals("NavigableMapTest.testTailMapInclude", expected, tree.tailMap(5, true));
        SortedMap<Integer, String> expectedMap = new TreeMap<>();
        expectedMap.put(10, "d");
        expectedMap.put(5, "a");
        expectedMap.put(12, "e");
        assertEquals("NavigableMapTest.testTailMap", expectedMap, tree.tailMap(5));
    }

    private static void testSubMap() {
        NavigableMap<Integer, String> tree = new TreeMap<>();
        tree.put(5, "a");
        tree.put(10, "d");
        tree.put(12, "e");
        tree.put(1, "a");
        NavigableMap<Integer, String> expected = new TreeMap<>();
        expected.put(10, "d");
        assertEquals("NavigableMapTest.testSubMapNotInclude", expected, tree.subMap(5, false, 12, false));
        expected.put(5, "a");
        expected.put(12, "e");
        assertEquals("NavigableMapTest.testSubMapInclude", expected, tree.subMap(5, true, 12, true));
        SortedMap<Integer, String> expectedMap = new TreeMap<>();
        expectedMap.put(10, "d");
        expectedMap.put(5, "a");
        assertEquals("NavigableMapTest.testSubMap", expectedMap, tree.subMap(5, 12));
    }

    private static void testPollLastEntry() {
        NavigableMap<Integer, String> tree = new TreeMap<>();
        tree.put(5, "a");
        tree.put(10, "d");
        tree.put(12, "e");
        tree.put(1, "a");
        Map.Entry<Integer, String> expectedElement = tree.ceilingEntry(12);
        NavigableMap<Integer, String> expected = new TreeMap<>();
        expected.put(5, "a");
        expected.put(10, "d");
        expected.put(1, "a");
        assertEquals("NavigableMapTest.testPollLastEntry", expectedElement, tree.pollLastEntry());
        assertEquals("NavigableMapTest.testPollLastEntry", expected, tree);
    }

    private static void testPollFirstEntry() {
        NavigableMap<Integer, String> tree = new TreeMap<>();
        tree.put(5, "a");
        tree.put(10, "d");
        tree.put(12, "e");
        tree.put(1, "a");
        Map.Entry<Integer, String> expectedElement = tree.ceilingEntry(1);
        NavigableMap<Integer, String> expected = new TreeMap<>();
        expected.put(5, "a");
        expected.put(10, "d");
        expected.put(12, "e");
        assertEquals("NavigableMapTest.testPollFirstEntry", expectedElement, tree.pollFirstEntry());
        assertEquals("NavigableMapTest.testPollFirstEntry", expected, tree);
    }

    private static void testNavigableKeySet() {
        NavigableMap<Integer, String> tree = new TreeMap<>();
        tree.put(5, "a");
        tree.put(10, "d");
        tree.put(12, "e");
        tree.put(1, "a");
        NavigableSet<Integer> expected = new TreeSet<>();
        expected.add(1);
        expected.add(5);
        expected.add(10);
        expected.add(12);
        assertEquals("NavigableMapTest.testNavigableKeySet", expected, tree.navigableKeySet());
    }

    private static void testLowerKey() {
        NavigableMap<Integer, String> tree = new TreeMap<>();
        tree.put(5, "a");
        tree.put(10, "d");
        tree.put(12, "e");
        tree.put(1, "a");
        tree.put(40, "a");
        tree.put(3, "a");
        assertEquals("NavigableMapTest.testLoweKey", (Integer) 10, tree.lowerKey(12));
        assertEquals("NavigableMapTest.testLastKeyNull", null, tree.lowerKey(1));
    }

    private static void testLowerEntry() {
        NavigableMap<Integer, String> tree = new TreeMap<>();
        tree.put(5, "a");
        tree.put(10, "d");
        tree.put(12, "e");
        tree.put(1, "a");
        tree.put(40, "a");
        tree.put(3, "a");
        Map.Entry<Integer, String> expected = tree.ceilingEntry(10);
        assertEquals("NavigableMapTest.testLowerEntry", expected, tree.lowerEntry(12));
        assertEquals("NavigableMapTest.testLastEntryNull", null, tree.lowerEntry(1));
    }

    private static void testLastEntry() {
        NavigableMap<Integer, String> tree = new TreeMap<>();
        assertEquals("NavigableMapTest.testLastEntryNull", null, tree.lastEntry());
        tree.put(5, "a");
        tree.put(10, "d");
        tree.put(12, "e");
        tree.put(1, "a");
        tree.put(40, "a");
        tree.put(3, "a");
        Map.Entry<Integer, String> expected = tree.ceilingEntry(40);
        assertEquals("NavigableMapTest.testLastEntry", expected, tree.lastEntry());
    }

    private static void testHigherKey() {
        NavigableMap<Integer, String> tree = new TreeMap<>();
        tree.put(5, "a");
        tree.put(10, "d");
        tree.put(12, "e");
        tree.put(1, "a");
        tree.put(40, "a");
        tree.put(3, "a");
        assertEquals("NavigableMapTest.testHigherKey", (Integer) 12, tree.higherKey(10));
        assertEquals("NavigableMapTest.testHigherKeyNull", null, tree.higherKey(40));
    }

    private static void testHigherEntry() {
        NavigableMap<Integer, String> tree = new TreeMap<>();
        tree.put(5, "a");
        tree.put(10, "d");
        tree.put(12, "e");
        tree.put(1, "a");
        tree.put(40, "a");
        tree.put(3, "a");
        Map.Entry<Integer, String> expected = tree.ceilingEntry(12);
        assertEquals("NavigableMapTest.testHigherEntry", expected, tree.higherEntry(10));
        assertEquals("NavigableMapTest.testHigherEntryNull", null, tree.higherEntry(40));
    }

    private static void testHeadMap() {
        NavigableMap<Integer, String> tree = new TreeMap<>();
        tree.put(5, "a");
        tree.put(10, "d");
        tree.put(12, "e");
        tree.put(1, "a");
        tree.put(40, "a");
        tree.put(3, "a");
        SortedMap<Integer, String> expected = new TreeMap<>();
        expected.put(5, "a");
        expected.put(10, "d");
        expected.put(1, "a");
        expected.put(3, "a");
        assertEquals("NavigableMapTest.testHeadMap", expected, tree.headMap(12));
        assertEquals("NavigableMapTest.testHeadMapNotIncludeVal", expected, tree.headMap(12, false));
        expected.put(12, "e");
        assertEquals("NavigableMapTest.testHeadMapIncludeVal", expected, tree.headMap(12, true));
    }

    private static void testFloorKey() {
        NavigableMap<Integer, String> tree = new TreeMap<>();
        tree.put(5, "a");
        tree.put(10, "d");
        tree.put(12, "e");
        assertEquals("NavigableMapTest.testFloorKeyNull", null, tree.floorKey(4));
        assertEquals("NavigableMapTest.testFloorKey", (Integer) 10, tree.floorKey(11));
    }

    private static void testFloorEntry() {
        NavigableMap<Integer, String> tree = new TreeMap<>();
        assertEquals("NavigableMapTest.testFloorEntryNull", null, tree.floorEntry(15));
        tree.put(1, "a");
        tree.put(10, "d");
        tree.put(12, "e");
        Map.Entry<Integer, String> expected = tree.ceilingEntry(12);
        assertEquals("NavigableMapTest.testFloorEntry", expected, tree.floorEntry(15));
    }

    private static void testFirstEntry() {
        NavigableMap<Integer, String> tree = new TreeMap<>();
        tree.put(1, "a");
        tree.put(10, "d");
        tree.put(12, "e");
        Map.Entry<Integer, String> expected = tree.ceilingEntry(1);
        assertEquals("NavigableMapTest.testFirstEntry", expected, tree.firstEntry());
    }

    private static void testDescendingMap() {
        NavigableMap<Integer, String> tree = new TreeMap<>();
        tree.put(1, "a");
        tree.put(10, "d");
        tree.put(12, "e");
        String expected = "{12=e, 10=d, 1=a}";
        assertEquals("NavigableMapTest.testDescendingMap", expected, tree.descendingMap().toString());
    }

    private static void testDescendingKeySet() {
        NavigableMap<Integer, String> tree = new TreeMap<>();
        tree.put(1, "a");
        tree.put(10, "d");
        tree.put(12, "e");
        String expected = "[12, 10, 1]";
        assertEquals("NavigableMapTest.testDescendingKeySet", expected, tree.descendingKeySet().toString());
    }

    private static void testCeilingKey() {
        NavigableMap<Integer, String> tree = new TreeMap<>();
        tree.put(5, "c");
        tree.put(2, "b");
        tree.put(1, "a");
        tree.put(10, "d");
        tree.put(12, "e");
        assertEquals("NavigableMapTest.testCeilingKey", (Integer) 5, tree.ceilingKey(4));
        assertEquals("NavigableMapTest.testCeilingKeyNull", null, tree.ceilingKey(13));
    }

    private static void testCeilingEntry() {
        NavigableMap<Integer, String> tree = new TreeMap<>();
        tree.put(5, "c");
        tree.put(2, "b");
        tree.put(1, "a");
        tree.put(10, "d");
        tree.put(12, "e");
        String expected = "5=c";
        assertEquals("NavigableMapTest.testCeilingEntry", expected, tree.ceilingEntry(4).toString());
        assertEquals("NavigableMapTest.testCeilingEntryNull", null, tree.ceilingEntry(13));
    }
}