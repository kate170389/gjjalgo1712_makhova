package com.getjavajob.training.algo1712.makhovak.lesson06;

import java.util.HashMap;
import java.util.Map;

import static com.getjavajob.training.util.Assert.assertEquals;

public class MatrixHashMapTest {
    public static void main(String[] args) {
        testSet();
        testGet();
    }

    private static void testGet() {
        int dimension = 10;
        MatrixHashMap<Integer> test = new MatrixHashMap<>(dimension);
        test.set(0, 0, 1);
        test.set(0, 5, 5);
        test.set(4, 6, 46);
        assertEquals("MatrixHashMapTest.testGet", (Integer) 46, test.get(4, 6));
        assertEquals("MatrixHashMapTest.testGetNull", null, test.get(1, 3));
    }

    private static void testSet() {
        int dimension = 1000000;
        MatrixHashMap<Integer> test = new MatrixHashMap<>(dimension);
        for (int i = 0; i < dimension; i++) {
            test.set(i, dimension - 1, i);
        }
        Object[] testArray = test.toArray();
        Map<Integer, Integer> map = new HashMap<>(dimension);
        for (int i = 0; i < dimension; i++) {
            map.put(i * dimension + dimension - 1, i);
        }
        Object[] exp = map.values().toArray();
        assertEquals("MatrixHashMapTest.testSet", exp, testArray);
    }
}