package com.getjavajob.training.algo1712.makhovak.lesson04;

import java.util.ListIterator;

import static com.getjavajob.training.util.Assert.assertEquals;
import static com.getjavajob.training.util.Assert.fail;

public class DoublyLinkedListTest {
    public static void main(String[] args) {
        testAdd();
        testAddBeginning();
        testAddMiddle();
        testAddEnd();
        testSet();
        testGet();
        testRemoveFromBeginning();
        testRemoveFromMiddle();
        testRemoveFromEnd();
        testRemoveObject();
        testSize();
        testIndexOf();
        testContains();

        testAddException();
        testSetException();
        testGetException();
        testRemoveException();

        testListIteratorHasNext();
        testListIteratorNext();
        testListIteratorHasPrevious();
        testListIteratorPrevious();
        testIteratorNextIndex();
        testIteratorPreviousIndex();
        testIteratorRemove();
        testIteratorSet();
        testIteratorAdd();

        testListIteratorNextNoSuchElementsException();
        testListIteratorPreviousNoSuchElementsException();
        testListIteratorNextConcurrentModificationException();
        testListIteratorPreviousConcurrentModificationException();
        testListIteratorRemoveConcurrentModificationException();
        testListIteratorSetConcurrentModificationException();
        testListIteratorAddConcurrentModificationException();
        testListIteratorRemoveIllegalStateException();
        testListIteratorSetIllegalStateException();
    }

    private static void testContains() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        testDLL.add(5);
        testDLL.add(7);
        testDLL.add(null);
        testDLL.add(3);
        assertEquals("DLLTest.testContainsTrue", true, testDLL.contains(null));
        assertEquals("DLLTest.testContainsFalse", false, testDLL.contains(10));
    }

    private static void testIndexOf() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        testDLL.add(5);
        testDLL.add(7);
        testDLL.add(null);
        testDLL.add(3);
        assertEquals("DLLTest.testIndexOfNullElement", 2, testDLL.indexOf(null));
        assertEquals("DLLTest.testIndexOf", 3, testDLL.indexOf(3));
        assertEquals("DLLTest.testIndexOfNotElement", -1, testDLL.indexOf(9));
    }

    private static void testSize() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        testDLL.add(5);
        testDLL.add(7);
        testDLL.add(3);
        assertEquals("DLLTest.testSize", 3, testDLL.size());
    }

    private static void testSet() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        Integer[] testArray = new Integer[]{5, 9, 3};
        testDLL.add(5);
        testDLL.add(7);
        testDLL.add(3);
        assertEquals("DLLTest.testSet", (Integer) 7, testDLL.set(1, 9));
        assertEquals("DLLTest.testSet", testArray, testDLL.toArray());
    }

    private static void testAdd() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        Integer[] testArray = new Integer[]{5, 7, 3};
        testDLL.add(5);
        testDLL.add(7);
        testDLL.add(3);
        assertEquals("DLLTest.testAdd", testArray, testDLL.toArray());
    }

    private static void testAddBeginning() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        Integer[] testArray = new Integer[]{6, 5, 7, 3};
        testDLL.add(5);
        testDLL.add(7);
        testDLL.add(3);
        testDLL.add(0, 6);
        assertEquals("DLLTest.testAddBeginning", testArray, testDLL.toArray());
    }

    private static void testAddMiddle() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        Integer[] testArray = new Integer[]{5, 7, 6, 3};
        testDLL.add(5);
        testDLL.add(7);
        testDLL.add(3);
        testDLL.add(2, 6);
        assertEquals("DLLTest.testAddMiddle", testArray, testDLL.toArray());
    }

    private static void testAddEnd() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        Integer[] testArray = new Integer[]{5, 7, 3, 6};
        testDLL.add(5);
        testDLL.add(7);
        testDLL.add(3);
        testDLL.add(3, 6);
        assertEquals("DLLTest.testAddEnd", testArray, testDLL.toArray());
    }

    private static void testGet() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        testDLL.add(5);
        testDLL.add(7);
        testDLL.add(3);
        assertEquals("DLLTest.testGet", (Integer) 7, testDLL.get(1));
    }

    private static void testRemoveFromBeginning() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        Integer[] testArray = new Integer[]{7, 3, 6};
        testDLL.add(5);
        testDLL.add(7);
        testDLL.add(3);
        testDLL.add(6);
        testDLL.remove(0);
        assertEquals("DLLTest.testRemoveFromBeginning", testArray, testDLL.toArray());
    }

    private static void testRemoveFromMiddle() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        Integer[] testArray = new Integer[]{5, 7, 6};
        testDLL.add(5);
        testDLL.add(7);
        testDLL.add(3);
        testDLL.add(6);
        testDLL.remove(2);
        assertEquals("DLLTest.testRemoveFromMiddle", testArray, testDLL.toArray());
    }

    private static void testRemoveFromEnd() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        Integer[] testArray = new Integer[]{5, 7, 3};
        testDLL.add(5);
        testDLL.add(7);
        testDLL.add(3);
        testDLL.add(6);
        testDLL.remove(3);
        assertEquals("DLLTest.testRemoveFromEnd", testArray, testDLL.toArray());
    }

    private static void testRemoveObject() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        Integer[] testArray = new Integer[]{5, 7, 6};
        testDLL.add(5);
        testDLL.add(7);
        testDLL.add(3);
        testDLL.add(6);
        Integer testVal = 3;
        testDLL.remove(testVal);
        assertEquals("DLLTest.testRemoveObject", testArray, testDLL.toArray());
    }

    private static void testListIteratorHasPrevious() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        testDLL.add(5);
        testDLL.add(7);
        testDLL.add(3);
        ListIterator<Integer> listIterator = testDLL.listIterator();
        assertEquals("DLLTest.testListIteratorHasNotPrevious", false, listIterator.hasPrevious());
        listIterator = testDLL.listIterator(2);
        assertEquals("DLLTest.testListIteratorHasPrevious", true, listIterator.hasPrevious());
    }

    private static void testListIteratorHasNext() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        testDLL.add(5);
        testDLL.add(7);
        testDLL.add(3);
        ListIterator<Integer> listIterator = testDLL.listIterator();
        assertEquals("DLLTest.testListIteratorHasNext", true, listIterator.hasNext());
        listIterator = testDLL.listIterator(3);
        assertEquals("DLLTest.testListIteratorHasNotNext", false, listIterator.hasNext());
    }

    private static void testIteratorRemove() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        Integer[] testArray = new Integer[]{3, 4};
        testDLL.add(1);
        testDLL.add(2);
        testDLL.add(3);
        testDLL.add(4);
        ListIterator<Integer> listIterator = testDLL.listIterator(1);
        listIterator.next();
        listIterator.remove();
        listIterator.previous();
        listIterator.remove();
        assertEquals("DLLTest.testIteratorRemove", testArray, testDLL.toArray());
    }

    private static void testIteratorSet() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        Integer[] testArray = new Integer[]{5, 1};
        testDLL.add(5);
        testDLL.add(7);
        ListIterator<Integer> listIterator = testDLL.listIterator(2);
        listIterator.previous();
        listIterator.set(1);
        assertEquals("DLLTest.testIteratorSet", testArray, testDLL.toArray());
    }

    private static void testIteratorAdd() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        Integer[] testArray = new Integer[]{5, 7, 1, 3, 2};
        testDLL.add(5);
        testDLL.add(7);
        testDLL.add(1);
        ListIterator<Integer> listIterator = testDLL.listIterator(3);
        listIterator.add(2);
        listIterator.previous();
        listIterator.add(3);
        assertEquals("DLLTest.testIteratorAdd", testArray, testDLL.toArray());
    }

    private static void testIteratorPreviousIndex() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        testDLL.add(5);
        testDLL.add(7);
        ListIterator<Integer> listIterator = testDLL.listIterator(2);
        assertEquals("DLLTest.testIteratorPreviousIndex", 1, listIterator.previousIndex());

    }

    private static void testIteratorNextIndex() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        testDLL.add(5);
        testDLL.add(7);
        ListIterator<Integer> listIterator = testDLL.listIterator(2);
        assertEquals("DLLTest.testIteratorNextIndex", 2, listIterator.nextIndex());
    }

    private static void testListIteratorPrevious() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        testDLL.add(5);
        testDLL.add(7);
        testDLL.add(3);
        ListIterator<Integer> listIterator = testDLL.listIterator(3);
        assertEquals("DLLTest.testListIteratorPrevious", (Integer) 3, listIterator.previous());
        assertEquals("DLLTest.testListIteratorPrevious", (Integer) 7, listIterator.previous());
        assertEquals("DLLTest.testListIteratorPrevious", (Integer) 5, listIterator.previous());
    }

    private static void testListIteratorNext() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        testDLL.add(5);
        testDLL.add(7);
        testDLL.add(3);
        ListIterator<Integer> listIterator = testDLL.listIterator();
        assertEquals("DLLTest.testListIteratorNext", (Integer) 5, listIterator.next());
        assertEquals("DLLTest.testListIteratorNext", (Integer) 7, listIterator.next());
        assertEquals("DLLTest.testListIteratorNext", (Integer) 3, listIterator.next());
    }

    private static void testGetException() {
        DoublyLinkedList<Number> testDLL = new DoublyLinkedList<>();
        try {
            testDLL.add(1.3);
            testDLL.add(53f);
            testDLL.add(30);
            testDLL.get(-5);
            fail("There are not exception"); //
        } catch (Exception e) {
            assertEquals("DLLTest.testGetException", "IndexOutOfBoundsException Index: -5, Size: " +
                    "3 Available indexes: from 0 to 2", e.getMessage());
        }
    }

    private static void testSetException() {
        DoublyLinkedList<Number> testDLL = new DoublyLinkedList<>();
        try {
            testDLL.add(1.3);
            testDLL.add(53f);
            testDLL.add(30);
            testDLL.set(3, 8);
            fail("There are not exception"); //
        } catch (Exception e) {
            assertEquals("DLLTest.testSetException", "IndexOutOfBoundsException Index: 3, Size: 3" +
                    " Available indexes: from 0 to 2", e.getMessage());
        }
    }

    private static void testRemoveException() {
        DoublyLinkedList<Number> testDLL = new DoublyLinkedList<>();
        try {
            testDLL.add(1.3);
            testDLL.add(53f);
            testDLL.add(30);
            testDLL.remove(6);
            fail("There are not exception"); //
        } catch (Exception e) {
            assertEquals("DLLTest.testRemoveException", "IndexOutOfBoundsException Index: 6, Size: 3" +
                    " Available indexes: from 0 to 2", e.getMessage());
        }
    }

    private static void testAddException() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        try {
            testDLL.add(9);
            testDLL.add(20);
            testDLL.add(30);
            testDLL.add(1, 60);
            testDLL.add(5, 4);
            fail("There are not exception"); //
        } catch (Exception e) {
            assertEquals("DLLTest.testAddException", "IndexOutOfBoundsException Index: 5, Size: 4 " +
                    "Available indexes: from 0 to 3", e.getMessage());
        }
    }

    private static void testListIteratorSetIllegalStateException() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        testDLL.add(10);
        testDLL.add(90);
        testDLL.add(45);
        ListIterator<Integer> testListIterator = testDLL.listIterator(1);
        try {
            testListIterator.set(5);
            fail("There are not IllegalStateException");
        } catch (Exception e) {
            assertEquals("DLLTest.testListIteratorSetIllegalStateException",
                    "IllegalStateException: there are no returned elements", e.getMessage());
        }
    }

    private static void testListIteratorAddConcurrentModificationException() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        testDLL.add(10);
        testDLL.add(90);
        testDLL.add(45);
        ListIterator<Integer> testListIterator = testDLL.listIterator(2);
        try {
            testDLL.add(7);
            testListIterator.add(6);
            fail("There are not ConcurrentModificationException");
        } catch (Exception e) {
            assertEquals("DLLTest.testListIteratorAddConcurrentModificationException",
                    "ConcurrentModificationException: the list was changed", e.getMessage());
        }
    }

    private static void testListIteratorSetConcurrentModificationException() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        testDLL.add(10);
        testDLL.add(90);
        testDLL.add(45);
        ListIterator<Integer> testListIterator = testDLL.listIterator(2);
        try {
            testDLL.add(7);
            testListIterator.set(6);
            fail("There are not ConcurrentModificationException");
        } catch (Exception e) {
            assertEquals("DLLTest.testListIteratorSetConcurrentModificationException",
                    "ConcurrentModificationException: the list was changed", e.getMessage());
        }
    }

    private static void testListIteratorRemoveConcurrentModificationException() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        testDLL.add(10);
        testDLL.add(90);
        testDLL.add(45);
        ListIterator<Integer> testListIterator = testDLL.listIterator(2);
        try {
            testDLL.add(7);
            testListIterator.remove();
            fail("There are not ConcurrentModificationException");
        } catch (Exception e) {
            assertEquals("DLLTest.testListIteratorRemoveConcurrentModificationException",
                    "ConcurrentModificationException: the list was changed", e.getMessage());
        }
    }

    private static void testListIteratorRemoveIllegalStateException() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        testDLL.add(10);
        testDLL.add(90);
        testDLL.add(45);
        ListIterator<Integer> testListIterator = testDLL.listIterator(1);
        try {
            testListIterator.remove();
            fail("There are not IllegalStateException");
        } catch (Exception e) {
            assertEquals("DLLTest.testListIteratorRemoveIllegalStateException",
                    "IllegalStateException: there are no returned elements", e.getMessage());
        }
    }

    private static void testListIteratorPreviousConcurrentModificationException() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        testDLL.add(10);
        testDLL.add(90);
        testDLL.add(45);
        ListIterator<Integer> testListIterator = testDLL.listIterator(2);
        try {
            testDLL.add(7);
            testListIterator.previous();
            fail("There are not ConcurrentModificationException");
        } catch (Exception e) {
            assertEquals("DLLTest.testListIteratorPreviousConcurrentModificationException",
                    "ConcurrentModificationException: the list was changed", e.getMessage());
        }
    }

    private static void testListIteratorNextConcurrentModificationException() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        testDLL.add(10);
        testDLL.add(90);
        testDLL.add(45);
        ListIterator<Integer> testListIterator = testDLL.listIterator(2);
        try {
            testDLL.add(7);
            testListIterator.next();
            fail("There are not ConcurrentModificationException");
        } catch (Exception e) {
            assertEquals("DLLTest.testListIteratorNextConcurrentModificationException",
                    "ConcurrentModificationException: the list was changed", e.getMessage());
        }
    }

    private static void testListIteratorPreviousNoSuchElementsException() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        testDLL.add(10);
        testDLL.add(90);
        testDLL.add(45);
        ListIterator<Integer> testListIterator = testDLL.listIterator(2);
        try {
            testListIterator.previous();
            testListIterator.previous();
            testListIterator.previous();
            fail("There are not NoSuchElementsException");
        } catch (Exception e) {
            assertEquals("DLLTest.testListIteratorPreviousNoSuchElementsException", "No such elements",
                    e.getMessage());
        }
    }

    private static void testListIteratorNextNoSuchElementsException() {
        DoublyLinkedList<Integer> testDLL = new DoublyLinkedList<>();
        testDLL.add(10);
        testDLL.add(90);
        testDLL.add(45);
        ListIterator<Integer> testListIterator = testDLL.listIterator();
        try {
            testListIterator.next();
            testListIterator.next();
            testListIterator.next();
            testListIterator.next();
            fail("There are not NoSuchElementsException");
        } catch (Exception e) {
            assertEquals("DLLTest.testListIteratorNextNoSuchElementsException", "No such elements",
                    e.getMessage());
        }
    }
}