package com.getjavajob.training.algo1712.makhovak.lesson04;

import java.util.*;

public class DoublyLinkedList<V> extends AbstractListDLL<V> implements List<V> {
    private int size;
    private Element<V> first;
    private Element<V> last;
    private int modificationsCount;

    public int size() {
        return size;
    }

    public boolean contains(Object val) {
        return indexOf(val) >= 0;
    }

    private Element<V> searchElement(int index) {
        Element<V> pointer;
        if (index < size / 2) {
            pointer = first;
            for (int i = 0; i < index; i++) {
                pointer = pointer.next;
            }
        } else {
            pointer = last;
            for (int i = size - 1; i > index; i--) {
                pointer = pointer.prev;
            }
        }
        return pointer;
    }

    private void addToTheEnd(V val) {
        Element<V> prev = last;
        Element<V> e = new Element<>(prev, null, val);
        last = e;
        if (prev == null) {
            first = e;
        } else {
            prev.next = e;
        }
        size++;
        modificationsCount++;
    }

    private void addToTheMiddle(int index, V val) {
        Element<V> pointer = searchElement(index);
        Element<V> prev = pointer.prev;
        Element<V> e = new Element<>(prev, pointer, val);
        pointer.prev = e;
        if (prev == null) {
            first = e;
        } else {
            prev.next = e;
        }
        size++;
        modificationsCount++;
    }

    public boolean add(V val) {
        addToTheEnd(val);
        return true;
    }

    public void add(int index, V val) {
        handleIndicesRangeForAdd(index);
        if (index == size) {
            addToTheEnd(val);
        } else {
            addToTheMiddle(index, val);
        }
    }

    private V simpleRemove(Element<V> pointer) {
        Element<V> prev = pointer.prev;
        Element<V> next = pointer.next;
        if (prev == null) {
            first = next;
        } else {
            prev.next = next;
            pointer.prev = null;
        }
        if (next == null) {
            last = prev;
        } else {
            next.prev = prev;
            pointer.next = null;
        }
        V deletedElement = pointer.val;
        pointer.val = null;
        size--;
        modificationsCount++;
        return deletedElement;
    }

    public boolean remove(Object val) {
        if (val == null) {
            for (Element<V> pointer = first; pointer != null; pointer = pointer.next) {
                if (pointer.val == null) {
                    simpleRemove(pointer);
                    return true;
                }
            }
        } else {
            for (Element<V> pointer = first; pointer != null; pointer = pointer.next) {
                if (val.equals(pointer.val)) {
                    simpleRemove(pointer);
                    return true;
                }
            }
        }
        return false;
    }

    public V remove(int index) {
        handleIndicesRange(index);
        return simpleRemove(searchElement(index));
    }

    public int indexOf(Object val) {
        int index = 0;
        if (val == null) {
            for (Element<V> pointer = first; pointer != null; pointer = pointer.next) {
                if (pointer.val == null) {
                    return index;
                }
                index++;
            }
        } else {
            for (Element<V> pointer = first; pointer != null; pointer = pointer.next) {
                if (val.equals(pointer.val)) {
                    return index;
                }
                index++;
            }
        }
        return -1;
    }

    public V get(int index) {
        handleIndicesRange(index);
        return searchElement(index).val;
    }

    public V set(int index, V e) {
        handleIndicesRange(index);
        Element<V> prevElement = searchElement(index);
        V valPrevElement = prevElement.val;
        prevElement.val = e;
        modificationsCount++;
        return valPrevElement;
    }

    private static class Element<V> {
        private Element<V> prev;
        private Element<V> next;
        private V val;

        Element(Element<V> prev, Element<V> next, V element) {
            this.val = element;
            this.next = next;
            this.prev = prev;
        }
    }

    public Object[] toArray() {
        Object[] array = new Object[size];
        int i = 0;
        for (Element<V> pointer = first; pointer != null; pointer = pointer.next, i++) {
            array[i] = pointer.val;
        }
        return array;
    }

    private void handleIndicesRangeForAdd(int index) {
        if (index > size || index < 0) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
    }

    private void handleIndicesRange(int index) {
        if (index >= size || index < 0) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
    }

    private String outOfBoundsMsg(int index) {
        return "IndexOutOfBoundsException Index: " + index + ", Size: " + size + " Available indexes: " +
                "from 0 to " + (size - 1);
    }

    public ListIterator<V> listIterator(int index) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException("Index: " + index);
        }
        return new ListIteratorImpl(index);
    }

    public ListIterator<V> listIterator() {
        return new ListIteratorImpl(0);
    }

    private class ListIteratorImpl implements ListIterator<V> {
        private Element<V> currentElement;
        private Element<V> lastReturned;

        private int currentIndex;
        private int iteratorModificationsCount = DoublyLinkedList.this.modificationsCount;
        private int lastIndexReturned = -1;

        ListIteratorImpl(int index) {
            currentElement = index == size ? null : searchElement(index);
            currentIndex = index;
        }

        @Override
        public boolean hasNext() {
            return currentIndex != size;
        }

        @Override
        public V next() {
            checkForModification();
            if (currentIndex >= DoublyLinkedList.this.size) {
                throw new NoSuchElementException("No such elements");
            }
            lastIndexReturned = currentIndex++;
            lastReturned = currentElement;
            currentElement = currentElement.next;
            return lastReturned.val;
        }

        @Override
        public boolean hasPrevious() {
            return currentIndex - 1 >= 0;
        }

        @Override
        public V previous() {
            checkForModification();
            if (currentIndex - 1 < 0) {
                throw new NoSuchElementException("No such elements");
            }
            currentElement = currentElement == null ? last : currentElement.prev;
            lastIndexReturned = --currentIndex;
            lastReturned = currentElement;
            return lastReturned.val;
        }

        @Override
        public int nextIndex() {
            return currentIndex;
        }

        @Override
        public int previousIndex() {
            return currentIndex - 1;
        }

        @Override
        public void remove() {
            checkForModification();
            if (lastIndexReturned < 0) {
                throw new IllegalStateException("IllegalStateException: there are no returned elements");
            }
            Element<V> lastNext = lastReturned.next;
            DoublyLinkedList.this.simpleRemove(lastReturned);
            if (Objects.equals(lastReturned, currentElement)) {  //lastReturned: prev==null, next==null, var==null
                currentElement = lastNext;
            } else {
                currentIndex--;
            }
            lastIndexReturned = -1;
            lastReturned = null;
            iteratorModificationsCount = modificationsCount;
        }

        @Override
        public void set(V v) {
            checkForModification();
            if (lastIndexReturned < 0) {
                throw new IllegalStateException("IllegalStateException: there are no returned elements");
            }
            lastReturned.val = v;
        }

        @Override
        public void add(V v) {
            checkForModification();
            if (currentElement == null) {
                addToTheEnd(v);
            } else {
                addToTheMiddle(currentIndex, v);
            }
            currentIndex++;
            iteratorModificationsCount = modificationsCount;
            lastIndexReturned = -1;
            lastReturned = null;
        }

        final void checkForModification() {
            if (iteratorModificationsCount != modificationsCount) {
                throw new ConcurrentModificationException("ConcurrentModificationException: the list was changed");
            }
        }
    }
}