package com.getjavajob.training.algo1712.makhovak.lesson07.tree;

import java.util.*;

/**
 * An abstract base class providing some functionality of the Tree interface
 *
 * @param <E> element
 */
public abstract class AbstractTree<E> implements Tree<E> {
    @Override
    public boolean isInternal(Node<E> n) throws IllegalArgumentException {
        return childrenNumber(n) > 0;
    }

    @Override
    public boolean isExternal(Node<E> n) throws IllegalArgumentException {
        return childrenNumber(n) == 0;
    }

    @Override
    public boolean isRoot(Node<E> n) throws IllegalArgumentException {
        return n.equals(root());
    }

    @Override
    public boolean isEmpty() {
        return root() == null;
    }

    @Override
    public Iterator<E> iterator() {
        return new ElementIterator();
    }

    /**
     * @return an iterable collection of nodes of the tree in preorder
     */
    public Collection<Node<E>> preOrder() {
        Collection<Node<E>> preOrderCollection = new ArrayList<>();
        preOrderNode(preOrderCollection, root());
        return preOrderCollection;
    }

    private void preOrderNode(Collection<Node<E>> collection, Node<E> node) {
        if (node != null) {
            collection.add(node);
            Iterator<Node<E>> it = children(node).iterator();
            while (it.hasNext()) {
                preOrderNode(collection, it.next());
            }
        }
    }

    /**
     * @return an iterable collection of nodes of the tree in postorder
     */
    public Collection<Node<E>> postOrder() {
        Collection<Node<E>> postOrderCollection = new ArrayList<>();
        postOrderNode(postOrderCollection, root());
        return postOrderCollection;
    }

    private void postOrderNode(Collection<Node<E>> collection, Node<E> node) {
        if (node != null) {
            Iterator<Node<E>> it = children(node).iterator();
            while (it.hasNext()) {
                postOrderNode(collection, it.next());
            }
            collection.add(node);
        }
    }

    /**
     * @return an iterable collection of nodes of the tree in breadth-first order
     */
    public Collection<Node<E>> breadthFirst() {
        Collection<Node<E>> breadthFirstCollection = new ArrayList<>();
        Deque<Node<E>> deque = new ArrayDeque<>();
        deque.push(root());
        while (!deque.isEmpty()) {
            Node<E> node = deque.pop();
            breadthFirstCollection.add(node);
            deque.addAll(children(node));
        }
        return breadthFirstCollection;
    }

    /**
     * Adapts the iteration produced by {@link Tree#nodes()}
     */
    private class ElementIterator implements Iterator<E> {
        private Iterator<Node<E>> it = nodes().iterator();

        @Override
        public boolean hasNext() {
            return it.hasNext();
        }

        @Override
        public E next() {
            return it.next().getElement();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("UnsupportedOperationException remove");
        }
    }
}