package com.getjavajob.training.algo1712.makhovak.lesson05;

import java.util.*;

import static java.lang.System.out;

public class CollectionUtils<T> {
    public static <T> boolean filter(Collection<T> collection, Predicate<T> predicate) {
        boolean result = false;
        Objects.requireNonNull(collection);
        Objects.requireNonNull(predicate);
        for (Iterator<T> iteratorCollection = collection.iterator(); iteratorCollection.hasNext(); ) {
            if (!predicate.evaluate(iteratorCollection.next())) {
                iteratorCollection.remove();
                result = true;
            }
        }
        return result;
    }

    public static <I, O> void transform(Collection collection, Transformer<I, O> transformer) {
        Collection<O> result = transformNewCollection(collection, transformer);
        collection.clear();
        collection.addAll(result);
    }

    public static <I, O> Collection<O> transformNewCollection(Collection<I> collection, Transformer<I, O> transformer) {
        Collection<O> result = new ArrayList<>();
        Objects.requireNonNull(collection);
        Objects.requireNonNull(transformer);
        for (Iterator<I> iterator = collection.iterator(); iterator.hasNext(); ) {
            result.add(transformer.transform(iterator.next()));
        }
        return result;
    }

    public static <I> void forAllDo(Collection<I> collection, Closure<I> closure) {
        Objects.requireNonNull(collection);
        Objects.requireNonNull(closure);
        for (Iterator<I> iterator = collection.iterator(); iterator.hasNext(); ) {
            closure.changeVar(iterator.next());
        }
    }

    public static <I> Collection<I> unmodifiableCollection(Collection<I> collection) {
        return new UnmodifiableCollection<>(collection);
    }

    static class UnmodifiableCollection<I> extends AbstractCollection<I> {
        private final Collection<I> collection;

        UnmodifiableCollection(Collection<I> collection) {
            if (collection == null) {
                throw new NullPointerException();
            }
            this.collection = collection;
        }

        public Object[] toArray() {
            return collection.toArray();
        }

        public <I> I[] toArray(I[] a) {
            return collection.toArray(a);
        }

        public boolean add(I i) {
            throw new UnsupportedOperationException("UnsupportedOperationException");
        }

        public boolean remove(Object o) {
            throw new UnsupportedOperationException("UnsupportedOperationException");
        }

        public boolean containsAll(Collection<?> c) {
            return collection.containsAll(c);
        }

        public boolean addAll(Collection<? extends I> c) {
            throw new UnsupportedOperationException("UnsupportedOperationException");
        }

        public boolean removeAll(Collection<?> c) {
            throw new UnsupportedOperationException("UnsupportedOperationException");
        }

        public boolean retainAll(Collection<?> c) {
            throw new UnsupportedOperationException("UnsupportedOperationException");
        }

        public void clear() {
            throw new UnsupportedOperationException("UnsupportedOperationException");
        }

        public Iterator<I> iterator() {
            return new Iterator<I>() {
                private final Iterator<I> iterator = collection.iterator();

                public boolean hasNext() {
                    return iterator.hasNext();
                }

                public I next() {
                    return iterator.next();
                }

                public void remove() {
                    throw new UnsupportedOperationException("UnsupportedOperationException");
                }
            };
        }

        public int size() {
            return collection.size();
        }

        public boolean isEmpty() {
            return collection.isEmpty();
        }

        public boolean contains(Object o) {
            return false;
        }

        public boolean equals(Object o) {
            return collection.equals(o);
        }

        public int hashCode() {
            return collection.hashCode();
        }
    }
}