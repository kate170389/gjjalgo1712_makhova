package com.getjavajob.training.algo1712.makhovak.lesson05;

import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;

abstract class AbstractQueue<V> implements Queue<V> {
    @Override
    public boolean offer(V v) {
        throw new UnsupportedOperationException("Unsupported operation exception");
    }

    @Override
    public V poll() {
        throw new UnsupportedOperationException("Unsupported operation exception");
    }

    @Override
    public V element() {
        throw new UnsupportedOperationException("Unsupported operation exception");
    }

    @Override
    public V peek() {
        throw new UnsupportedOperationException("Unsupported operation exception");
    }

    @Override
    public int size() {
        throw new UnsupportedOperationException("Unsupported operation exception");
    }

    @Override
    public boolean isEmpty() {
        throw new UnsupportedOperationException("Unsupported operation exception");
    }

    @Override
    public boolean contains(Object o) {
        throw new UnsupportedOperationException("Unsupported operation exception");
    }

    @Override
    public Iterator<V> iterator() {
        throw new UnsupportedOperationException("Unsupported operation exception");
    }

    @Override
    public Object[] toArray() {
        throw new UnsupportedOperationException("Unsupported operation exception");
    }

    @Override
    public <V> V[] toArray(V[] a) {
        throw new UnsupportedOperationException("Unsupported operation exception");
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException("Unsupported operation exception");
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException("Unsupported operation exception");
    }

    @Override
    public boolean addAll(Collection<? extends V> c) {
        throw new UnsupportedOperationException("Unsupported operation exception");
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("Unsupported operation exception");
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("Unsupported operation exception");
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("Unsupported operation exception");
    }
}