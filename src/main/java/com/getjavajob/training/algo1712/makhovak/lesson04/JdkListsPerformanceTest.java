package com.getjavajob.training.algo1712.makhovak.lesson04;

import com.getjavajob.training.util.StopWatch;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static java.lang.System.out;

public class JdkListsPerformanceTest {
    public static void main(String[] args) {
        testAddBeginningLL();
        testAddBeginningAL();
        testAddMiddleAL();
        testAddMiddleLL();
        testAddEndAL();
        testAddEndLL();
        testRemoveBeginningAL();
        testRemoveBeginningLL();
        testRemoveMiddleAL();
        testRemoveMiddleLL();
        testRemoveEndAL();
        testRemoveEndLL();
    }

    private static void testRemoveEndLL() {
        List<Double> testLL = new LinkedList<>();
        StopWatch timer = new StopWatch();
        for (int i = 0; i < 500000; i++) {
            testLL.add(0, 2.9);
        }
        timer.start();
        for (int i = testLL.size() - 1; i > 0; i--) {
            testLL.remove(i);
        }
        out.println("-------- Remove from the beginning --------");
        out.println("LL.remove(end):" + timer.getElapsedTime() + "ms");
    }

    private static void testRemoveEndAL() {
        List<Double> testAL = new ArrayList<>();
        StopWatch timer = new StopWatch();
        for (int i = 0; i < 500000; i++) {
            testAL.add(0, 2.9);
        }
        timer.start();
        for (int i = testAL.size() - 1; i > 0; i--) {
            testAL.remove(i);
        }
        out.println("-------- Remove from the beginning --------");
        out.println("AL.remove(end):" + timer.getElapsedTime() + "ms");
    }

    private static void testRemoveMiddleLL() {
        List<Double> testLL = new LinkedList<>();
        StopWatch timer = new StopWatch();
        for (int i = 0; i < 700000; i++) {
            testLL.add(0, 2.9);
        }
        timer.start();
        for (int i = 10, m = testLL.size() / 2; i < 9000; i++) {
            testLL.remove(m + 3);
            testLL.remove(m - 3);
        }
        out.println("-------- Remove from the beginning --------");
        out.println("LL.remove(middle):" + timer.getElapsedTime() + "ms");
    }

    private static void testRemoveMiddleAL() {
        List<Double> testLL = new ArrayList<>();
        StopWatch timer = new StopWatch();
        for (int i = 0; i < 700000; i++) {
            testLL.add(0, 2.9);
        }
        timer.start();
        for (int i = 10, m = testLL.size() / 2; i < 9000; i++) {
            testLL.remove(m + 3);
            testLL.remove(m - 3);
        }
        out.println("-------- Remove from the beginning --------");
        out.println("AL.remove(middle):" + timer.getElapsedTime() + "ms");
    }

    private static void testRemoveBeginningLL() {
        List<Double> testLL = new LinkedList<>();
        StopWatch timer = new StopWatch();
        for (int i = 0; i < 300000; i++) {
            testLL.add(0, 2.9);
        }
        timer.start();
        for (int i = 300000; i > 0; i--) {
            testLL.remove(0);
        }
        out.println("-------- Remove from the beginning --------");
        out.println("LL.remove(0):" + timer.getElapsedTime() + "ms");
    }

    private static void testRemoveBeginningAL() {
        List<Double> testAL = new ArrayList<>();
        StopWatch timer = new StopWatch();
        for (int i = 0; i < 300000; i++) {
            testAL.add(0, 2.9);
        }
        timer.start();
        for (int i = 300000; i > 0; i--) {
            testAL.remove(0);
        }
        out.println("-------- Remove from the beginning --------");
        out.println("AL.remove(0):" + timer.getElapsedTime() + "ms");
    }

    private static void testAddEndLL() {
        List<Double> testLL = new LinkedList<>();
        StopWatch timer = new StopWatch();
        timer.start();
        for (int i = 0; i < 4000000; i++) {
            testLL.add(2.9);
        }
        out.println("-------- Addition to the end --------");
        out.println("LL.add(end,e):" + timer.getElapsedTime() + "ms");
    }

    private static void testAddEndAL() {
        List<Double> testAL = new ArrayList<>();
        StopWatch timer = new StopWatch();
        timer.start();
        for (int i = 0; i < 4000000; i++) {
            testAL.add(2.9);
        }
        out.println("-------- Addition to the end --------");
        out.println("AL.add(end,e):" + timer.getElapsedTime() + "ms");
    }

    private static void testAddMiddleAL() {
        List<Double> testAL = new ArrayList<>();
        StopWatch timer = new StopWatch();
        for (int i = 0; i < 100000; i++) {
            testAL.add(0, 1.0);
        }
        timer.start();
        for (int i = 10; i < 90000; i++) {
            testAL.add(testAL.size() / 2, 1.);
        }
        out.println("-------- Addition to the middle --------");
        out.println("AL.add(i/2):" + timer.getElapsedTime() + "ms");
    }

    private static void testAddMiddleLL() {
        List<Double> testLL = new LinkedList<>();
        StopWatch timer = new StopWatch();
        for (int i = 0; i < 100000; i++) {
            testLL.add(0, 1.0);
        }
        timer.start();
        for (int i = 10; i < 90000; i++) {
            testLL.add(testLL.size() / 2, 1.);
        }
        out.println("-------- Addition to the middle --------");
        out.println("LL.add(i/2):" + timer.getElapsedTime() + "ms");
    }

    private static void testAddBeginningAL() {
        List<Double> testAL = new ArrayList<>();
        StopWatch timer = new StopWatch();
        timer.start();
        for (int i = 0; i < 300000; i++) {
            testAL.add(0, 2.9);
        }
        out.println("-------- Addition to the beginning --------");
        out.println("AL.add(0,e):" + timer.getElapsedTime() + "ms");
    }

    private static void testAddBeginningLL() {
        List<Double> testLL = new LinkedList<>();
        StopWatch timer = new StopWatch();
        timer.start();
        for (int i = 0; i < 300000; i++) {
            testLL.add(0, 2.9);
        }
        out.println("-------- Addition to the beginning --------");
        out.println("LL.add(0,e):" + timer.getElapsedTime() + "ms");
    }
}