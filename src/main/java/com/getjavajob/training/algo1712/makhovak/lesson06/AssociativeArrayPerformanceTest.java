package com.getjavajob.training.algo1712.makhovak.lesson06;

import com.getjavajob.training.util.StopWatch;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static java.lang.System.out;

public class AssociativeArrayPerformanceTest {
    public static void main(String[] args) {
        testAddAA();
        testAddHM();
        testRemoveAA();
        testRemoveHM();
    }

    private static void testRemoveHM() {
        Map<Integer, String> testAA = new HashMap<>();
        StopWatch timer = new StopWatch();
        for (int i = 0; i < 20000000; i++) {
            testAA.put(i, "89");
        }
        timer.start();
        for (int i = 0; i < 20000000; i++) {
            testAA.remove(i);
        }
        out.println("-------- REMOVE --------");
        out.println("HM.remove(key): " + timer.getElapsedTime() + "ms");
    }

    private static void testRemoveAA() {
        AssociativeArray<Integer, String> testAA = new AssociativeArray<>();
        StopWatch timer = new StopWatch();
        for (int i = 0; i < 20000000; i++) {
            testAA.add(i, "89");
        }
        timer.start();
        for (int i = 0; i < 20000000; i++) {
            testAA.remove(i);
        }
        out.println("-------- REMOVE --------");
        out.println("AA.remove(key):" + timer.getElapsedTime() + "ms");
    }

    private static void testAddAA() {
        AssociativeArray<Integer, String> testAA = new AssociativeArray<>();
        StopWatch timer = new StopWatch();
        Random rand = new Random();
        timer.start();
        for (int i = 0; i < 100000000; i++) {
            testAA.add(rand.nextInt(1000), "89");
        }
        out.println("-------- ADD --------");
        out.println("AA.add(random):" + timer.getElapsedTime() + "ms");
    }

    private static void testAddHM() {
        Map<Integer, String> testAA = new HashMap<>();
        StopWatch timer = new StopWatch();
        Random rand = new Random();
        timer.start();
        for (int i = 0; i < 100000000; i++) {
            testAA.put(rand.nextInt(1000), "89");
        }
        out.println("-------- PUT --------");
        out.println("HM.put(random):" + timer.getElapsedTime() + "ms");
    }
}