package com.getjavajob.training.algo1712.makhovak.lesson05;

import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.training.util.Assert.assertEquals;

public class LinkedListStackTest {
    public static void main(String[] args) {
        testPop();
        testPush();
    }

    private static void testPush() {
        LinkedListStack<Integer> test = new LinkedListStack<>();
        test.push(1);
        test.push(2);
        test.push(3);
        test.push(4);
        List<Integer> exp = new ArrayList<>();
        exp.add(1);
        exp.add(2);
        exp.add(3);
        exp.add(4);
        assertEquals("LLStackTest.testPush", exp, test.asList());
    }

    private static void testPop() {
        LinkedListStack<Integer> test = new LinkedListStack<>();
        test.push(1);
        test.push(2);
        test.push(3);
        test.push(4);
        test.push(5);
        List<Integer> exp = new ArrayList<>();
        exp.add(1);
        exp.add(2);
        exp.add(3);
        exp.add(4);
        assertEquals("LLStackTest.testPop", (Integer) 5, test.pop());
        assertEquals("LLStackTest.testPop", exp, test.asList());
    }
}