package com.getjavajob.training.algo1712.makhovak.lesson04;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import static com.getjavajob.training.util.Assert.assertEquals;
import static com.getjavajob.training.util.Assert.fail;

public class DynamicArrayTest {
    public static void main(String[] args) {

        testGrowCapacityDynamicArray();
        testToArray();
        testAdd();
        testAddBeginning();
        testAddMiddle();
        testAddEnd();
        testSet();
        testGet();
        testRemoveFromBeginning();
        testRemoveFromMiddle();
        testRemoveFromEnd();
        testRemoveObject();
        testSize();
        testIndexOf();
        testContains();
        testAddException();
        testSetException();
        testGetException();
        testRemoveException();
        testListIteratorHasNext();
        testListIteratorNext();
        testListIteratorHasPrevious();
        testListIteratorPrevious();
        testIteratorNextIndex();
        testIteratorPreviousIndex();
        testIteratorRemove();
        testIteratorSet();
        testIteratorAdd();

        //checks methods, that are throws NextNoSuchElementsException, SuchElementsException
        //and ConcurrentModificationException
        testListIteratorNextNoSuchElementsException();
        testListIteratorPreviousNoSuchElementsException();
        testListIteratorNextConcurrentModificationException();
        testListIteratorPreviousConcurrentModificationException();
        testListIteratorRemoveIllegalStateException();
        testListIteratorRemoveConcurrentModificationException();
        testListIteratorSetIllegalStateException();
        testListIteratorSetConcurrentModificationException();
        testListIteratorAddConcurrentModificationException();

        //checks methods, that are not specified for implementation (throws UnsupportedOperationException)
        testIsEmptyException();
        testIteratorException();
        testAddAllException();
        testAddAllException();
        testClearException();
        testLastIndexOfException();
        testSubListException();
        testRetainAllException();
        testRemoveAllException();
        testContainsAllException();
        testToArrayException();
    }

    public static void testGrowCapacityDynamicArray() {
        DynamicArray<Integer> testDA = new DynamicArray<>(5);
        testDA.add(10);
        testDA.add(10);
        testDA.add(10);
        testDA.add(10);
        testDA.add(10);
        testDA.add(10);
        assertEquals("DynamicArrayTest.testGrowCapacityDynamicArray", 7, testDA.capacity());
    }

    public static void testToArray() {
        Integer[] testArray = new Integer[]{10, 20, 30};
        DynamicArray<Integer> testDA = new DynamicArray<>();
        testDA.add(10);
        testDA.add(20);
        testDA.add(30);
        assertEquals("DynamicArrayTest.testToArray", testArray, testDA.toArray());
    }

    public static void testAdd() {
        Object[] testArray = new Object[]{10, "pol", 1.5, null};
        DynamicArray<Object> testDA = new DynamicArray<>();
        assertEquals("DynamicArrayTest.testAdd", true, testDA.add(10));
        assertEquals("DynamicArrayTest.testAdd", true, testDA.add("pol"));
        assertEquals("DynamicArrayTest.testAdd", true, testDA.add(1.5));
        assertEquals("DynamicArrayTest.testAdd", true, testDA.add(null));
        assertEquals("DynamicArrayTest.testAdd", testArray, testDA.toArray());
    }

    public static void testAddBeginning() {
        Integer[] testArray = new Integer[]{null, 10, 20, 30};
        DynamicArray<Integer> testDA = new DynamicArray<>();
        testDA.add(10);
        testDA.add(20);
        testDA.add(30);
        testDA.add(0, null);
        assertEquals("DynamicArrayTest.testAddBeginning", testArray, testDA.toArray());
    }

    public static void testAddMiddle() {
        Integer[] testArray = new Integer[]{10, 20, null, 30, 40};
        DynamicArray<Integer> testDA = new DynamicArray<>();
        testDA.add(10);
        testDA.add(20);
        testDA.add(30);
        testDA.add(40);
        testDA.add(2, null);
        assertEquals("DynamicArrayTest.testAddMiddle", testArray, testDA.toArray());
    }

    public static void testAddEnd() {
        Integer[] testArray = new Integer[]{10, 20, 30, 40, null, 8};
        DynamicArray<Integer> testDA = new DynamicArray<>();
        testDA.add(10);
        testDA.add(20);
        testDA.add(30);
        testDA.add(40);
        testDA.add(testDA.size(), null);
        testDA.add(testDA.size(), 8);
        assertEquals("DynamicArrayTest.testAddEnd", testArray, testDA.toArray());
    }

    public static void testSet() {
        Integer[] testArray = new Integer[]{10, null, 30, 40};
        DynamicArray<Integer> testDA = new DynamicArray<>();
        testDA.add(10);
        testDA.add(20);
        testDA.add(30);
        testDA.add(40);
        assertEquals("DynamicArrayTest.testSet", (Integer) 20, testDA.set(1, null));
        assertEquals("DynamicArrayTest.testSet", testArray, testDA.toArray());
    }

    public static void testGet() {
        DynamicArray<Integer> testDA = new DynamicArray<>();
        testDA.add(10);
        testDA.add(20);
        testDA.add(30);
        testDA.add(40);
        assertEquals("DynamicArrayTest.testGet", (Integer) 30, testDA.get(2));
    }

    public static void testRemoveFromBeginning() {
        Integer[] testArray = new Integer[]{20, 30, 40};
        DynamicArray<Integer> testDA = new DynamicArray<>();
        testDA.add(10);
        testDA.add(20);
        testDA.add(30);
        testDA.add(40);
        assertEquals("DynamicArrayTest.testRemoveFromBeginning", (Integer) 10, testDA.remove(0));
        assertEquals("DynamicArrayTest.testRemoveFromBeginning", testArray, testDA.toArray());
    }

    public static void testRemoveFromMiddle() {
        String[] testArray = new String[]{"10", "remove", "40"};
        DynamicArray<String> testDA = new DynamicArray<>();
        testDA.add("10");
        testDA.add("p");
        testDA.add("remove");
        testDA.add("40");
        assertEquals("DynamicArrayTest.testRemoveFromMiddle", "p", testDA.remove(1));
        assertEquals("DynamicArrayTest.testRemoveFromMiddle", testArray, testDA.toArray());
    }

    public static void testRemoveFromEnd() {
        String[] testArray = new String[]{"10", "p", "remove"};
        DynamicArray<String> testDA = new DynamicArray<>();
        testDA.add("10");
        testDA.add("p");
        testDA.add("remove");
        testDA.add("40");
        assertEquals("DynamicArrayTest.testRemoveFromEnd", "40", testDA.remove(3));
        assertEquals("DynamicArrayTest.testRemoveFromEnd", testArray, testDA.toArray());
    }

    public static void testRemoveObject() {
        Integer[] testArray = new Integer[]{10, 2, 5, 90, 45};
        DynamicArray<Integer> testDA = new DynamicArray<>();
        testDA.add(10);
        testDA.add(2);
        testDA.add(5);
        testDA.add(40);
        testDA.add(90);
        testDA.add(45);
        int testObject = 9;
        int testObject2 = 40;
        assertEquals("DynamicArrayTest.testRemoveObject", true, testDA.remove((Integer) testObject2));
        assertEquals("DynamicArrayTest.testNotRemoveObject", false, testDA.remove((Integer) testObject));
        assertEquals("DynamicArrayTest.testRemoveObject", testArray, testDA.toArray());
    }

    public static void testSize() {
        DynamicArray<Object> testDA = new DynamicArray<>();
        testDA.add(10);
        testDA.add('p');
        testDA.add("remove");
        testDA.add(40);
        testDA.add(90);
        testDA.add(45);
        assertEquals("DynamicArrayTest.testSize", 6, testDA.size());
    }

    public static void testIndexOf() {
        DynamicArray<Number> testDA = new DynamicArray<>();
        testDA.add(10);
        testDA.add(5);
        testDA.add(7);
        testDA.add(null);
        testDA.add(90);
        testDA.add(45);
        int testObject = 5;
        assertEquals("DynamicArrayTest.testIndexOf", 1, testDA.indexOf(testObject));
        assertEquals("DynamicArrayTest.testIndexOf", 3, testDA.indexOf(null));
        assertEquals("DynamicArrayTest.testIndexOf", -1, testDA.indexOf(80));
    }

    public static void testContains() {
        DynamicArray<Object> testDA = new DynamicArray<>();
        testDA.add(10);
        testDA.add('p');
        testDA.add("remove");
        testDA.add(90);
        testDA.add(45);
        Object testObject = 'p';
        assertEquals("DynamicArrayTest.testContains", true, testDA.contains(testObject));
        assertEquals("DynamicArrayTest.testNotContains", false, testDA.contains(null));
    }


    public static void testAddException() {
        DynamicArray<Integer> testDA = new DynamicArray<>();
        try {
            testDA.add(10);
            testDA.add(20);
            testDA.add(30);
            testDA.add(4, null);
            fail("There are not exception"); //
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testAddException", "IndexOutOfBoundsException Index: 4, Size: 3 Available indexes: from 0 to 2", e.getMessage());
        }
    }

    public static void testSetException() {
        DynamicArray<String> testDA = new DynamicArray<>();
        try {
            testDA.add("g");
            testDA.add("e");
            testDA.add("t");
            testDA.set(6, "351");
            fail("There are not exception"); //
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testSetException", "IndexOutOfBoundsException Index: 6, Size: 3 Available indexes: from 0 to 2", e.getMessage());
        }
    }

    public static void testGetException() {
        DynamicArray<Number> testDA = new DynamicArray<>();
        try {
            testDA.add(1.3);
            testDA.add(5.2);
            testDA.add(30);
            testDA.get(6);
            fail("There are not exception"); //
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testGetException", "IndexOutOfBoundsException Index: 6, Size: 3 Available indexes: from 0 to 2", e.getMessage());
        }
    }

    public static void testRemoveException() {
        DynamicArray<Double> testDA = new DynamicArray<>();
        try {
            testDA.add(1.3);
            testDA.add(5.2);
            testDA.add(30.0);
            testDA.remove(6);
            fail("There are not exception"); //
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testRemoveException", "IndexOutOfBoundsException Index: 6, Size: 3 Available indexes: from 0 to 2", e.getMessage());
        }
    }

    public static void testListIteratorHasNext() {
        DynamicArray<Integer> testDA = new DynamicArray<>();
        testDA.add(10);
        testDA.add(4);
        testDA.add(5);
        testDA.add(90);
        testDA.add(45);
        ListIterator<Integer> testListIterator = testDA.listIterator();
        assertEquals("DynamicArrayTest.testIteratorHasNextTrue", true, testListIterator.hasNext());
        testListIterator = testDA.listIterator(4);
        testDA.remove(0);
        assertEquals("DynamicArrayTest.testIteratorHasNextFalse", false, testListIterator.hasNext());
    }

    public static void testListIteratorNext() {
        DynamicArray<Integer> testDA = new DynamicArray<>();
        testDA.add(10);
        testDA.add(4);
        testDA.add(5);
        testDA.add(90);
        testDA.add(45);
        int index = 2;
        ListIterator<Integer> testListIterator = testDA.listIterator(index);
        while (testListIterator.hasNext()) {
            assertEquals("DynamicArrayTest.testIteratorNext", testDA.get(index++), testListIterator.next());
        }
    }

    public static void testListIteratorHasPrevious() {
        DynamicArray<Integer> testDA = new DynamicArray<>();
        testDA.add(10);
        testDA.add(90);
        testDA.add(45);
        ListIterator<Integer> testListIterator = testDA.listIterator(2);
        assertEquals("DynamicArrayTest.testListIteratorHasPreviousTrue", true, testListIterator.hasPrevious());
        testListIterator = testDA.listIterator();
        assertEquals("DynamicArrayTest.testListIteratorHasPreviousFalse", false, testListIterator.hasPrevious());
    }

    public static void testListIteratorPrevious() {
        DynamicArray<Integer> testDA = new DynamicArray<>();
        testDA.add(10);
        testDA.add(4);
        testDA.add(5);
        testDA.add(90);
        testDA.add(45);
        ListIterator<Integer> testListIterator = testDA.listIterator(3);
        assertEquals("DynamicArrayTest.testListIteratorPrevious", (Integer) 5, testListIterator.previous());
    }

    public static void testIteratorNextIndex() {
        DynamicArray<Integer> testDA = new DynamicArray<>();
        testDA.add(10);
        testDA.add(4);
        testDA.add(5);
        testDA.add(90);
        testDA.add(45);
        ListIterator<Integer> testListIterator = testDA.listIterator();
        assertEquals("DynamicArrayTest.testIteratorNextIndex", 0, testListIterator.nextIndex());
        testListIterator.next();
        assertEquals("DynamicArrayTest.testIteratorNextIndex", 1, testListIterator.nextIndex());
    }

    public static void testIteratorPreviousIndex() {
        DynamicArray<Integer> testDA = new DynamicArray<>();
        testDA.add(10);
        testDA.add(4);
        testDA.add(5);
        testDA.add(90);
        testDA.add(45);
        ListIterator<Integer> testListIterator = testDA.listIterator(4);
        assertEquals("DynamicArrayTest.testIteratorNextIndex", 3, testListIterator.previousIndex());
    }

    public static void testIteratorRemove() {
        DynamicArray<Integer> testDA = new DynamicArray<>();
        testDA.add(10);
        testDA.add(20);
        testDA.add(30);
        testDA.add(40);
        testDA.add(50);
        ListIterator<Integer> testListIterator = testDA.listIterator(3);
        testListIterator.next();
        testListIterator.remove();
        DynamicArray<Integer> testDAAfterRemove = new DynamicArray<>();
        testDAAfterRemove.add(10);
        testDAAfterRemove.add(20);
        testDAAfterRemove.add(30);
        testDAAfterRemove.add(50);
        assertEquals("DynamicArrayTest.testIteratorRemove", testDAAfterRemove.toArray(), testDA.toArray());
    }

    public static void testIteratorSet() {
        DynamicArray<Integer> testDA = new DynamicArray<>();
        testDA.add(10);
        testDA.add(20);
        testDA.add(30);
        ListIterator<Integer> testListIterator = testDA.listIterator(2);
        testListIterator.previous();
        testListIterator.set(50);
        DynamicArray<Integer> testDAAfterSet = new DynamicArray<>();
        testDAAfterSet.add(10);
        testDAAfterSet.add(50);
        testDAAfterSet.add(30);
        assertEquals("DynamicArrayTest.testIteratorSet", testDAAfterSet.toArray(), testDA.toArray());
    }

    public static void testIteratorAdd() {
        DynamicArray<Integer> testDA = new DynamicArray<>();
        testDA.add(10);
        testDA.add(20);
        testDA.add(30);
        testDA.add(40);
        ListIterator<Integer> testListIterator = testDA.listIterator(2);
        testListIterator.next();
        testListIterator.add(50);
        DynamicArray<Integer> testDAAfterAdd = new DynamicArray<>();
        testDAAfterAdd.add(10);
        testDAAfterAdd.add(20);
        testDAAfterAdd.add(30);
        testDAAfterAdd.add(50);
        testDAAfterAdd.add(40);
        assertEquals("DynamicArrayTest.testIteratorAdd", testDAAfterAdd.toArray(), testDA.toArray());
    }

    public static void testListIteratorNextNoSuchElementsException() {
        DynamicArray<Integer> testDA = new DynamicArray<>();
        testDA.add(10);
        testDA.add(90);
        testDA.add(45);
        ListIterator<Integer> testListIterator = testDA.listIterator();
        try {
            testListIterator.next();
            testListIterator.next();
            testListIterator.next();
            testListIterator.next();
            fail("There are not NoSuchElementsException");
        } catch (Exception e) {
            assertEquals("DynamicArrayTest. testListIteratorNextNoSuchElementsException", "No such elements", e.getMessage());
        }
    }

    public static void testListIteratorPreviousNoSuchElementsException() {
        DynamicArray<Integer> testDA = new DynamicArray<>();
        testDA.add(10);
        testDA.add(90);
        testDA.add(45);
        ListIterator<Integer> testListIterator = testDA.listIterator(2);
        try {
            testListIterator.previous();
            testListIterator.previous();
            testListIterator.previous();
            fail("There are not NoSuchElementsException");
        } catch (Exception e) {
            assertEquals("DynamicArrayTest. testListIteratorPreviousNoSuchElementsException", "No such elements", e.getMessage());
        }
    }

    public static void testListIteratorNextConcurrentModificationException() {
        DynamicArray<Integer> testDA = new DynamicArray<>();
        testDA.add(10);
        testDA.add(90);
        testDA.add(45);
        ListIterator<Integer> testListIterator = testDA.listIterator();
        try {
            testListIterator.next();
            testListIterator.next();
            testDA.add(50);
            testListIterator.next();
            fail("There are not ConcurrentModificationException");
        } catch (Exception e) {
            assertEquals("DynamicArrayTest. testListIteratorNextConcurrentModificationException", "ConcurrentModificationException: the array was changed", e.getMessage());
        }
    }

    public static void testListIteratorPreviousConcurrentModificationException() {
        DynamicArray<Integer> testDA = new DynamicArray<>();
        testDA.add(10);
        testDA.add(90);
        testDA.add(45);
        ListIterator<Integer> testListIterator = testDA.listIterator(2);
        try {
            testListIterator.previous();
            testDA.add(50);
            testListIterator.previous();
            fail("There are not ConcurrentModificationException");
        } catch (Exception e) {
            assertEquals("DynamicArrayTest. testListIteratorNextConcurrentModificationException", "ConcurrentModificationException: the array was changed", e.getMessage());
        }
    }

    public static void testListIteratorRemoveIllegalStateException() {
        DynamicArray<Integer> testDA = new DynamicArray<>();
        testDA.add(10);
        testDA.add(90);
        testDA.add(45);
        ListIterator<Integer> testListIterator = testDA.listIterator();
        try {
            testListIterator.remove();
            testListIterator.next();
            testListIterator.remove();
            fail("There are not IllegalStateException");
        } catch (Exception e) {
            assertEquals("DynamicArrayTest. testListIteratorRemoveIllegalStateException", "IllegalStateException: there are no returned elements", e.getMessage());
        }
    }

    public static void testListIteratorRemoveConcurrentModificationException() {
        DynamicArray<Integer> testDA = new DynamicArray<>();
        testDA.add(10);
        testDA.add(90);
        testDA.add(45);
        ListIterator<Integer> testListIterator = testDA.listIterator();
        try {
            testListIterator.next();
            testListIterator.remove();
            testDA.remove(testDA.size() - 1);
            testListIterator.next();
            testListIterator.remove();
            fail("There are not ConcurrentModificationException");
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testListIteratorRemoveConcurrentModificationException", "ConcurrentModificationException: the array was changed", e.getMessage());
        }
    }

    public static void testListIteratorSetIllegalStateException() {
        DynamicArray<Integer> testDA = new DynamicArray<>();
        testDA.add(10);
        testDA.add(90);
        testDA.add(45);
        ListIterator<Integer> testListIterator = testDA.listIterator();
        try {
            testListIterator.set(50);
            testListIterator.next();
            testListIterator.set(50);
            fail("There are not IllegalStateException");
        } catch (Exception e) {
            assertEquals("DynamicArrayTest. testListIteratorSetIllegalStateException", "IllegalStateException: there are no returned elements", e.getMessage());
        }
    }

    public static void testListIteratorSetConcurrentModificationException() {
        DynamicArray<Integer> testDA = new DynamicArray<>();
        testDA.add(10);
        testDA.add(90);
        testDA.add(45);
        ListIterator<Integer> testListIterator = testDA.listIterator();
        try {
            testListIterator.next();
            testListIterator.set(30);
            testDA.remove(testDA.size() - 1);
            testListIterator.set(20);
            fail("There are not ConcurrentModificationException");
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testListIteratorSetConcurrentModificationException", "ConcurrentModificationException: the array was changed", e.getMessage());
        }
    }

    public static void testListIteratorAddConcurrentModificationException() {
        DynamicArray<Integer> testDA = new DynamicArray<>();
        testDA.add(10);
        testDA.add(90);
        testDA.add(45);
        ListIterator<Integer> testListIterator = testDA.listIterator();
        try {
            testListIterator.add(30);
            testDA.remove(testDA.size() - 1);
            testListIterator.add(20);
            fail("There are not ConcurrentModificationException");
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testListIteratorAddConcurrentModificationException", "ConcurrentModificationException: the array was changed", e.getMessage());
        }
    }

    public static void testIsEmptyException() {
        DynamicArray<Integer> testDA = new DynamicArray<>(5);
        try {
            testDA.isEmpty();
            testDA.add(6);
            fail("There are not UnsupportedOperationException");
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testIsEmptyException", "Unsupported operation exception", e.getMessage());
        }
    }

    public static void testIteratorException() {
        DynamicArray<Integer> testDA = new DynamicArray<>(5);
        try {
            testDA.iterator();
            testDA.add(6);
            fail("There are not UnsupportedOperationException");
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testIteratorException", "Unsupported operation exception", e.getMessage());
        }
    }

    public static void testAddAllException() {
        DynamicArray<Integer> testDA = new DynamicArray<>(5);
        List<Integer> c = new ArrayList<>();
        try {
            testDA.addAll(c);
            testDA.add(6);
            fail("There are not UnsupportedOperationException");
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testAddAllException(Collection)", "Unsupported operation exception", e.getMessage());
        }
        try {
            testDA.addAll(1, c);
            testDA.add(6);
            fail("There are not UnsupportedOperationException");
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testAddAllException(int,Collection)", "Unsupported operation exception", e.getMessage());
        }
    }

    public static void testClearException() {
        DynamicArray<Integer> testDA = new DynamicArray<>(5);
        try {
            testDA.clear();
            testDA.add(6);
            fail("There are not UnsupportedOperationException");
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testClearException", "Unsupported operation exception", e.getMessage());
        }
    }

    public static void testLastIndexOfException() {
        DynamicArray<Integer> testDA = new DynamicArray<>(5);
        Integer[] test = new Integer[3];
        try {
            testDA.lastIndexOf(test);
            testDA.add(6);
            fail("There are not UnsupportedOperationException");
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testLastIndexOfException", "Unsupported operation exception", e.getMessage());
        }
    }

    public static void testSubListException() {
        DynamicArray<Integer> testDA = new DynamicArray<>(5);
        int from = 1;
        int to = 3;
        try {
            testDA.subList(from, to);
            testDA.add(6);
            fail("There are not UnsupportedOperationException");
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testSubListException", "Unsupported operation exception", e.getMessage());
        }
    }

    public static void testRetainAllException() {
        DynamicArray<Integer> testDA = new DynamicArray<>(5);
        List<Integer> c = new ArrayList<>();
        try {
            testDA.retainAll(c);
            testDA.add(6);
            fail("There are not UnsupportedOperationException");
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testRetainAllException", "Unsupported operation exception", e.getMessage());
        }
    }

    public static void testRemoveAllException() {
        DynamicArray<Integer> testDA = new DynamicArray<>(5);
        List<Integer> c = new ArrayList<>();
        try {
            testDA.removeAll(c);
            testDA.add(6);
            fail("There are not UnsupportedOperationException");
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testRemoveAllException", "Unsupported operation exception", e.getMessage());
        }
    }

    public static void testContainsAllException() {
        DynamicArray<Integer> testDA = new DynamicArray<>(5);
        List<Integer> c = new ArrayList<>();
        try {
            testDA.containsAll(c);
            testDA.add(6);
            fail("There are not UnsupportedOperationException");
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testContainsAllException", "Unsupported operation exception", e.getMessage());
        }
    }

    public static void testToArrayException() {
        DynamicArray<Integer> testDA = new DynamicArray<>(5);
        Integer[] test = new Integer[5];
        try {
            testDA.toArray(test);
            testDA.add(6);
            fail("There are not UnsupportedOperationException");
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testContainsAllException", "Unsupported operation exception", e.getMessage());
        }
    }
}