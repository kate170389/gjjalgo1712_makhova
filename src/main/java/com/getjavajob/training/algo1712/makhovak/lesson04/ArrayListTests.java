package com.getjavajob.training.algo1712.makhovak.lesson04;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.getjavajob.training.util.Assert.assertEquals;
import static java.lang.System.out;

public class ArrayListTests {
    public static void main(String[] args) {
        out.println("CollectionTest");
        out.println();
        testSize();
        testIsEmpty();
        testContains();
        testToArray();
        testAdd();
        testRemove();
        testAddAll();
        testRemoveAll();
        testRemoveIf();
        testRetainAll();
        testClear();
        testEquals();
        testHashCode();
        out.println();

        out.println("ListTest");
        out.println();
        testAddAllList();
        testReplaceAll();
        testSort();
        testGet();
        testSet();
        testAddList();
        testRemoveList();
        testIndexOf();
        testLastIndexOf();
    }

    private static void testLastIndexOf() {
        List<Integer> test = new ArrayList<>();
        test.add(1);
        test.add(null);
        test.add(2);
        test.add(2);
        test.add(4);
        test.add(2);
        assertEquals("ArrayListTests.testLastIndexOfFalse", -1, test.lastIndexOf(63));
        assertEquals("ArrayListTests.testLastIndexOfNull", 1, test.lastIndexOf(null));
        assertEquals("ArrayListTests.testLastIndexOf", 5, test.lastIndexOf(2));
    }

    private static void testIndexOf() {
        List<Integer> test = new ArrayList<>();
        test.add(1);
        test.add(2);
        test.add(3);
        test.add(4);
        test.add(null);
        assertEquals("ArrayListTests.testIndexOfFalse", -1, test.indexOf(63));
        assertEquals("ArrayListTests.testIndexOfNull", 4, test.indexOf(null));
        assertEquals("ArrayListTests.testIndexOf", 3, test.indexOf(4));
    }

    private static void testRemoveList() {
        List<Integer> test = new ArrayList<>();
        test.add(1);
        test.add(2);
        test.add(3);
        test.add(4);
        test.add(5);
        List<Integer> testExp = new ArrayList<>();
        testExp.add(1);
        testExp.add(2);
        testExp.add(4);
        testExp.add(5);
        assertEquals("ArrayListTests.testRemove", (Integer) 3, test.remove(2));
        assertEquals("ArrayListTests.testRemoveList", testExp, test);
    }

    private static void testSet() {
        List<Integer> test = new ArrayList<>();
        test.add(1);
        test.add(2);
        test.add(3);
        test.set(1, 22);
        List<Integer> testExp = new ArrayList<>();
        testExp.add(1);
        testExp.add(22);
        testExp.add(3);
        assertEquals("ArrayListTests.testSet", testExp, test);
    }

    private static void testGet() {
        List<Integer> test = new ArrayList<>();
        test.add(1);
        test.add(2);
        test.add(3);
        test.add(4);
        test.add(5);
        assertEquals("ArrayListTests.testGet", (Integer) 3, test.get(2));
    }

    private static void testAddList() {
        List<Integer> test = new ArrayList<>();
        test.add(5);
        test.add(2);
        test.add(0);
        test.add(1, 8);
        List<Integer> testExp = new ArrayList<>();
        testExp.add(5);
        testExp.add(8);
        testExp.add(2);
        testExp.add(0);
        assertEquals("ArrayListTests.testAddList", testExp, test);
    }

    private static void testSort() {
        List<Integer> test = new ArrayList<>();
        test.add(5);
        test.add(2);
        test.add(0);
        List<Integer> testExp = new ArrayList<>();
        testExp.add(0);
        testExp.add(2);
        testExp.add(5);
        test.sort(null);
        assertEquals("ArrayListTests.testSort", testExp, test);
    }

    private static void testReplaceAll() {
        List<Integer> test = new ArrayList<>();
        test.add(1);
        test.add(2);
        test.add(3);
        test.replaceAll(e -> e * 2);
        List<Integer> testExp = new ArrayList<>();
        testExp.add(2);
        testExp.add(4);
        testExp.add(6);
        assertEquals("ArrayListTests.testReplaceAll", testExp, test);
    }

    private static void testAddAllList() {
        List<Integer> test = new ArrayList<>();
        test.add(1);
        test.add(2);
        test.add(3);
        List<Integer> testTwo = new ArrayList<>();
        testTwo.add(4);
        testTwo.add(5);
        testTwo.add(6);
        List<Integer> testExp = new ArrayList<>();
        testExp.add(1);
        testExp.add(4);
        testExp.add(5);
        testExp.add(6);
        testExp.add(2);
        testExp.add(3);
        List<Integer> testNull = new ArrayList<>();
        assertEquals("ArrayListTests.testAddAllListFalse", false, test.addAll(1, testNull));
        assertEquals("ArrayListTests.testAddAllListTrue", true, test.addAll(1, testTwo));
        assertEquals("ArrayListTests.testAddAllList", testExp, test);
    }

    private static void testHashCode() {
        Collection<Integer> test = new ArrayList<>();
        test.add(1);
        assertEquals("ArrayListTests.testEqualsFalse", 32, test.hashCode());
    }

    private static void testEquals() {
        Collection<Integer> test = new ArrayList<>();
        test.add(1);
        test.add(2);
        test.add(3);
        Collection<Integer> testExp = new ArrayList<>();
        testExp.add(1);
        testExp.add(2);
        testExp.add(3);
        assertEquals("ArrayListTests.testEqualsTrue", true, test.equals(testExp));
        testExp.clear();
        assertEquals("ArrayListTests.testEqualsFalse", false, test.equals(testExp));
    }

    private static void testClear() {
        Collection<Integer> test = new ArrayList<>();
        test.add(1);
        test.add(2);
        test.add(3);
        test.add(4);
        test.add(5);
        test.clear();
        Collection<Integer> testExp = new ArrayList<>();
        assertEquals("ArrayListTests.testClear", testExp, test);
    }

    private static void testRemoveIf() {
        Collection<Integer> test = new ArrayList<>();
        test.add(1);
        test.add(2);
        test.add(4);
        test.add(3);
        test.add(4);
        test.add(5);
        test.add(4);
        Collection<Integer> testExp = new ArrayList<>();
        testExp.add(1);
        testExp.add(2);
        testExp.add(3);
        testExp.add(5);
        assertEquals("ArrayListTests.testRemoveIfFalse", false, test.removeIf(p -> p == null));
        assertEquals("ArrayListTests.testRemoveIfTrue", true, test.removeIf(p -> p == 4));
        assertEquals("ArrayListTests.testRemoveIf", testExp, test);
    }

    private static void testRetainAll() {
        Collection<Integer> test = new ArrayList<>();
        test.add(1);
        test.add(2);
        test.add(3);
        test.add(4);
        test.add(5);
        Collection<Integer> testCol = new ArrayList<>();
        testCol.add(1);
        testCol.add(2);
        testCol.add(3);
        assertEquals("ArrayListTests.testRemoveAllFalse", false, testCol.retainAll(test));
        assertEquals("ArrayListTests.testRetainAllTrue", true, test.retainAll(testCol));
        Collection<Integer> testExp = new ArrayList<>();
        testExp.add(1);
        testExp.add(2);
        testExp.add(3);
        assertEquals("ArrayListTests.testAddAll", testExp, test);
    }

    private static void testRemoveAll() {
        Collection<Integer> test = new ArrayList<>();
        test.add(1);
        test.add(2);
        test.add(3);
        test.add(4);
        test.add(5);
        Collection<Integer> testCol = new ArrayList<>();
        assertEquals("ArrayListTests.testRemoveAllFalse", false, test.removeAll(testCol));
        testCol.add(1);
        testCol.add(2);
        testCol.add(3);
        Collection<Integer> testExp = new ArrayList<>();
        testExp.add(4);
        testExp.add(5);
        assertEquals("ArrayListTests.testRemoveAllTrue", true, test.removeAll(testCol));
        assertEquals("ArrayListTests.testAddAll", testExp, test);
    }

    private static void testAddAll() {
        Collection<Integer> test = new ArrayList<>();
        Collection<Integer> testCol = new ArrayList<>();
        assertEquals("ArrayListTests.testAddAllFalse", false, testCol.addAll(test));
        test.add(1);
        test.add(2);
        test.add(3);
        assertEquals("ArrayListTests.testAddAllTrue", true, testCol.addAll(test));
        assertEquals("ArrayListTests.testAddAll", test, testCol);
    }

    private static void testRemove() {
        Collection<Integer> test = new ArrayList<>();
        test.add(1);
        test.add(2);
        test.add(null);
        test.add(3);
        assertEquals("ArrayListTests.testRemoveNull", true, test.remove(null));
        assertEquals("ArrayListTests.testRemoveTrue", true, test.remove(2));
        assertEquals("ArrayListTests.testRemoveFalse", false, test.remove(10));
    }

    private static void testAdd() {
        Collection<Integer> test = new ArrayList<>();
        test.add(1);
        test.add(2);
        assertEquals("ArrayListTests.testAdd", true, test.add(3));
    }

    private static void testToArray() {
        Collection<Integer> test = new ArrayList<>();
        test.add(1);
        test.add(2);
        test.add(3);
        Object[] exp = new Object[]{1, 2, 3};
        Integer[] array = test.toArray(new Integer[test.size()]); //return Integer[]
        assertEquals("ArrayListTests.testToArrayGeneric", exp, array);
        assertEquals("ArrayListTests.testToArrayObject", exp, test.toArray()); //return Object[]
    }

    private static void testContains() {
        Collection<Integer> test = new ArrayList<>();
        test.add(1);
        test.add(2);
        test.add(3);
        assertEquals("ArrayListTests.testContainsFalse", false, test.contains(5));
        assertEquals("ArrayListTests.testContainsTrue", true, test.contains(2));
    }

    private static void testIsEmpty() {
        Collection<Integer> test = new ArrayList<>();
        test.add(1);
        test.add(1);
        test.add(1);
        assertEquals("ArrayListTests.testIsEmptyFalse", false, test.isEmpty());
        Collection<Integer> test1 = new ArrayList<>();
        assertEquals("ArrayListTests.testIsEmptyTrue", true, test1.isEmpty());
    }

    private static void testSize() {
        Collection<Integer> test = new ArrayList<>();
        test.add(1);
        test.add(1);
        test.add(1);
        assertEquals("ArrayListTests.testSize", 3, test.size());
    }
}