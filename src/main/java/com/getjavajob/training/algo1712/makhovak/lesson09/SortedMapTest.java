package com.getjavajob.training.algo1712.makhovak.lesson09;

import java.util.*;

import static com.getjavajob.training.util.Assert.assertEquals;

public class SortedMapTest {
    public static void main(String[] args) {
        testComparator();
        testEntrySet();
        testFirstKey();
        testHeadMap();
        testKeySet();
        testLastKey();
        testSubMap();
        testTailMap();
        testValues();
    }

    private static void testValues() {
        SortedMap<Integer, String> tree = new TreeMap<>();
        tree.put(5, "c");
        tree.put(12, "e");
        tree.put(10, "d");
        Collection<String> expected = new ArrayList<>();
        expected.add("c");
        expected.add("d");
        expected.add("e");
        Collection<String> actual = new ArrayList<>();
        actual.addAll(tree.values());
        assertEquals("SortedMapTest.testValues", expected, actual);
    }

    private static void testTailMap() {
        SortedMap<Integer, String> tree = new TreeMap<>();
        tree.put(5, "c");
        tree.put(2, "b");
        tree.put(1, "a");
        tree.put(10, "d");
        tree.put(12, "e");
        SortedMap<Integer, String> expected = new TreeMap<>();
        expected.put(5, "c");
        expected.put(12, "e");
        expected.put(10, "d");
        assertEquals("SortedMapTest.testTailMap", expected, tree.tailMap(5));
    }

    private static void testSubMap() {
        SortedMap<Integer, String> tree = new TreeMap<>();
        tree.put(5, "c");
        tree.put(2, "b");
        tree.put(1, "a");
        tree.put(10, "d");
        tree.put(12, "e");
        SortedMap<Integer, String> expected = new TreeMap<>();
        expected.put(5, "c");
        expected.put(2, "b");
        assertEquals("SortedMapTest.testSubMap", expected, tree.subMap(2, 10));
    }

    private static void testLastKey() {
        SortedMap<Integer, String> tree = new TreeMap<>();
        tree.put(5, "c");
        tree.put(2, "b");
        tree.put(1, "a");
        assertEquals("SortedMapTest.testLastKey", (Integer) 5, tree.lastKey());
    }

    private static void testKeySet() {
        SortedMap<Integer, String> tree = new TreeMap<>();
        tree.put(5, "c");
        tree.put(2, "b");
        tree.put(1, "a");
        Set<Integer> expected = new HashSet<>();
        expected.add(1);
        expected.add(2);
        expected.add(5);
        assertEquals("SortedMapTest.testKeySet", expected, tree.keySet());
    }

    private static void testHeadMap() {
        SortedMap<Integer, String> tree = new TreeMap<>();
        tree.put(5, "c");
        tree.put(2, "b");
        tree.put(1, "a");
        SortedMap<Integer, String> expected = new TreeMap<>();
        expected.put(2, "b");
        expected.put(1, "a");
        assertEquals("SortedMapTest.testHeadMap", expected, tree.headMap(5));
    }

    private static void testFirstKey() {
        SortedMap<Integer, String> tree = new TreeMap<>();
        tree.put(5, "c");
        tree.put(2, "b");
        tree.put(1, "a");
        assertEquals("SortedMapTest.testFirstKey", (Integer) 1, tree.firstKey());
    }

    private static void testEntrySet() {
        SortedMap<Integer, String> tree = new TreeMap<>();
        tree.put(5, "c");
        tree.put(2, "b");
        tree.put(1, "a");
        Map<Integer, String> expected = new HashMap<>();
        expected.put(1, "a");
        expected.put(2, "b");
        expected.put(5, "c");
        assertEquals("SortedMapTest.testEntrySet", expected.entrySet(), tree.entrySet());
    }

    private static void testComparator() {
        Comparator<Integer> comp = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1.compareTo(o2);
            }
        };
        SortedMap<Integer, String> tree = new TreeMap<>(comp);
        tree.put(5, "c");
        tree.put(2, "b");
        tree.put(1, "a");
        assertEquals("SortedMapTest.testComparator", comp, tree.comparator());
        tree = new TreeMap<>();
        assertEquals("SortedMapTest.testComparatorNull", null, tree.comparator());
    }
}