package com.getjavajob.training.algo1712.makhovak.lesson05;

import java.util.Calendar;

import static java.lang.System.out;

class Employee {
    private String surname;
    private String name;
    private String middleName;
    private String address;
    private int monthStartWorking;
    private int yearStartWorking;

    Employee(String surname, String name, String middleName, String address, int monthStartWorking,
             int yearStartWorking) {
        this.surname = surname;
        this.name = name;
        this.middleName = middleName;
        this.address = address;
        this.monthStartWorking = monthStartWorking;
        this.yearStartWorking = yearStartWorking;
    }

    Employee(String surname, String name, String address, int monthStartWorking, int yearStartWorking) {
        this(surname, name, "", address, monthStartWorking, yearStartWorking);
    }

    public void setMonthStartWorking(int month) {
        this.monthStartWorking = month;
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getAddress() {
        return address;
    }

    public int getMonthStartWorking() {
        return monthStartWorking;
    }

    public int getYearStartWorking() {
        return yearStartWorking;
    }

    public int calculateNumberOfWorkingYears() {
        Calendar calendar = Calendar.getInstance();
        int currentMonth = calendar.get(Calendar.MONTH) + 1;
        int currentYear = calendar.get(Calendar.YEAR);
        if (monthStartWorking - currentMonth > 0) {
            return currentYear - yearStartWorking - 1;
        } else {
            return currentYear - yearStartWorking;
        }
    }

    public String toString() {
        return getSurname() + "  " + getName() + "  " + getMiddleName() + "  " + getAddress() + "  " +
                getMonthStartWorking() + "  " + getYearStartWorking();
    }

    public int hashCode() {
        final int prime = 31;
        int result;
        result = (surname == null) ? 0 : surname.hashCode();
        result = (name == null) ? 0 : name.hashCode();
        result = (middleName == null) ? 0 : middleName.hashCode();
        result = (address == null) ? 0 : address.hashCode();
        result = prime * result + monthStartWorking;
        result = prime * result + yearStartWorking;
        return result;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Employee other = (Employee) obj;
        return name.equals(other.name) && surname.equals(other.surname) && middleName.equals(other.middleName) &&
                monthStartWorking == other.monthStartWorking && yearStartWorking == other.yearStartWorking;
    }

    public void print() {
        out.printf("%-14s", getSurname());
        out.printf("%-14s", getName());
        out.printf("%-14s", getMiddleName());
        out.printf("%-20s", getAddress());
        out.printf("%-14d", getMonthStartWorking());
        out.printf("%-14d", getYearStartWorking());
        out.printf("%d", calculateNumberOfWorkingYears());
        out.println();
    }
}