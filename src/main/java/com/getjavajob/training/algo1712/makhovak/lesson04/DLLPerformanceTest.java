package com.getjavajob.training.algo1712.makhovak.lesson04;

import com.getjavajob.training.util.StopWatch;

import java.util.LinkedList;
import java.util.List;

import static java.lang.System.out;

public class DLLPerformanceTest {
    public static void main(String[] args) {
        testAddBeginningLL();
        testAddBeginningDLL();
        testAddMiddleDLL();
        testAddMiddleLL();
        testAddEndDLL();
        testAddEndLL();
        testRemoveBeginningDLL();
        testRemoveBeginningLL();
        testRemoveMiddleDLL();
        testRemoveMiddleLL();
        testRemoveEndDLL();
        testRemoveEndLL();
    }

    private static void testRemoveEndLL() {
        List<Double> testLL = new LinkedList<>();
        StopWatch timer = new StopWatch();
        for (int i = 0; i < 50000000; i++) {
            testLL.add(0, 2.9);
        }
        timer.start();
        for (int i = testLL.size() - 1; i > 0; i--) {
            testLL.remove(i);
        }
        out.println("-------- Remove from the beginning --------");
        out.println("LL.remove(end):" + timer.getElapsedTime() + "ms");
    }

    private static void testRemoveEndDLL() {
        DoublyLinkedList<Double> testDLL = new DoublyLinkedList<>();
        StopWatch timer = new StopWatch();
        for (int i = 0; i < 50000000; i++) {
            testDLL.add(0, 2.9);
        }
        timer.start();
        for (int i = testDLL.size() - 1; i > 0; i--) {
            testDLL.remove(i);
        }
        out.println("-------- Remove from the beginning --------");
        out.println("DLL.remove(0):" + timer.getElapsedTime() + "ms");
    }

    private static void testRemoveMiddleLL() {
        List<Double> testLL = new LinkedList<>();
        StopWatch timer = new StopWatch();
        for (int i = 0; i < 400000; i++) {
            testLL.add(0, 2.9);
        }
        timer.start();
        for (int i = 10, m = testLL.size() / 2; i < 1000; i++) {
            testLL.remove(m + 3);
            testLL.remove(m - 3);
        }
        out.println("-------- Remove from the beginning --------");
        out.println("LL.remove(middle):" + timer.getElapsedTime() + "ms");
    }

    private static void testRemoveMiddleDLL() {
        DoublyLinkedList<Double> testDLL = new DoublyLinkedList<>();
        StopWatch timer = new StopWatch();
        for (int i = 0; i < 400000; i++) {
            testDLL.add(0, 2.9);
        }
        timer.start();
        for (int i = 10, m = testDLL.size() / 2; i < 1000; i++) {
            testDLL.remove(m + 3);
            testDLL.remove(m - 3);
        }
        out.println("-------- Remove from the beginning --------");
        out.println("DLL.remove(0):" + timer.getElapsedTime() + "ms");
    }

    private static void testRemoveBeginningDLL() {
        DoublyLinkedList<Double> testDLL = new DoublyLinkedList<>();
        StopWatch timer = new StopWatch();
        for (int i = 0; i < 40000000; i++) {
            testDLL.add(0, 2.9);
        }
        timer.start();
        for (int i = 40000000; i > 0; i--) {
            testDLL.remove(0);
        }
        out.println("-------- Remove from the beginning --------");
        out.println("DLL.remove(0):" + timer.getElapsedTime() + "ms");
    }

    private static void testRemoveBeginningLL() {
        List<Double> testLL = new LinkedList<>();
        StopWatch timer = new StopWatch();
        for (int i = 0; i < 40000000; i++) {
            testLL.add(0, 2.9);
        }
        timer.start();
        for (int i = 40000000; i > 0; i--) {
            testLL.remove(0);
        }
        out.println("-------- Remove from the beginning --------");
        out.println("LL.remove(0):" + timer.getElapsedTime() + "ms");
    }

    private static void testAddEndLL() {
        List<Double> testLL = new LinkedList<>();
        StopWatch timer = new StopWatch();
        timer.start();
        for (int i = 0; i < 4000000; i++) {
            testLL.add(2.9);
        }
        out.println("-------- Addition to the end --------");
        out.println("LL.add(end,e):" + timer.getElapsedTime() + "ms");
    }

    private static void testAddEndDLL() {
        DoublyLinkedList<Double> testDLL = new DoublyLinkedList<>();
        StopWatch timer = new StopWatch();
        timer.start();
        for (int i = 0; i < 4000000; i++) {
            testDLL.add(2.9);
        }
        out.println("-------- Addition to the end --------");
        out.println("DLL.add(end,e):" + timer.getElapsedTime() + "ms");

    }

    private static void testAddMiddleLL() {
        List<Double> testLL = new LinkedList<>();
        StopWatch timer = new StopWatch();
        timer.start();
        for (int i = 0; i < 4000000; i++) {
            testLL.add(0, 2.9);
        }
        timer.start();
        for (int i = 10, m = i / 2; i < 2000000; i++) {
            testLL.add(m + 3, 2.9);
            testLL.add(m - 3, 2.9);
        }
        out.println("-------- Addition to the middle --------");
        out.println("LL.add(i/2+-2,e):" + timer.getElapsedTime() + "ms");
    }

    private static void testAddMiddleDLL() {
        DoublyLinkedList<Double> testDLL = new DoublyLinkedList<>();
        StopWatch timer = new StopWatch();
        timer.start();
        for (int i = 0; i < 4000000; i++) {
            testDLL.add(0, 2.9);
        }
        timer.start();
        for (int i = 10, m = i / 2; i < 2000000; i++) {
            testDLL.add(m + 3, 2.9);
            testDLL.add(m - 3, 2.9);
        }
        out.println("-------- Addition to the middle --------");
        out.println("DLL.add(i/2+-2,e):" + timer.getElapsedTime() + "ms");
    }

    private static void testAddBeginningLL() {
        List<Double> testLL = new LinkedList<>();
        StopWatch timer = new StopWatch();
        timer.start();
        for (int i = 0; i < 4000000; i++) {
            testLL.add(0, 2.9);
        }
        out.println("-------- Addition to the beginning --------");
        out.println("LL.add(0,e):" + timer.getElapsedTime() + "ms");
    }

    private static void testAddBeginningDLL() {
        DoublyLinkedList<Double> testDLL = new DoublyLinkedList<>();
        StopWatch timer = new StopWatch();
        timer.start();
        for (int i = 0; i < 4000000; i++) {
            testDLL.add(0, 2.9);
        }
        out.println("-------- Addition to the beginning --------");
        out.println("DLL.add(0,e):" + timer.getElapsedTime() + "ms");
    }
}