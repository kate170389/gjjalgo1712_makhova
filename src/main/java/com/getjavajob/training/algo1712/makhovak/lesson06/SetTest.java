package com.getjavajob.training.algo1712.makhovak.lesson06;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static com.getjavajob.training.util.Assert.assertEquals;

public class SetTest {
    public static void main(String[] args) {
        testAdd();
        testAddAll();
    }

    private static void testAddAll() {
        Set<String> testSet = new HashSet<>();
        Collection<String> testCol = new ArrayList<>();
        testCol.add("1");
        testCol.add("2");
        testCol.add("3");
        testCol.add("1");
        String[] expected = new String[]{"1", "2", "3"};
        assertEquals("SetTest.testAddAll", true, testSet.addAll(testCol));
        assertEquals("SetTest.testAddAll", expected, testSet.toArray());
        testCol.clear();
        assertEquals("SetTest.testNotAddAll", false, testSet.addAll(testCol));
    }

    private static void testAdd() {
        Set<String> testSet = new HashSet<>();
        testSet.add("1");
        testSet.add("2");
        testSet.add("3");
        assertEquals("SetTest.testAdd", true, testSet.add("4"));
        assertEquals("SetTest.testNotAdd", false, testSet.add("4"));
    }
}