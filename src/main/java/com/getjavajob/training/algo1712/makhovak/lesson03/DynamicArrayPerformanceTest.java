package com.getjavajob.training.algo1712.makhovak.lesson03;

import com.getjavajob.training.util.StopWatch;

import java.util.ArrayList;

import static java.lang.System.out;

public class DynamicArrayPerformanceTest {
    public static void main(String[] args) {
        testAddBeginningDA();
        testAddBeginningAL();
        testAddMiddleDA();
        testAddMiddleAL();
        testAddEndDA();
        testAddEndAL();
        testRemoveBeginningDA();
        testRemoveBeginningAL();
        testRemoveMiddleDA();
        testRemoveMiddleAL();
        testRemoveEndDA();
        testRemoveEndAL();
    }

    public static void testAddBeginningDA() {
        DynamicArray testDA = new DynamicArray();
        StopWatch timer = new StopWatch();
        timer.start();
        for (int i = 0; i < 400000; i++) {
            testDA.add(0, 2);
        }
        out.println("-------- Addition to the beginning --------");
        out.println("DynamicArray.add(0,e):" + timer.getElapsedTime() + "ms");
    }

    public static void testAddBeginningAL() {
        ArrayList<Integer> testAL = new ArrayList<>();
        StopWatch timer = new StopWatch();
        timer.start();
        for (int i = 0; i < 400000; i++) {
            testAL.add(0, 2);
        }
        out.println("ArrayList.add(0,e):" + timer.getElapsedTime() + "ms");
    }

    public static void testAddMiddleDA() {
        DynamicArray testDA = new DynamicArray();
        for (int i = 0; i < 5; i++) {
            testDA.add(2);
        }
        StopWatch timer = new StopWatch();
        timer.start();
        for (int i = 0; i < 400000; i++) {
            testDA.add(3, 2);
        }
        out.println("-------- Addition to the middle --------");
        out.println("DynamicArray.add(3,e):" + timer.getElapsedTime() + "ms");
    }

    public static void testAddMiddleAL() {
        ArrayList<Integer> testAL = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            testAL.add(2);
        }
        StopWatch timer = new StopWatch();
        timer.start();
        for (int i = 0; i < 400000; i++) {
            testAL.add(3, 2);
        }
        out.println("ArrayList.add(3,e):" + timer.getElapsedTime() + "ms");
    }

    public static void testAddEndDA() {
        DynamicArray testDA = new DynamicArray();
        StopWatch timer = new StopWatch();
        timer.start();
        for (int i = 0; i < 107500000; i++) {
            testDA.add(testDA.size(), 2);
        }
        out.println("-------- Addition to the end --------");
        out.println("DynamicArray.add(size,e):" + timer.getElapsedTime() + "ms");
    }

    public static void testAddEndAL() {
        ArrayList<Integer> testAL = new ArrayList<>();
        StopWatch timer = new StopWatch();
        timer.start();
        for (int i = 0; i < 107500000; i++) {
            testAL.add(testAL.size(), 2);
        }
        out.println("ArrayList.add(size,e):" + timer.getElapsedTime() + "ms");
    }

    public static void testRemoveBeginningDA() {
        DynamicArray testDA = new DynamicArray();
        for (int i = 0; i < 400000; i++) {
            testDA.add(2);
        }
        StopWatch timer = new StopWatch();
        timer.start();
        for (int i = 0; i < 400000; i++) {
            testDA.remove(0);
        }
        out.println("-------- Remove from the beginning --------");
        out.println("DynamicArray.remove(0):" + timer.getElapsedTime() + "ms");
    }

    public static void testRemoveBeginningAL() {
        ArrayList<Integer> testAL = new ArrayList<>();
        for (int i = 0; i < 400000; i++) {
            testAL.add(2);
        }
        StopWatch timer = new StopWatch();
        timer.start();
        for (int i = 0; i < 400000; i++) {
            testAL.remove(0);
        }
        out.println("ArrayList.remove(0):" + timer.getElapsedTime() + "ms");
    }

    public static void testRemoveMiddleDA() {
        DynamicArray testDA = new DynamicArray();
        for (int i = 0; i < 400010; i++) {
            testDA.add(2);
        }
        StopWatch timer = new StopWatch();
        timer.start();
        for (int i = 400000; i > 4; i--) {
            testDA.remove(i / 2);
        }
        out.println("-------- Remove from the middle --------");
        out.println("DynamicArray.remove(i):" + timer.getElapsedTime() + "ms");
    }

    public static void testRemoveMiddleAL() {
        ArrayList<Integer> testAL = new ArrayList<>();
        for (int i = 0; i < 400010; i++) {
            testAL.add(2);
        }
        StopWatch timer = new StopWatch();
        timer.start();
        for (int i = 400000; i > 4; i--) {
            testAL.remove(i / 2);
        }
        out.println("ArrayList.remove(i):" + timer.getElapsedTime() + "ms");
    }

    public static void testRemoveEndDA() {
        DynamicArray testDA = new DynamicArray();
        byte testVar = 1;
        for (int i = 0; i < 500000000; i++) {
            testDA.add(testVar);
        }
        StopWatch timer = new StopWatch();
        timer.start();
        for (int i = testDA.size() - 1; i > 0; i--) {
            testDA.remove(i);
        }
        out.println("-------- Remove from the end --------");
        out.println("DynamicArray.remove(last):" + timer.getElapsedTime() + "ms");
    }

    public static void testRemoveEndAL() {
        ArrayList<Byte> testAL = new ArrayList<>();
        byte testVar = 1;
        for (int i = 0; i < 500000000; i++) {
            testAL.add(testVar);
        }
        StopWatch timer = new StopWatch();
        timer.start();
        for (int i = testAL.size() - 1; i > 0; i--) {
            testAL.remove(i);
        }
        out.println("ArrayList.remove(last):" + timer.getElapsedTime() + "ms");
    }
}
