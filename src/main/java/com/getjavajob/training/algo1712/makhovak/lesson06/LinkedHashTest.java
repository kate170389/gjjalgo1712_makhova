package com.getjavajob.training.algo1712.makhovak.lesson06;

import java.util.*;

import static com.getjavajob.training.util.Assert.assertEquals;

public class LinkedHashTest {
    public static void main(String[] args) {
        testSetAdd();
        testMapPut();
    }

    private static void testMapPut() {
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "1");
        map.put(5, "5");
        map.put(2, "2");
        Map<Integer, String> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put(1, "1");
        linkedHashMap.put(5, "5");
        linkedHashMap.put(2, "2");
        String exp = "{1=1, 5=5, 2=2}";
        assertEquals("LinkedHashTest.testMapPut", exp, linkedHashMap.toString());
        assertEquals("LinkedHashTest.testHashMapNotEqualsLinkedHashMap", false,
                linkedHashMap.toString().equals(map.toString()));
    }

    private static void testSetAdd() {
        Set<Integer> set = new HashSet<>();
        set.add(1);
        set.add(5);
        set.add(2);
        Set<Integer> linkedHashSet = new LinkedHashSet<>();
        linkedHashSet.add(1);
        linkedHashSet.add(5);
        linkedHashSet.add(2);
        Object[] exp = {1, 5, 2};
        assertEquals("LinkedHashTest.testSetAdd", exp, linkedHashSet.toArray());
        assertEquals("LinkedHashTest.testHashSetNotEqualsLinkedHashSet", false,
                Arrays.equals(linkedHashSet.toArray(), set.toArray()));
    }
}