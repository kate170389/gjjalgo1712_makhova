package com.getjavajob.training.algo1712.makhovak.lesson08.tree.binary.search;

import com.getjavajob.training.algo1712.makhovak.lesson07.tree.Node;
import com.getjavajob.training.algo1712.makhovak.lesson07.tree.binary.LinkedBinaryTree;

import java.util.Comparator;

public class BinarySearchTree<E> extends LinkedBinaryTree<E> {
    private Comparator<E> comparator;

    public BinarySearchTree() {
        this(null);
    }

    public BinarySearchTree(Comparator<E> comparator) {
        this.comparator = comparator;
    }

    /**
     * Method for comparing two values
     *
     */
    protected int compare(E val1, E val2) {
        return comparator == null && val1 instanceof Comparable ?
                ((Comparable) val1).compareTo(val2) : comparator.compare(val1, val2);
    }

    /**
     * Returns the node in n's subtree by val
     *
     */
    public Node<E> treeSearch(Node<E> n, E val) {
        Node<E> node = validate(n);
        int compare;
        while (node != null && (((compare = compare(node.getElement(), val))) != 0)) {
            node = compare == -1 ? right(node) : left(node);
        }
        return node;
    }

    @Override
    public Node<E> add(Node<E> node, E e) throws IllegalArgumentException {
        if (node != root()) {
            throw new IllegalArgumentException("Node must be only root");
        }
        add(e);
        return null;
    }

    @Override
    public Node<E> addLeft(Node<E> node, E e) {
        throw new IllegalStateException("use add for this tree");
    }

    @Override
    public Node<E> addRight(Node<E> node, E e) {
        throw new IllegalStateException("use add for this tree");
    }

    public Node<E> add(E val) {
        Node<E> node = root();
        Node<E> parent = node;
        while (node != null) {
            int compare = compare(node.getElement(), val);
            if (compare == 0) {
                return null;
            }
            parent = node;
            node = compare == -1 ? right(node) : left(node);
        }
        if (parent == null) {
            addRoot(val);
            return root;
        } else {
            node = compare(parent.getElement(), val) == 1 ? super.addLeft(parent, val) : super.addRight(parent, val);
        }
        return node;
    }

    public E remove(E val) {
        Node<E> node = treeSearch(root(), val);
        E prev = super.remove(node);
        if (prev != null) {
            return prev;
        } else {
            prev = node.getElement();
            Node<E> newNode = right(node);
            NodeImpl<E> y = null;
            while (left(newNode) != null) {
                y = validate(newNode);
                newNode = left(newNode);
            }
            set(node, newNode.getElement());
            if (y != null) {
                y.setLeft(((NodeImpl) newNode).getRight());
            } else {
                validate(node).setRight(right(newNode) == null ? null : validate(right(newNode)));
            }
            size--;
            return prev;
        }
    }

    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        return remove(n.getElement());
    }

    public String toString() {
        StringBuilder str = new StringBuilder();
        if (isEmpty()) {
            return str.append("()").toString();
        }
        str.append(root.toString());
        toString(str, root());
        return str.toString();
    }

    private void toString(StringBuilder str, Node node) {
        if (node != null) {
            if (left(node) == null && right(node) == null) {
                return;
            }
            if (left(node) == null && childrenNumber(right(node)) == 0 ||
                    right(node) == null && childrenNumber(left(node)) == 0) {
                str.append("(" + left(node) + "," + right(node) + ")");
            } else {
                str.append("(" + left(node));
                toString(str, left(node));
                str.append("," + right(node));
                toString(str, right(node));
                str.append(')');
            }
        }
    }

    protected void afterElementRemoved(Node<E> n) {
        throw new UnsupportedOperationException("Unsupported Operation Exception");
    }

    protected void afterElementAdded(Node<E> n) {
        throw new UnsupportedOperationException("Unsupported Operation Exception");
    }
}