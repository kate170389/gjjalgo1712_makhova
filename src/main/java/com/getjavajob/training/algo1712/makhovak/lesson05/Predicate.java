package com.getjavajob.training.algo1712.makhovak.lesson05;

public interface Predicate<T> {
    boolean evaluate(T o);
}