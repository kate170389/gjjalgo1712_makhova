package com.getjavajob.training.algo1712.makhovak.lesson09;

import java.util.Arrays;
import java.util.NavigableSet;
import java.util.TreeSet;

import static com.getjavajob.training.util.Assert.assertEquals;

public class CityStorageTest {
    public static void main(String[] args) {
        testAdd();
        testRangeOfCities();
        testRangeOfCitiesMaxValueOne();
        testRangeOfCitiesMaxValueTwo();
        testRangeOfCitiesMaxValueThree();
    }

    private static void testRangeOfCitiesMaxValueThree() {
        String[] cities = new String[]{Character.MAX_VALUE + "w",
                "" + Character.MAX_VALUE + Character.MAX_VALUE + Character.MAX_VALUE,
                "Moscow"};
        CityStorage storage = new CityStorage();
        storage.add(cities);
        NavigableSet<String> expected = new TreeSet<>();
        expected.add("\uFFFFw");
        expected.add("\uFFFF\uFFFF\uFFFF");
        assertEquals("CityStorageTest.testRangeOfCitiesMaxValueThree", expected,
                storage.rangeOfCities("" + Character.MAX_VALUE, cities));
    }

    private static void testRangeOfCitiesMaxValueTwo() {
        String[] cities = new String[]{"ab", "ABC", "aB" + Character.MAX_VALUE,
                "aB" + Character.MAX_VALUE + Character.MAX_VALUE, "ac",
                "ab" + Character.MAX_VALUE + Character.MAX_VALUE + "er"};
        CityStorage storage = new CityStorage();
        storage.add(cities);
        NavigableSet<String> expected = new TreeSet<>();
        expected.add("ab" + Character.MAX_VALUE + Character.MAX_VALUE + "er");
        expected.add("aB" + Character.MAX_VALUE + Character.MAX_VALUE);
        assertEquals("CityStorageTest.testRangeOfCitiesMaxValueTwo", expected,
                storage.rangeOfCities("ab" + Character.MAX_VALUE + Character.MAX_VALUE, cities));
    }

    private static void testRangeOfCitiesMaxValueOne() {
        String[] cities = new String[]{"ab", "ABC", "aB" + Character.MAX_VALUE,
                "aB" + Character.MAX_VALUE + Character.MAX_VALUE, "ac", "ab" + Character.MAX_VALUE + "er"};
        CityStorage storage = new CityStorage();
        storage.add(cities);
        NavigableSet<String> expected = new TreeSet<>();
        expected.add("aB" + Character.MAX_VALUE);
        expected.add("aB" + Character.MAX_VALUE + Character.MAX_VALUE);
        expected.add("ab" + Character.MAX_VALUE + "er");
        assertEquals("CityStorageTest.testRangeOfCitiesMaxValueOne", expected,
                storage.rangeOfCities("ab" + Character.MAX_VALUE, cities));
    }

    private static void testRangeOfCities() {
        String[] cities = new String[]{"mo", "Moscow", "mogilev", "mn", "mp", "Mo" + Character.MAX_VALUE +
                Character.MAX_VALUE + "df", "Mo" + Character.MAX_VALUE + Character.MAX_VALUE + "dp", "Mn" +
                Character.MAX_VALUE + Character.MAX_VALUE + "dp", Character.MAX_VALUE + "d",};
        CityStorage storage = new CityStorage();
        storage.add(cities);

        NavigableSet<String> expected = new TreeSet<>();
        expected.add("mo");
        expected.add("mogilev");
        expected.add("Moscow");
        expected.add("Mo" + Character.MAX_VALUE + Character.MAX_VALUE + "dp");
        expected.add("Mo" + Character.MAX_VALUE + Character.MAX_VALUE + "df");
        assertEquals("CityStorageTest.testRangeOfCities", expected,
                storage.rangeOfCities("MO", cities));
    }

    private static void testAdd() {
        NavigableSet<String> tree = new TreeSet<>();
        tree.add("10");
        tree.add("15");
        tree.add("8");
        tree.add("23");
        tree.add("1");
        CityStorage actual = new CityStorage();
        actual.add("1");
        actual.add("8");
        actual.add("10");
        actual.add("15");
        actual.add("23");
        assertEquals("CityStorageTest.testAdd", tree, actual.getTree());
        String[] cities = new String[]{"mo", "Moscow", "mogilev", "mn", "mp", "Mo" + Character.MAX_VALUE +
                Character.MAX_VALUE + "df", "Mo" + Character.MAX_VALUE + Character.MAX_VALUE + "dp", "Mn" +
                Character.MAX_VALUE + Character.MAX_VALUE + "dp", Character.MAX_VALUE + "d",};
        tree = new TreeSet<>(Arrays.asList(cities));
        CityStorage storage = new CityStorage();
        storage.add(cities);
        assertEquals("CityStorageTest.testAddArray", tree, storage.getTree());
    }
}