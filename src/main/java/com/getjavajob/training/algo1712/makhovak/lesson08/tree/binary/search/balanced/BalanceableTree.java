package com.getjavajob.training.algo1712.makhovak.lesson08.tree.binary.search.balanced;

import com.getjavajob.training.algo1712.makhovak.lesson07.tree.Node;
import com.getjavajob.training.algo1712.makhovak.lesson08.tree.binary.search.BinarySearchTree;

public abstract class BalanceableTree<E> extends BinarySearchTree<E> {

    /**
     * Sets new relationship between parent and child. This method is used by
     * {@link #rotate(com.getjavajob.training.algo1712.makhovak.lesson07.tree.Node)} for node and its grandparent,
     * node and its parent, node's child and node's parent relinking.
     *
     * @param parent        new parent
     * @param child         new child
     * @param makeLeftChild whether new child must be left or right
     */
    private void relink(NodeImpl<E> parent, NodeImpl<E> child, boolean makeLeftChild) {
        if (child != null) {
            NodeImpl<E> newParent = child.getParent();
            if (newParent == null) {
                root = parent;
            }
            child.setParent(parent);
            parent.setParent(newParent);
        }
        if (makeLeftChild) {
            parent.setLeft(child);
        } else {
            parent.setRight(child);
        }
    }

    /**
     * Rotates n with it's parent.
     *
     * @param n node to rotate above its parent
     */
    protected void rotate(Node<E> n) {
        NodeImpl<E> node = validate(n);
        NodeImpl<E> parent = node.getParent();
        NodeImpl<E> grandParent = parent.getParent();
        boolean makeLeft = left(parent(node)) == node;
        NodeImpl<E> child = makeLeft ? node.getRight() : node.getLeft();
        relink(node, parent, !makeLeft);
        relink(parent, child, makeLeft);
        if (grandParent != null) {
            if (grandParent.getRight() == parent) {
                grandParent.setRight(node);
            } else {
                grandParent.setLeft(node);
            }
        }
    }

    /**
     * Performs one rotation of <i>n</i>'s parent node or two rotations of <i>n</i> by the means of
     * {@link #rotate(com.getjavajob.training.algo1712.makhovak.lesson07.tree.Node)} to reduce the height of subtree rooted at <i>n1</i>
     * <p>
     * <pre>
     *     n1         n2           n1           n
     *    /          /  \         /            / \
     *   n2    ==>  n   n1  or  n2     ==>   n2   n1
     *  /                         \
     * n                           n
     *
     * </pre>
     * <p>
     * Similarly for subtree with right side children.
     *
     * @param n grand child of subtree root node
     * @return new subtree root
     */
    protected Node<E> reduceSubtreeHeight(Node<E> n) {
        NodeImpl<E> node = validate(n);
        if (node == root || parent(node.getParent()) == null) {
            return null;
        }
        NodeImpl<E> parent = node.getParent();
        if (node == right(parent) && parent == left(parent(parent)) ||
                node == left(parent) && parent == right(parent(parent))) {
            rotate(node);
        } else {
            node = node.getParent();
        }
        rotate(node);
        return node;
    }
}