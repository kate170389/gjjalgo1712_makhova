package com.getjavajob.training.algo1712.makhovak.lesson07.tree.binary;

import com.getjavajob.training.algo1712.makhovak.lesson07.tree.Node;

import java.util.Iterator;

import static com.getjavajob.training.util.Assert.assertEquals;
import static com.getjavajob.training.util.Assert.fail;

public class ArrayBinaryTreeTest {
    public static void main(String[] args) {
        testAddRoot();
        testValidate();
        testAddLeft();
        testAddRight();
        testAdd();
        testSet();
        testRemove();
        testParent();
        testSize();
        testNodes();
        testSibling();
        testChildren();
        tetChildrenNumber();
        testPreOrder();
        testPostOrder();
        testInOrder();
        testBreadthFirst();
        testIsInternal();
        testIsExternal();
        testIsRoot();
        testIsEmpty();
        testIterator();
    }

    private static void testIterator() {
        try {
            ArrayBinaryTree<Integer> tree = new ArrayBinaryTree<>();
            tree.addRoot(1);
            tree.add(tree.root(), 2);
            tree.add(tree.root(), 3);
            tree.add(tree.left(tree.root()), 4);
            Iterator<Integer> it = tree.iterator();
            assertEquals("ArrayBinaryTreeTest.testIteratorHasNext", true, it.hasNext());
            assertEquals("ArrayBinaryTreeTest.testIteratorNext", tree.root().toString(),
                    it.next().toString());
            it.remove();
            fail("There are not UnsupportedOperationException");
        } catch (UnsupportedOperationException e) {
            assertEquals("ArrayBinaryTreeTest.testIteratorRemoveException",
                    "UnsupportedOperationException remove", e.getMessage());
        }
    }

    private static void testIsEmpty() {
        BinaryTree<Integer> tree = new ArrayBinaryTree<>();
        assertEquals("ArrayBinaryTreeTest.testIsEmpty", true, tree.isEmpty());
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        tree.add(tree.left(tree.root()), 4);
        assertEquals("ArrayBinaryTreeTest.testIsNotEmpty", false, tree.isEmpty());
    }

    private static void testIsRoot() {
        BinaryTree<Integer> tree = new ArrayBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        tree.add(tree.left(tree.root()), 4);
        assertEquals("ArrayBinaryTreeTest.testIsRoot", true, tree.isRoot(tree.root()));
        assertEquals("ArrayBinaryTreeTest.testIsNotRoot", false, tree.isRoot(tree.right(tree.root())));
    }

    private static void testIsExternal() {
        BinaryTree<Integer> tree = new ArrayBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        tree.add(tree.left(tree.root()), 4);
        assertEquals("ArrayBinaryTreeTest.testIsNotExternal", false, tree.isExternal(tree.root()));
        assertEquals("ArrayBinaryTreeTest.testIsExternal", true, tree.isExternal(tree.right(tree.root())));
    }

    private static void testIsInternal() {
        BinaryTree<Integer> tree = new ArrayBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        tree.add(tree.left(tree.root()), 4);
        assertEquals("ArrayBinaryTreeTest.testIsInternal", true, tree.isInternal(tree.left(tree.root())));
        assertEquals("ArrayBinaryTreeTest.testIsNotInternal", false, tree.isInternal(tree.right(tree.root())));
    }

    private static void testBreadthFirst() {
        ArrayBinaryTree<Integer> tree = new ArrayBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        tree.add(tree.left(tree.root()), 4);
        tree.add(tree.left(tree.root()), 5);
        tree.add(tree.right(tree.root()), 6);
        String actual = "[1, 2, 3, 4, 5, 6]";
        assertEquals("ArrayBinaryTreeTest.testBreadthFirst", actual, tree.breadthFirst().toString());
    }

    private static void testPreOrder() {
        ArrayBinaryTree<Integer> tree = new ArrayBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        tree.add(tree.left(tree.root()), 4);
        tree.add(tree.left(tree.root()), 5);
        tree.add(tree.right(tree.root()), 6);
        String actual = "[1, 2, 4, 5, 3, 6]";
        assertEquals("ArrayBinaryTreeTest.testPreOrder", actual, tree.preOrder().toString());
    }

    private static void testPostOrder() {
        ArrayBinaryTree<Integer> tree = new ArrayBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        tree.add(tree.left(tree.root()), 4);
        tree.add(tree.left(tree.root()), 5);
        tree.add(tree.right(tree.root()), 6);
        String actual = "[4, 5, 2, 6, 3, 1]";
        assertEquals("ArrayBinaryTreeTest.testPostOrder", actual, tree.postOrder().toString());
    }

    private static void tetChildrenNumber() {
        BinaryTree<Integer> tree = new ArrayBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        assertEquals("ArrayBinaryTreeTest.tetChildrenNumber", 2, tree.childrenNumber(tree.root()));
    }

    private static void testChildren() {
        BinaryTree<Integer> tree = new ArrayBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        String expected = "[2, 3]";
        assertEquals("ArrayBinaryTreeTest.testChildren", expected, tree.children(tree.root()).toString());
    }

    private static void testSibling() {
        BinaryTree<Integer> tree = new ArrayBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        tree.add(tree.left(tree.root()), 4);
        assertEquals("ArrayBinaryTreeTest.testSiblingNull", null, tree.sibling(null));
        assertEquals("ArrayBinaryTreeTest.testSiblingRoot", null, tree.sibling(tree.root()));
        assertEquals("ArrayBinaryTreeTest.testSibling", tree.right(tree.root()),
                tree.sibling(tree.left(tree.root())));
        assertEquals("ArrayBinaryTreeTest.testNotSibling", null,
                tree.sibling(tree.left(tree.left(tree.root()))));
    }

    private static void testNodes() {
        BinaryTree<Integer> tree = new ArrayBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        tree.add(tree.left(tree.root()), 4);
        tree.add(tree.left(tree.root()), 5);
        tree.add(tree.right(tree.root()), 6);
        String actual = "[1, 2, 4, 5, 3, 6]";
        assertEquals("ArrayBinaryTreeTest.testNodes", actual, tree.nodes().toString());
    }

    private static void testInOrder() {
        ArrayBinaryTree<Integer> tree = new ArrayBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        tree.add(tree.left(tree.root()), 4);
        tree.add(tree.left(tree.root()), 5);
        tree.add(tree.right(tree.root()), 6);
        String actual = "[4, 2, 5, 1, 6, 3]";
        assertEquals("ArrayBinaryTreeTest.testInOrder", actual, tree.inOrder().toString());
    }

    private static void testSize() {
        BinaryTree<Integer> tree = new ArrayBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        tree.add(tree.left(tree.root()), 4);
        tree.add(tree.left(tree.root()), 5);
        tree.add(tree.right(tree.root()), 6);
        assertEquals("ArrayBinaryTreeTest.testSize", 6, tree.size());
    }

    private static void testParent() {
        BinaryTree<Integer> tree = new ArrayBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        assertEquals("ArrayBinaryTreeTest.testParent", tree.root(), tree.parent(tree.right(tree.root())));
        assertEquals("ArrayBinaryTreeTest.testRootParent", null, tree.parent(tree.root()));
    }

    private static void testRemove() {
        BinaryTree<Integer> tree = new ArrayBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        assertEquals("ArrayBinaryTreeTest.testNotRemove", null, tree.remove(tree.root()));
        assertEquals("ArrayBinaryTreeTest.testRemove", (Integer) 2, tree.remove(tree.left(tree.root())));
        tree.remove(tree.right(tree.root()));
        assertEquals("ArrayBinaryTreeTest.testRemoveRoot", (Integer) 1, tree.remove(tree.root()));
    }

    private static void testSet() {
        BinaryTree<Integer> tree = new ArrayBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        assertEquals("ArrayBinaryTreeTest.testSet", (Integer) 2, tree.set(tree.left(tree.root()), 5));
    }

    private static void testAdd() {
        BinaryTree<Integer> tree = new ArrayBinaryTree<>();
        tree.addRoot(1);
        Node<Integer> actual = tree.add(tree.root(), 2);
        assertEquals("ArrayBinaryTreeTest.testAddOne", tree.left(tree.root()), actual);
        actual = tree.add(tree.root(), 3);
        assertEquals("ArrayBinaryTreeTest.testAddTwo", tree.right(tree.root()), actual);
        actual = tree.add(tree.root(), 4);
        assertEquals("ArrayBinaryTreeTest.testNotAdd", null, actual);
    }

    private static void testAddRight() {
        BinaryTree<Integer> tree = new ArrayBinaryTree<>();
        tree.addRoot(1);
        Node<Integer> actual = tree.addRight(tree.root(), 2);
        assertEquals("ArrayBinaryTreeTest.testAddRight", tree.right(tree.root()), actual);
    }

    private static void testAddLeft() {
        BinaryTree<Integer> tree = new ArrayBinaryTree<>();
        tree.addRoot(1);
        Node<Integer> actual = tree.addLeft(tree.root(), 2);
        assertEquals("ArrayBinaryTreeTest.testAddLeft", tree.left(tree.root()), actual);
    }

    private static void testValidate() {
        try {
            ArrayBinaryTree<Integer> tree = new ArrayBinaryTree<>();
            Node<Integer> node = new ArrayBinaryTree.NodeImpl<>(2);
            assertEquals("ArrayBinaryTreeTest.testValidate",
                    node, tree.validate(node));
            tree.validate(null);
            tree.validate(node);
            fail("There are not IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            assertEquals("ArrayBinaryTreeTest.testValidateException",
                    "IllegalArgumentException: the node is not an instance of supported", e.getMessage());
        }
    }

    private static void testAddRoot() {
        try {
            BinaryTree<Integer> tree = new ArrayBinaryTree<>();
            String actualStr = "element:" + tree.addRoot(1);
            String expectedStr = "element:1";
            assertEquals("ArrayBinaryTreeTest.testAddRoot", expectedStr, actualStr);
            tree.addRoot(2);
            fail("There are not IllegalStateException");
        } catch (IllegalStateException e) {
            assertEquals("linkedBinaryTreeTest.testAddRootException",
                    "IllegalStateException: root already exists", e.getMessage());
        }
    }
}