package com.getjavajob.training.algo1712.makhovak.lesson09.tree.binary.search.balanced;

import com.getjavajob.training.algo1712.makhovak.lesson08.tree.binary.search.balanced.BalanceableTree;

import static com.getjavajob.training.util.Assert.assertEquals;

public class RedBlackTreeTest {
    public static void main(String[] args) {
        testAddCaseOne();
        testAddCaseTwo();
        testAddCaseThree();
        testAddCaseFour();
        testRemoveCase1();
        testRemoveCase2();
        testRemoveCase3();
        testRemoveCase4();
        testRemoveCase5();
        testRemoveCase6();
        testRemoveCase7();
        testRemoveCase8();
    }
    /**
     * In a regular binary search tree when deleting a node with two non-leaf children,
     * we find either the maximum element in its left subtree and move its value into the node being deleted.
     * We then delete the node we copied the value from, which must have fewer than two non-leaf children.
     * this reduces to the problem of deleting a node with at most one non-leaf child.
     */

    /**
     * Node is black with 1 red child(simply remove and change color)
     */
    private static void testRemoveCase8() {
        BalanceableTree<Integer> tree = new RedBlackTree<>();
        tree.add(20);
        tree.add(10);
        tree.add(30);
        tree.add(25);
        tree.remove(30);
        String expected = "20 black(10 black,25 black)";
        assertEquals("RedBlackTreeTest.testRemoveBlackWithRedChild", expected, tree.toString());
    }

    /**
     * RightLeftCase (two rotations)
     * <pre>
     *                10B                              30B
     *             /        \                        /       \
     *        -30B          50B         ==>        10B       50B
     *        /  \        /    \                  /  \       /  \
     * (del)-40B  -20B    30R   70B              -30B 15B   40B  70B
     *                  /  \                      \
     *                15B  40B                    -20R
     * <pre>
     */
    private static void testRemoveCase7() {
        RedBlackTree<Integer> tree = new RedBlackTree<>();
        tree.addForTest(10, true);
        tree.addForTest(-30, true);
        tree.addForTest(-40, true);
        tree.addForTest(-20, true);
        tree.addForTest(50, true);
        tree.addForTest(30, false);
        tree.addForTest(15, true);
        tree.addForTest(40, true);
        tree.addForTest(70, true);
        tree.remove(-40);
        String expected = "30 black(10 black(-30 black(null,-20 red),15 black),50 black(40 black,70 black))";
        assertEquals("RedBlackTreeTest.testRemoveBlackSheetTwoRotations", expected, tree.toString());
    }

    /**
     * LeftLeftCase (one rotation)
     * <pre>
     *         20B                  30B
     *         / \                 / \
     * (del)10B  30B   ==>      20B  32B
     *          /  \              \
     *        25R  32R            25R
     *
     * <pre>
     */
    private static void testRemoveCase6() {
        BalanceableTree<Integer> tree = new RedBlackTree<>();
        tree.add(20);
        tree.add(10);
        tree.add(30);
        tree.add(25);
        tree.add(32);
        tree.remove(10);
        String expected = "30 black(20 black(null,25 red),32 black)";
        assertEquals("RedBlackTreeTest.testRemoveBlackSheetWithSiblingBlackWithChild",
                expected, tree.toString());
    }

    /**
     * change color
     * <pre>
     *         20B (del)               22B                   22B
     *         / \                    / \                    / \
     *      10B  30R   ==>          10B  30R      ==>     10B  30B
     *           / \                     / \                     \
     *         22B 35B           (del)22B  35B                   35R
     *
     * <pre>
     */
    private static void testRemoveCase5() {
        RedBlackTree<Integer> tree = new RedBlackTree<>();
        tree.addForTest(20, true);
        tree.addForTest(10, true);
        tree.addForTest(30, false);
        tree.addForTest(22, true);
        tree.addForTest(35, true);
        tree.remove(20);
        String expected = "22 black(10 black,30 black(null,35 red))";
        assertEquals("RedBlackTreeTest.testRemoveBlackSheetWithSiblingBlack", expected, tree.toString());
    }

    /**
     * change color
     * <pre>
     *         20B                     20B
     *         / \                    / \
     *      10R  30B   ==>          10B  30B
     *     /  \                       \
     * (del)5B  12B                   12R
     *
     * <pre>
     */
    private static void testRemoveCase4() {
        RedBlackTree<Integer> tree = new RedBlackTree<>();
        tree.addForTest(20, true);
        tree.addForTest(10, false);
        tree.addForTest(30, true);
        tree.addForTest(12, true);
        tree.addForTest(5, true);
        tree.remove(5);
        String expected = "20 black(10 black(null,12 red),30 black)";
        assertEquals("RedBlackTreeTest.testRemoveBlackSheetWithSiblingBlack", expected, tree.toString());
    }

    /**
     * RightRightCase (one rotation)
     * <pre>
     *         20B                     10B
     *         / \                    / \
     *      10R  30B(del)   ==>     5B  20B
     *     /  \                         /
     *    5B  12B                    12R
     *
     * <pre>
     */
    private static void testRemoveCase3() {
        RedBlackTree<Integer> tree = new RedBlackTree<>();
        tree.addForTest(20, true);
        tree.addForTest(10, false);
        tree.addForTest(30, true);
        tree.addForTest(12, true);
        tree.addForTest(5, true);
        tree.remove(30);
        String expected = "10 black(5 black,20 black(12 red,null))";
        assertEquals("RedBlackTreeTest.testRemoveBlackSheetWithSiblingRed", expected, tree.toString());
    }

    /**
     * red sheet removal (nothing changes)
     */
    private static void testRemoveCase2() {
        BalanceableTree<Integer> tree = new RedBlackTree<>();
        tree.add(20);
        tree.add(10);
        tree.add(30);
        tree.remove(10);
        String expected = "20 black(null,30 red)";
        assertEquals("RedBlackTreeTest.testRemoveRedSheet", expected, tree.toString());
    }

    /**
     * root removal without sheets
     */
    private static void testRemoveCase1() {
        BalanceableTree<Integer> tree = new RedBlackTree<>();
        tree.add(20);
        tree.remove(20);
        String expected = "()";
        assertEquals("RedBlackTreeTest.testRemoveRoot", expected, tree.toString());
    }

    /**
     * LeftRightCase (two rotations)
     * <pre>
     *         20B                20B               12B
     *         / \               / \                / \
     *      10R  30B   ==>     12R  30B  ==>    10R   20R
     *        \                /                        \
     *        12R add        10R                        30B
     *
     * <pre>
     */
    private static void testAddCaseFour() {
        RedBlackTree<Integer> tree = new RedBlackTree<>();
        tree.addForTest(20, true);
        tree.addForTest(10, false);
        tree.addForTest(30, true);
        tree.add(12);
        String expected = "12 black(10 red,20 red(null,30 black))";
        assertEquals("RedBlackTreeTest.testAddCaseFour", expected, tree.toString());
    }

    /**
     * RightRightCase (one rotation)
     * <pre>
     *         20B                20B
     *         / \               / \
     *      10B  30B   ==>     5B  30B
     *     /                  /  \
     *    5R               4R    10R
     *   /
     *  4R add
     * <pre>
     */
    private static void testAddCaseThree() {
        BalanceableTree<Integer> tree = new RedBlackTree<>();
        tree.add(20);
        tree.add(10);
        tree.add(30);
        tree.add(5);
        tree.add(4);
        String expected = "20 black(5 black(4 red,10 red),30 black)";
        assertEquals("RedBlackTreeTest.testAddCaseThree", expected, tree.toString());
    }

    /**
     * <pre>
     *     B                B
     *    / \              / \
     *   R  R   ==>       B   B
     *  /                /
     * add              R
     * <pre>
     */
    private static void testAddCaseTwo() {
        BalanceableTree<Integer> tree = new RedBlackTree<>();
        tree.add(20);
        tree.add(10);
        tree.add(30);
        tree.add(5);
        String expected = "20 black(10 black(5 red,null),30 black)";
        assertEquals("RedBlackTreeTest.testAddCaseTwo", expected, tree.toString());
    }

    /**
     * <pre>
     *    B                B
     *   / \              / \
     *  R  add   ==>     R   R
     * <pre>
     */
    private static void testAddCaseOne() {
        BalanceableTree<Integer> tree = new RedBlackTree<>();
        tree.add(20);
        tree.add(10);
        tree.add(30);
        String expected = "20 black(10 red,30 red)";
        assertEquals("RedBlackTreeTest.testAddCaseOne", expected, tree.toString());
    }
}