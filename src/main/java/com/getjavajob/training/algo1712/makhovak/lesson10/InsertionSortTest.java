package com.getjavajob.training.algo1712.makhovak.lesson10;

import static com.getjavajob.training.util.Assert.assertEquals;

public class InsertionSortTest {
    public static void main(String[] args) {
        testSortOne();
        testSortTwo();
    }

    private static void testSortTwo() {
        InsertionSort insertion = new InsertionSort();
        int[] array = new int[]{1, 2, 3, 4, 4, 5};
        int[] expected = array;
        insertion.sort(array);
        assertEquals("InsertionSortTest.testSortTwoAlreadySorted", expected, array);
    }

    private static void testSortOne() {
        InsertionSort insertion = new InsertionSort();
        int[] array = new int[]{3, 7, 4, 9, 5, 2, 6, 1};
        insertion.sort(array);
        int[] expected = new int[]{1, 2, 3, 4, 5, 6, 7, 9};
        assertEquals("InsertionSortTest.testSortOne", expected, array);
    }
}