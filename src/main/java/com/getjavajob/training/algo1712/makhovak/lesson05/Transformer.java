package com.getjavajob.training.algo1712.makhovak.lesson05;

public interface Transformer<I, O> {
    O transform(I object);
}