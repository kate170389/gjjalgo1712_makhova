package com.getjavajob.training.algo1712.makhovak.lesson05;

import java.util.ArrayDeque;
import java.util.Deque;

import static com.getjavajob.training.util.Assert.assertEquals;

public class DequeTest {
    public static void main(String[] args) {
        testAddFirst();
        testAddLast();
        testOfferFirst();
        testOfferLast();
        testRemoveFirst();
        testRemoveLast();
        testPollFirst();
        testPollLast();
        testGetFirst();
        testGetLast();
        testPeekFirst();
        testPeekLast();
        testPop();
        testPush();
        testRemoveFirstOccurrence();
        testRemoveLastOccurrence();
        testContains();
    }

    private static void testContains() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("one");
        deque.add("two");
        deque.add("three");
        assertEquals("DequeTest.testContainsTrue", true, deque.contains("two"));
        assertEquals("DequeTest.testContainsFalse", false, deque.contains("four"));
        assertEquals("DequeTest.testContainsNull", false, deque.contains(null));
    }

    private static void testRemoveLastOccurrence() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("one");
        deque.add("two");
        deque.add("three");
        deque.add("four");
        deque.add("two");
        deque.add("five");
        assertEquals("DequeTest.testRemoveLastOccurrenceTrue", true,
                deque.removeLastOccurrence("two"));
        assertEquals("DequeTest.testRemoveLastOccurrenceFalse", false,
                deque.removeLastOccurrence("six"));
        String[] test = {"one", "two", "three", "four", "five"};
        assertEquals("DequeTest.testRemoveLastOccurrence", test, deque.toArray());
    }

    private static void testRemoveFirstOccurrence() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("one");
        deque.add("two");
        deque.add("three");
        deque.add("two");
        deque.add("two");
        assertEquals("DequeTest.testRemoveFirstOccurrenceTrue", true,
                deque.removeFirstOccurrence("two"));
        assertEquals("DequeTest.testRemoveFirstOccurrenceFalse", false,
                deque.removeFirstOccurrence("five"));
        String[] test = {"one", "three", "two", "two"};
        assertEquals("DequeTest.testRemoveFirstOccurrence", test, deque.toArray());

    }

    private static void testPush() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("one");
        deque.add("two");
        deque.push("three");
        String[] test = {"three", "one", "two"};
        assertEquals("DequeTest.testPush", test, deque.toArray());
    }

    private static void testPop() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("one");
        deque.add("two");
        deque.add("three");
        String[] test = {"two", "three"};
        assertEquals("DequeTest.testPop", "one", deque.pop());
        assertEquals("DequeTest.testPop", test, deque.toArray());
    }

    private static void testPeekLast() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("one");
        deque.add("two");
        deque.add("three");
        String[] test = {"one", "two", "three"};
        Deque<String> array = new ArrayDeque<>();
        assertEquals("DequeTest.testPeekLast", "three", deque.peekLast());
        assertEquals("DequeTest.testPeekLastNull", null, array.peekLast());
        assertEquals("DequeTest.testPeekLast", test, deque.toArray());
    }

    private static void testPeekFirst() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("one");
        deque.add("two");
        deque.add("three");
        String[] test = {"one", "two", "three"};
        Deque<String> array = new ArrayDeque<>();
        assertEquals("DequeTest.testPeekFirst", "one", deque.peekFirst());
        assertEquals("DequeTest.testPeekFirstNull", null, array.peekFirst());
        assertEquals("DequeTest.testPeekFirst", test, deque.toArray());
    }

    private static void testGetLast() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("one");
        deque.add("two");
        deque.add("three");
        String[] test = {"one", "two", "three"};
        assertEquals("DequeTest.testGetLast", "three", deque.getLast());
        assertEquals("DequeTest.testRemoveLast", test, deque.toArray());
    }

    private static void testGetFirst() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("one");
        deque.add("two");
        deque.add("three");
        String[] test = {"one", "two", "three"};
        assertEquals("DequeTest.testGetFirst", "one", deque.getFirst());
        assertEquals("DequeTest.testRemoveLast", test, deque.toArray());
    }

    private static void testPollLast() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("one");
        deque.add("two");
        deque.add("three");
        deque.add("four");
        Deque<String> test = new ArrayDeque<>();
        String[] array = {"one", "two", "three"};
        assertEquals("DequeTest.testPollLast", "four", deque.pollLast());
        assertEquals("DequeTest.testPollLastNull", null, test.pollLast());
        assertEquals("DequeTest.testPollLast", array, deque.toArray());
    }

    private static void testPollFirst() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("one");
        deque.add("two");
        deque.add("three");
        deque.add("four");
        Deque<String> test = new ArrayDeque<>();
        String[] array = {"two", "three", "four"};
        assertEquals("DequeTest.testPollFirst", "one", deque.pollFirst());
        assertEquals("DequeTest.testPollFirstNull", null, test.pollFirst());
        assertEquals("DequeTest.testPollFirst", array, deque.toArray());
    }

    private static void testRemoveLast() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("one");
        deque.add("two");
        deque.add("three");
        deque.add("four");
        String[] test = {"one", "two", "three"};
        assertEquals("DequeTest.testRemoveLast", "four", deque.removeLast());
        assertEquals("DequeTest.testRemoveLast", test, deque.toArray());
    }

    private static void testRemoveFirst() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("one");
        deque.add("two");
        deque.add("three");
        deque.add("four");
        String[] test = {"two", "three", "four"};
        assertEquals("DequeTest.testRemoveFirst", "one", deque.removeFirst());
        assertEquals("DequeTest.testRemoveFirst", test, deque.toArray());
    }

    private static void testOfferLast() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("one");
        deque.add("two");
        deque.add("three");
        String[] test = {"one", "two", "three", "four"};
        assertEquals("DequeTest.testOfferLastTrue", true, deque.offerLast("four"));
        assertEquals("DequeTest.testOfferLast", test, deque.toArray());
    }

    private static void testOfferFirst() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("one");
        deque.add("two");
        deque.add("three");
        String[] test = {"four", "one", "two", "three"};
        assertEquals("DequeTest.testOfferFirstTrue", true, deque.offerFirst("four"));
        assertEquals("DequeTest.testOfferFirst", test, deque.toArray());
    }

    private static void testAddLast() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("one");
        deque.add("two");
        deque.add("three");
        deque.addLast("four");
        String[] test = {"one", "two", "three", "four"};
        assertEquals("DequeTest.testAddLast", test, deque.toArray());
    }

    private static void testAddFirst() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("one");
        deque.add("two");
        deque.add("three");
        deque.addFirst("four");
        String[] test = {"four", "one", "two", "three"};
        assertEquals("DequeTest.testAddFirst", test, deque.toArray());
    }
}