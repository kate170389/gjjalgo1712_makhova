package com.getjavajob.training.algo1712.makhovak.lesson07.tree.binary;

import com.getjavajob.training.algo1712.makhovak.lesson07.tree.Node;

import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;

/**
 * Concrete implementation of a binary tree using a node-based, linked structure
 *
 * @param <E> element
 */
public class LinkedBinaryTree<E> extends AbstractBinaryTree<E> {
    protected NodeImpl<E> root;
    protected int size;

    /**
     * Validates the node is an instance of supported {@link NodeImpl} type and casts to it
     *
     * @param n node
     * @return casted {@link NodeImpl} node
     * @throws IllegalArgumentException
     */
    protected NodeImpl<E> validate(Node<E> n) throws IllegalArgumentException {
        if (n == null || !(n instanceof NodeImpl)) {
            throw new IllegalArgumentException("IllegalArgumentException: the node is not an instance of supported");
        }
        return (NodeImpl<E>) n;
    }

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        if (!isEmpty()) {
            throw new IllegalStateException("IllegalStateException: root already exists");
        }
        size++;
        return this.root = new NodeImpl<>(e, null);
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        if (node.left == null) {
            return addLeft(node, e);
        }
        if (node.right == null) {
            return addRight(node, e);
        }
        return null;
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        if (node.left == null) {
            size++;
            return node.left = new NodeImpl<>(e, node);
        } else {
            throw new IllegalArgumentException("node already has a left child");
        }
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        if (node.right == null) {
            size++;
            return node.right = new NodeImpl<>(e, node);
        } else {
            throw new IllegalArgumentException("node already has a right child");
        }
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @param e element
     * @return replace element
     * @throws IllegalArgumentException
     */
    @Override
    public E set(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        E prev = node.element;
        node.element = e;
        return prev;
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param node node
     * @return replace element
     * @throws IllegalArgumentException
     */
    @Override
    public E remove(Node<E> node) throws IllegalArgumentException {
        NodeImpl<E> n = validate(node);
        E prev = null;
        if (right(n) == null) {
            prev = replace(n, n.getLeft());
        } else {
            if (left(n) == null) {
                prev = replace(n, n.getRight());
            }
        }
        size--;
        return prev;
    }

    private E replace(NodeImpl<E> removable, NodeImpl<E> newNode) {
        E prev = removable.getElement();
        if (Objects.equals(removable, root)) {
            root = newNode;
            if (root != null) {
                root.setParent(null);
            }
        } else {
            NodeImpl<E> parent = removable.getParent();
            if (parent.getLeft() == removable) {
                parent.setLeft(newNode);
            } else {
                parent.setRight(newNode);
            }
        }
        return prev;
    }


    @Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        return validate(p).getLeft();
    }

    @Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        return validate(p).getRight();
    }

    @Override
    public Node<E> root() {
        return root;
    }

    @Override
    public Node<E> parent(Node<E> n) throws IllegalArgumentException {
        return validate(n).getParent();
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<E> iterator() {
        return super.iterator();
    }

    /**
     * @return an iterable collection of nodes of the tree in inorder
     */
    @Override
    public Collection<Node<E>> nodes() {
        return super.inOrder();
    }

    protected static class NodeImpl<E> implements Node<E> {
        private E element;
        private NodeImpl<E> parent;
        private NodeImpl<E> left;
        private NodeImpl<E> right;

        public NodeImpl(E element, NodeImpl<E> parent) {
            this.element = element;
            this.parent = parent;
        }

        public void setLeft(NodeImpl<E> left) {
            this.left = left;
        }

        public void setRight(NodeImpl<E> right) {
            this.right = right;
        }

        public void setElement(E element) {
            this.element = element;
        }

        public void setParent(NodeImpl<E> parent) {
            this.parent = parent;
        }

        public NodeImpl<E> getLeft() {
            return left;
        }

        public NodeImpl<E> getRight() {
            return right;
        }

        public NodeImpl<E> getParent() {
            return parent;
        }

        @Override
        public E getElement() {
            return element;
        }

        @Override
        public String toString() {
            return element.toString();
        }
    }
}