package com.getjavajob.training.algo1712.makhovak.lesson01;

import static com.getjavajob.training.algo1712.makhovak.lesson01.Task6.*;
import static com.getjavajob.training.util.Assert.assertEquals;

public class Task6Test {
    public static void main(String[] args) {
        testElevateNumber();
        testSumElevateNumbers();
        testResetNumberBits();
        testSetBitInOneFromZero();
        testSetBitInOneFromOne();
        testInvertBitInNumberFromOne();
        testInvertBitInNumberFromZero();
        testSetBitInZeroFromOne();
        testSetBitInZeroFromZero();
        testReturnLowerBits();
        testReturnNthBitOne();
        testReturnNthBitZero();
        testRepresentInBin();
    }

    public static void testElevateNumber() {
        assertEquals("Task6Test.testElevateNumber", 8, elevateNumber(3));
    }

    public static void testSumElevateNumbers() {
        assertEquals("Task6Test.testSumElevateNumbers", 20, sumElevateNumbers(2, 4));
    }

    public static void testResetNumberBits() {
        assertEquals("Task6Test.testResetNumberBits", 0b1010000, resetNumberBits(0b1011001, 4));
    }

    public static void testSetBitInOneFromZero() {
        assertEquals("Task6Test.testSetBitInOneFromZero", 0b10001000, setBitInOne(0b10000000, 4));
    }

    public static void testSetBitInOneFromOne() {
        assertEquals("Task6Test.testSetBitInOneFromOne", 0b111111111, setBitInOne(0b111111111, 4));
    }

    public static void testInvertBitInNumberFromOne() {
        assertEquals("Task6Test.testInvertBitInNumberFromOne", 0b1010001, invertBitInNumber(0b1010101, 3));
    }

    public static void testInvertBitInNumberFromZero() {
        assertEquals("Task6Test.testInvertBitInNumberFromZero", 0b1010111, invertBitInNumber(0b1010101, 2));
    }

    public static void testSetBitInZeroFromOne() {
        assertEquals("Task6Test.testSetBitInZeroFromOne", 0b11111011, setBitInZero(0b11111111, 3));
    }

    public static void testSetBitInZeroFromZero() {
        assertEquals("Task6Test.testSetBitInZeroFromZero", 0b1111011, setBitInZero(0b1111011, 3));
    }

    public static void testReturnLowerBits() {
        assertEquals("Task6Test.testReturnLowerBits", 0b0101, returnLowerBits(0b1010101, 4));
    }

    public static void testReturnNthBitOne() {
        assertEquals("Task6Test.testReturnNthBit", 1, returnNthBit(0b1010101, 3));
    }

    public static void testReturnNthBitZero() {
        assertEquals("Task6Test.testReturnNthBit", 0, returnNthBit(0b1010101, 2));
    }

    public static void testRepresentInBin() {
        assertEquals("Task6Test.testRepresentInBin", "00001010", representInBin(10));
    }
}