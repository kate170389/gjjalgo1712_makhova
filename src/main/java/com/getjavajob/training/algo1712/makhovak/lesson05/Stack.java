package com.getjavajob.training.algo1712.makhovak.lesson05;

public interface Stack<V> {
    void push(V var);

    V pop();
}