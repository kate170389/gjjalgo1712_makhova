package com.getjavajob.training.algo1712.makhovak.lesson04;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public abstract class AbstractListDLL<V> implements List<V> {
    public boolean isEmpty() {
        throw new UnsupportedOperationException("Unsupported operation exception");
    }

    public Iterator<V> iterator() {
        throw new UnsupportedOperationException("Unsupported operation exception");
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException("Unsupported operation exception");
    }

    @Override
    public boolean addAll(Collection<? extends V> c) {
        throw new UnsupportedOperationException("Unsupported operation exception");
    }

    @Override
    public boolean addAll(int index, Collection<? extends V> c) {
        throw new UnsupportedOperationException("Unsupported operation exception");
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("Unsupported operation exception");
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("Unsupported operation exception");
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("Unsupported operation exception");
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException("Unsupported operation exception");
    }

    @Override
    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException("Unsupported operation exception");
    }

    @Override
    public List<V> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException("Unsupported operation exception");
    }
}