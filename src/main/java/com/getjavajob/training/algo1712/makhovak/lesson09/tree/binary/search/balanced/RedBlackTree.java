package com.getjavajob.training.algo1712.makhovak.lesson09.tree.binary.search.balanced;

import com.getjavajob.training.algo1712.makhovak.lesson07.tree.Node;
import com.getjavajob.training.algo1712.makhovak.lesson08.tree.binary.search.balanced.BalanceableTree;

public class RedBlackTree<E> extends BalanceableTree<E> {
    private static final boolean BLACK = true;
    private static final boolean RED = false;

    @Override
    protected NodeRBImpl<E> validate(Node<E> n) throws IllegalArgumentException {
        if (n == null || !(n instanceof NodeRBImpl)) {
            throw new IllegalArgumentException("IllegalArgumentException: the node is not an instance of supported");
        }
        return (NodeRBImpl<E>) n;
    }

    private boolean isBlack(Node<E> n) {
        NodeRBImpl<E> node = validate(n);
        return node.getColor() == BLACK;
    }

    private boolean isRed(Node<E> n) {
        NodeRBImpl<E> node = validate(n);
        return node.getColor() == RED;
    }

    private void makeBlack(Node<E> n) {
        NodeRBImpl<E> node = validate(n);
        node.setColor(BLACK);
    }

    private void makeRed(Node<E> n) {
        NodeRBImpl<E> node = validate(n);
        node.setColor(RED);
    }

    @Override
    protected void afterElementAdded(Node<E> n) {
        NodeRBImpl<E> node = validate(n);
        if (parent(n) == null) {
            makeBlack(node);
        } else {
            NodeRBImpl<E> parent = validate(node.getParent());
            if (isRed(parent(node))) {
                NodeRBImpl<E> grandParent = (NodeRBImpl<E>) (parent.getParent());
                NodeRBImpl<E> uncle = (NodeRBImpl<E>) sibling(parent);
                if (uncle != null && isRed(uncle)) {
                    makeBlack(parent);
                    makeBlack(uncle);
                    makeRed(grandParent);
                    afterElementAdded(grandParent);
                } else {
                    if (node == parent.getRight() && parent == grandParent.getLeft() ||
                            node == parent.getLeft() && parent == grandParent.getRight()) {
                        makeBlack(node);
                        makeRed(grandParent);
                    } else {
                        makeBlack(parent);
                        makeRed(grandParent);
                    }
                    reduceSubtreeHeight(node);
                }
            }
        }
    }

    @Override
    protected void afterElementRemoved(Node<E> n) {
        while (n != root && isBlack(n)) {
            Node<E> sibling = sibling(n);
            if (isRed(sibling)) {
                makeBlack(sibling);
                makeRed(parent(n));
                rotate(sibling);
                sibling = sibling(n);
            }
            if (isExternal(sibling) || isBlack(left(sibling)) && isBlack(right(sibling))) {
                makeRed(sibling);
                n = parent(n);
            } else {
                Node<E> siblingChild = sibling == right(parent(sibling)) ? right(sibling) : left(sibling);
                if (isBlack(siblingChild)) {
                    makeBlack(sibling(siblingChild));
                    makeRed(sibling);
                    rotate(sibling(siblingChild));
                    sibling = sibling(n);
                }
                if (isBlack(parent(n))) {
                    makeBlack(sibling);
                } else {
                    makeRed(sibling);
                }
                makeBlack(parent(n));
                siblingChild = sibling == right(parent(sibling)) ? right(sibling) : left(sibling);
                makeBlack(siblingChild);
                rotate(sibling);
                n = root;
            }
        }
        makeBlack(n);
    }

    public Node<E> addRoot(E e) throws IllegalStateException {
        if (!isEmpty()) {
            throw new IllegalStateException("IllegalStateException: root already exists");
        }
        size++;
        this.root = new NodeRBImpl<>(e, null, BLACK);
        return root;
    }

    @Override
    public Node<E> add(Node<E> node, E e) throws IllegalArgumentException {
        if (node != root()) {
            throw new IllegalArgumentException("Node must be only root");
        }
        add(e);
        return null;
    }

    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        return remove(n.getElement());
    }

    @Override
    public E remove(E val) {
        NodeImpl<E> node = validate(treeSearch(root(), val));
        E prev = node.getElement();
        if (childrenNumber(node) == 2) {
            Node<E> newNode = right(node);
            while (left(newNode) != null) {
                newNode = left(newNode);
            }
            set(node, newNode.getElement());
            node = validate(newNode);
        }
        Node<E> childNode = left(node) == null ? right(node) : left(node);
        if (childNode != null) {
            super.remove(node.getElement());
            if (isBlack(node)) {
                afterElementRemoved(childNode);
            }
        } else {
            if (node == root) {
                super.remove(node.getElement());
            } else {
                if (isBlack(node)) {
                    afterElementRemoved(node);
                }
                super.remove(node.getElement());
            }
        }
        return prev;
    }

    @Override
    public Node<E> addLeft(Node<E> node, E e) {
        throw new IllegalStateException("use method add(val) for this tree");
    }

    @Override
    public Node<E> addRight(Node<E> node, E e) {
        throw new IllegalStateException("use method add(val) for this tree");
    }

    private NodeRBImpl<E> addLeft(NodeRBImpl<E> n, E e) throws IllegalArgumentException {
        size++;
        n.setLeft(new NodeRBImpl<>(e, n, RED));
        return validate(n.getLeft());
    }

    private NodeRBImpl<E> addRight(NodeRBImpl<E> n, E e) throws IllegalArgumentException {
        size++;
        n.setRight(new NodeRBImpl<>(e, n, RED));
        return validate(n.getRight());
    }

    public void addForTest(E val, boolean color) {
        if (root() == null) {
            addRoot(val);
            return;
        }
        NodeRBImpl<E> node = validate(root);
        NodeRBImpl<E> parent = node;
        while (node != null) {
            int compare = compare(node.getElement(), val);
            parent = node;
            node = compare == 1 ? (NodeRBImpl<E>) node.getLeft() : (NodeRBImpl<E>) node.getRight();
        }
        if (compare(parent.getElement(), val) == 1) {
            node = addLeft(parent, val);
        } else {
            node = addRight(parent, val);
        }
        if (color) {
            makeBlack(node);
        } else {
            makeRed(node);
        }
    }

    @Override
    public Node<E> add(E val) {
        if (root() == null) {
            addRoot(val);
            return root();
        }
        NodeRBImpl<E> node = validate(root);
        NodeRBImpl<E> parent = node;
        while (node != null) {
            int compare = compare(node.getElement(), val);
            parent = node;
            node = compare == 1 ? (NodeRBImpl<E>) node.getLeft() : (NodeRBImpl<E>) node.getRight();
        }
        if (compare(parent.getElement(), val) == 1) {
            node = addLeft(parent, val);
        } else {
            node = addRight(parent, val);
        }
        afterElementAdded(node);
        return node;
    }

    protected static class NodeRBImpl<E> extends NodeImpl<E> implements Node<E> {
        private boolean color;

        public NodeRBImpl(E element, NodeImpl<E> parent, boolean color) {
            super(element, parent);
            this.color = color;
        }

        public void setColor(boolean color) {
            this.color = color;
        }

        public boolean getColor() {
            return color;
        }

        @Override
        public String toString() {
            String col = color ? "black" : "red";
            return super.toString() + " " + col;
        }
    }
}