package com.getjavajob.training.algo1712.makhovak.lesson05;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

import static com.getjavajob.training.algo1712.makhovak.lesson05.CollectionUtils.filter;
import static com.getjavajob.training.algo1712.makhovak.lesson05.CollectionUtils.transform;
import static com.getjavajob.training.util.Assert.assertEquals;
import static com.getjavajob.training.util.Assert.fail;

public class CollectionUtilsTest {
    public static void main(String[] args) {
        testFilter();
        testTransform();
        testForAllDo();
        testUnmodifiableCollection();
    }

    private static void testUnmodifiableCollection() {
        Collection<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        Collection<Integer> test = CollectionUtils.unmodifiableCollection(list);
        assertEquals("CollectionUtilsTests.testUnmodifiableCollectionSize", 3, test.size());
        try {
            test.add(8);
            fail("There are not UnsupportedOperationException");
        } catch (UnsupportedOperationException uoe) {
            assertEquals("CollectionUtilsTests.testUnmodifiableCollectionException",
                    "UnsupportedOperationException", uoe.getMessage());
        }
    }

    private static void testForAllDo() {
        Collection<Employee> employees = new ArrayList<>();
        Employee employee1 = new Employee("Mahov", "Alexey", "Prostornaya str", 3, 2012);
        Employee employee2 = new Employee("Bikov", "Vadim", "Federovich",
                "Akademika Pavloda ", 2, 2014);
        Employee employee3 = new Employee("Sagirova", "Olga", "Mikhailovna", "Filevskaya",
                11, 2014);
        Employee employee4 = new Employee("Gradov", "Oleg", "Moscowskiy pr-t", 1, 2015);
        employees.add(employee1);
        employees.add(employee2);
        employees.add(employee3);
        employees.add(employee4);
        Closure<Employee> closure = new Closure<Employee>() {
            @Override
            public void changeVar(Employee var) {
                var.setMonthStartWorking(var.getMonthStartWorking() + 1);
            }
        };
        Collection<Employee> notChangedCollection = new ArrayList<>(employees);
        CollectionUtils.forAllDo(employees, closure);
        Collection<Employee> test = new ArrayList<>();
        Employee employee11 = new Employee("Mahov", "Alexey", "Prostornaya str", 4, 2012);
        Employee employee12 = new Employee("Bikov", "Vadim", "Federovich",
                "Akademika Pavloda ", 3, 2014);
        Employee employee13 = new Employee("Sagirova", "Olga", "Mikhailovna",
                "Filevskaya", 12, 2014);
        Employee employee14 = new Employee("Gradov", "Oleg", "Moscowskiy pr-t", 2, 2015);
        test.add(employee11);
        test.add(employee12);
        test.add(employee13);
        test.add(employee14);
        assertEquals("CollectionUtilsTests.testForAllDo", test, employees);
    }

    private static void testTransform() {
        Collection<Employee> employees = new LinkedList<>();
        Employee employee1 = new Employee("Mahov", "Alexey", "Prostornaya str", 3, 2012);
        Employee employee2 = new Employee("Bikov", "Vadim", "Federovich",
                "Akademika Pavloda ", 2, 2014);
        Employee employee3 = new Employee("Sagirova", "Olga", "Mikhailovna",
                "Filevskaya", 12, 2014);
        Employee employee4 = new Employee("Gradov", "Oleg", "Moscowskiy pr-t", 1, 2015);
        employees.add(employee1);
        employees.add(employee2);
        employees.add(employee3);
        employees.add(employee4);
        Transformer<Employee, String> transformer = new Transformer<Employee, String>() {
            @Override
            public String transform(Employee object) {
                return object.getSurname();
            }
        };
        Collection<String> testCollection = new ArrayList<>();
        testCollection.add("Mahov");
        testCollection.add("Bikov");
        testCollection.add("Sagirova");
        testCollection.add("Gradov");
        Collection<String> transformedNewCollection = CollectionUtils.transformNewCollection(employees, transformer);
        assertEquals("CollectionUtilsTests.testTransformNewCollection", testCollection,
                transformedNewCollection);
        transform(employees, transformer);
        assertEquals("CollectionUtilsTests.testTransform", testCollection, employees);
    }

    private static void testFilter() {
        Collection<Employee> employees = new ArrayList<>();
        Employee employee1 = new Employee("Mahov", "Alexey", "Prostornaya str", 3, 2012);
        Employee employee2 = new Employee("Bikov", "Vadim", "Federovich",
                "Akademika Pavloda ", 2, 2014);
        Employee employee3 = new Employee("Sagirova", "Olga", "Mikhailovna",
                "Filevskaya", 12, 2014);
        Employee employee4 = new Employee("Gradov", "Oleg", "Moscowskiy pr-t", 1, 2015);
        employees.add(employee1);
        employees.add(employee2);
        employees.add(employee3);
        employees.add(employee4);
        Predicate<Employee> predicate = new Predicate<Employee>() {
            public boolean evaluate(Employee o) {
                return o.getSurname().contains("Mah");
            }
        };
        assertEquals("CollectionUtilsTests.testFilterTrue", true, filter(employees, predicate));
        Collection<Employee> employeesAfterFilter = new ArrayList<>();
        employeesAfterFilter.add(employee1);
        assertEquals("CollectionUtilsTests.testCollectionAfterFilter", employees, employeesAfterFilter);
        Collection<Employee> employeesNull = new ArrayList<>();
        Predicate<Employee> predicateNull = null;
        assertEquals("CollectionUtilsTests.testFilterFalseNullCollection",
                false, filter(employeesNull, predicate));
    }
}