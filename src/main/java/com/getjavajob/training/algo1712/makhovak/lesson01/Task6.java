package com.getjavajob.training.algo1712.makhovak.lesson01;

public class Task6 {
    public static int elevateNumber(int n) {
        return 1 << n;
    }

    public static int sumElevateNumbers(int n, int m) {
        return 1 << n | 1 << m;
    }

    public static int resetNumberBits(int number, int numberOfBits) {
        return number & (~1 << numberOfBits >> 1);
    }

    public static int setBitInOne(int number, int bit) {
        return number | 1 << bit >> 1;
    }

    public static int invertBitInNumber(int number, int bit) {
        return number ^ 1 << bit >> 1;
    }

    public static int setBitInZero(int number, int bit) {
        return number & ~(1 << bit >> 1);
    }

    public static int returnLowerBits(int number, int numberOfBits) {
        return number & ~(~1 << numberOfBits >> 1);
    }

    public static int returnNthBit(int number, int bit) {
        return 1 & number << 1 >> bit;
    }

    public static String representInBin(int number) {
        StringBuilder result = new StringBuilder();
        for (int i = 8; i > 0; i--) {
            result.append(1 & number << 1 >> i);
        }
        return result.toString();
    }
}