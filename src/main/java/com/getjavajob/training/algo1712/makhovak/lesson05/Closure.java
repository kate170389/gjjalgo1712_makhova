package com.getjavajob.training.algo1712.makhovak.lesson05;

public interface Closure<I> {
    void changeVar(I var);
}