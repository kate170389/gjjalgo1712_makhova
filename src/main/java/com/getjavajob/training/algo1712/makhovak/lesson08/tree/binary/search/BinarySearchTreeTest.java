package com.getjavajob.training.algo1712.makhovak.lesson08.tree.binary.search;

import com.getjavajob.training.algo1712.makhovak.lesson07.tree.Node;

import static com.getjavajob.training.util.Assert.assertEquals;

public class BinarySearchTreeTest {
    public static void main(String[] args) {
        testCompare();
        testTreeSearch();
        testAdd();
        testPrint();
        testRemove();
    }

    private static void testRemove() {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>();
        tree.add(10);
        tree.add(15);
        tree.add(5);
        tree.add(12);
        tree.add(20);
        tree.add(4);
        tree.add(6);
        tree.add(19);
        tree.add(35);
        String expected = "10(5(null,6),15(12,20(19,35)))";
        tree.remove(4);
        assertEquals("BinarySearchTreeTest.testRemoveSheet", expected, tree.toString());
        tree.remove(15);
        expected = "10(5(null,6),19(12,20(null,35)))";
        assertEquals("BinarySearchTreeTest.testRemoveInternalOne", expected, tree.toString());
        tree.remove(19);
        expected = "10(5(null,6),20(12,35))";
        assertEquals("BinarySearchTreeTest.testRemoveInternalTwo", expected, tree.toString());
    }

    private static void testTreeSearch() {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>();
        tree.add(10);
        tree.add(15);
        tree.add(5);
        tree.add(21);
        tree.add(12);
        tree.add(2);
        assertEquals("BinarySearchTreeTest.testTreeSearch", tree.left(tree.right(tree.root())),
                tree.treeSearch(tree.right(tree.root()), 12));
        assertEquals("BinarySearchTreeTest.testTreeSearchNull", null,
                tree.treeSearch(tree.right(tree.root()), 25));
    }

    private static void testAdd() {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>();
        tree.add(10);
        tree.add(15);
        tree.add(5);
        tree.add(21);
        tree.add(12);
        Node<Integer> actual = tree.add(2);
        assertEquals("BinarySearchTreeTest.testAddTrue", tree.treeSearch(tree.root(), 2), actual);
        assertEquals("BinarySearchTreeTest.testAddFalse", null, tree.add(2));
        String expected = "[2, 5, 10, 12, 15, 21]";
        assertEquals("BinarySearchTreeTest.testAdd", expected, tree.inOrder().toString());
    }

    private static void testCompare() {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>();
        tree.add(10);
        tree.add(15);
        tree.add(5);
        assertEquals("BinarySearchTreeTest.testCompareIsPositive", 1,
                tree.compare(tree.root().getElement(), tree.left(tree.root()).getElement()));
        assertEquals("BinarySearchTreeTest.testCompareIsNegative", -1,
                tree.compare(tree.root().getElement(), tree.right(tree.root()).getElement()));
        assertEquals("BinarySearchTreeTest.testCompareIsNull", 0,
                tree.compare(tree.root().getElement(), 10));
    }

    private static void testPrint() {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>();
        tree.add(10);
        tree.add(15);
        tree.add(5);
        tree.add(12);
        tree.add(20);
        tree.add(4);
        tree.add(6);
        String expected = "10(5(4,6),15(12,20))";
        assertEquals("BinarySearchTreeTest.testPrint", expected, tree.toString());
    }
}