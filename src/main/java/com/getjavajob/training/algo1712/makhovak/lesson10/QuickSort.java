package com.getjavajob.training.algo1712.makhovak.lesson10;

public class QuickSort {
    public void sort(byte[] array) {
        quickSort(array, 0, array.length - 1);
    }

    private void quickSort(byte[] array, int lo, int hi) {
        int index = partition(array, lo, hi);
        if (lo < index - 1) {
            quickSort(array, lo, index - 1);
        }
        if (index < hi) {
            quickSort(array, index, hi);
        }
    }

    private int partition(byte[] array, int lo, int hi) {
        int pivot = array[lo + (hi - lo) / 2];
        int i = lo;
        int j = hi;
        while (i <= j) {
            while (array[i] < pivot) {
                i++;
            }
            while (array[j] > pivot) {
                j--;
            }
            if (i <= j) {
                byte tmp = array[i];
                array[i] = array[j];
                array[j] = tmp;
                i++;
                j--;
            }
        }
        return i;
    }
}