package com.getjavajob.training.algo1712.makhovak.lesson06;

import java.util.Objects;

import static java.lang.StrictMath.abs;
import static java.lang.System.out;

public class AssociativeArray<K, V> {
    private static final int MAXIMUM_CAPACITY = 1 << 30;
    private static final float DEFAULT_LOAD_FACTOR = 0.75f;
    private static final int DEFAULT_INITIAL_CAPACITY = 16;
    private float loadFactor;
    private Entry<K, V>[] array;
    private int threshold;
    private int size;

    public AssociativeArray(int capacity, float loadFactor) {
        if (capacity < 0) {
            throw new IllegalArgumentException("Illegal Capacity: The capacity should not be less than 0 ");
        }
        if (capacity > MAXIMUM_CAPACITY) {
            capacity = MAXIMUM_CAPACITY;
        }
        if (loadFactor <= 0 || loadFactor > 1) {
            throw new IllegalArgumentException("Illegal Load Factor: The load factor should not be ess than 0 " +
                    "or equal 0");
        }
        this.loadFactor = loadFactor;
        array = new Entry[capacity];
        threshold = (int) (capacity * loadFactor);
    }

    public AssociativeArray() {
        this(DEFAULT_INITIAL_CAPACITY, DEFAULT_LOAD_FACTOR);
    }

    public AssociativeArray(int capacity) {
        this(capacity, DEFAULT_LOAD_FACTOR);
    }

    private static class Entry<K, V> {
        private final K key;
        private final int hash;
        private V value;
        private Entry<K, V> next;

        Entry(int hash, K key, V value, Entry<K, V> next) {
            this.hash = hash;
            this.key = key;
            this.value = value;
            this.next = next;
        }
    }

    public V add(K key, V value) {
        if (key == null) {
            return addForNullKey(value);
        }
        int index = abs(key.hashCode() % array.length);
        for (Entry<K, V> e = array[index]; e != null; e = e.next) {
            if (e.key == key || e.key.equals(key)) {
                V oldValue = e.value;
                e.value = value;
                return oldValue;
            }
        }
        addEntry(key.hashCode(), key, value, index);
        return null;
    }

    private V addForNullKey(V value) {
        for (Entry<K, V> e = array[0]; e != null; e = e.next) {
            if (e.key == null) {
                V oldValue = e.value;
                e.value = value;
                return oldValue;
            }
        }
        addEntry(0, null, value, 0);
        return null;
    }

    private void addEntry(int hash, K key, V value, int index) {
        if (size >= threshold) {
            resize(array.length << 1);
        }
        Entry<K, V> e = array[index];
        array[index] = new Entry<>(hash, key, value, e);
        size++;
    }

    private void resize(int newCapacity) {
        if (array.length == MAXIMUM_CAPACITY) {
            threshold = Integer.MAX_VALUE;
            return;
        }
        Entry<K, V>[] newArray = new Entry[newCapacity];
        transfer(newArray);
        array = newArray;
        threshold = (int) (newCapacity * loadFactor);
    }

    private void transfer(Entry[] newArray) {
        for (int i = 0; i < array.length; i++) {
            for (Entry<K, V> e = array[i]; e != null; e = e.next) {
                if (e.key == null) {
                    Entry<K, V> eNA = newArray[0];
                    while (eNA != null) {
                        eNA = eNA.next;
                    }
                    eNA = e;
                    eNA.next = null;
                } else {
                    int index = abs(e.hash % array.length);
                    Entry<K, V> eNA = newArray[index];
                    while (eNA != null) {
                        eNA = eNA.next;
                    }
                    eNA = e;
                    eNA.next = null;
                }
            }
        }

    }

    private V getForNullKey() {
        for (Entry<K, V> e = array[0]; e != null; e = e.next) {
            if (e.key == null) {
                return e.value;
            }
        }
        return null;
    }

    public V get(K key) {
        if (key == null) {
            return getForNullKey();
        }
        int index = abs(key.hashCode() % array.length);
        for (Entry<K, V> e = array[index]; e != null; e = e.next) {
            if (e.key == key || e.key.equals(key)) {
                return e.value;
            }
        }
        return null;
    }

    public V remove(K key) {
        int index = (key == null) ? 0 : abs(key.hashCode() % array.length);
        Entry<K, V> prev = array[index];
        Entry<K, V> e = prev;
        while (e != null) {
            if (e.key == key || key != null && e.key.equals(key)) {
                if (Objects.equals(prev, e)) {
                    array[index] = e.next;
                } else {
                    prev.next = e.next;
                }
                size--;
                return e.value;
            }
            prev = e;
            e = prev.next;
        }
        return null;
    }

    public void printAA() {
        for (int i = 0; i < array.length; i++) {
            for (Entry<K, V> bucket = array[i]; bucket != null; bucket = bucket.next) {
                out.println(i);
                out.print("key: " + bucket.key + "      val: " + bucket.value);
                out.println();
            }
        }
    }

    public int getSize() {
        return size;
    }

    public int getCapacity() {
        return array.length;
    }
}