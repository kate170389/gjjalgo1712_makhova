package com.getjavajob.training.algo1712.makhovak.lesson10;

import static com.getjavajob.training.util.Assert.assertEquals;

public class MergeSortTest {
    public static void main(String[] args) {
        testSortOne();
        testSortTwo();
    }

    private static void testSortTwo() {
        MergeSort merge = new MergeSort();
        //The maximum length of an array for JVM is Integer.MAX_VALUE-2
        //(if more, you get an OutOfMemoryError when compiling "Requested array size exceeds VM limit")
        //if we use an array of elements of int type, then we need about 8 GB of memory(+memory for GC)
        //for bytes 4 times less - 2 GB(+memory for GC).
        //In sort we also need the same amount of memory to create the copy array.
        //Therefore, we can only use array byte[Integer.MAX_VALUE-2]
        byte[] array = new byte[Integer.MAX_VALUE - 2];
        //There will be no int overflow, because we use the expression lo + (hi - lo) / 2 to find the middle
        merge.sort(array);
    }

    private static void testSortOne() {
        MergeSort merge = new MergeSort();
        byte[] array = new byte[]{10, 5, 0, 56, -8, -2, 5, 4, 3, 2, 1};
        merge.sort(array);
        byte[] expected = new byte[]{-8, -2, 0, 1, 2, 3, 4, 5, 5, 10, 56};
        assertEquals("MergeSortTest.testSortOne", expected, array);
    }
}