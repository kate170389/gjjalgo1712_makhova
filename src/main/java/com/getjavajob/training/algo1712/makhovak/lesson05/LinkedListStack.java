package com.getjavajob.training.algo1712.makhovak.lesson05;

import java.util.List;

public class LinkedListStack<V> implements Stack<V> {
    private SinglyLinkedList<V> stack = new SinglyLinkedList<>();

    @Override
    public void push(V val) {
        stack.add(val);
    }

    @Override
    public V pop() {
        V deleted = stack.get();
        if (!stack.remove()) {
            throw new NullPointerException("There are not elements");
        }
        return deleted;
    }

    public List<V> asList() {
        return stack.asList();
    }
}