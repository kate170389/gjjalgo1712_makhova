1) byte b = 100 // b=100
2) byte b = 100L ? // not compile, type mismatch. Byte has only 1 byte,long - 4 bytes. To compile: byte b=(byte)100L
3) int i = 100L ? //not compile, type mismatch. Byte has only 1 byte, and long - 4 bytes. To compile: int i=(int)100L