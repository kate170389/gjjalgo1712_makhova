package com.getjavajob.training.algo1712.makhovak.lesson06;

import java.util.*;

import static com.getjavajob.training.util.Assert.assertEquals;

public class MapTest {
    public static void main(String[] args) {
        testSize();
        testPut();
        testGet();
        testIsEmpty();
        testContainsKey();
        testMerge();
        testReplace();
        testReplaceDependingOfTheValue();
        testRemove();
        testRemoveDependingOfTheValue();
        testPutAll();
        testClear();
        testContainsValue();
        testKeySet();
        testValues();
        testEntrySet();
    }

    public static void testSize() {
        Map<Integer, String> mapTest = new HashMap<>();
        mapTest.put(-1, "one");
        mapTest.put(2, "two");
        mapTest.put(3, "three");
        mapTest.put(3, "new three");
        assertEquals("MapTest.testSize", 3, mapTest.size());
    }

    public static void testIsEmpty() {
        Map<Integer, String> mapTest = new HashMap<>();
        assertEquals("MapTest.testIsEmpty", true, mapTest.isEmpty());
        mapTest.put(-1, "one");
        mapTest.put(2, "two");
        mapTest.put(3, "three");
        assertEquals("MapTest.testIsEmpty", false, mapTest.isEmpty());
    }

    public static void testContainsKey() {
        Map<Integer, String> mapTest = new HashMap<>();
        mapTest.put(-1, "one");
        mapTest.put(2, "two");
        mapTest.put(3, "three");
        assertEquals("MapTest.testContainsKey", true, mapTest.containsKey(2));
        assertEquals("MapTest.testNotContainsKey", false, mapTest.containsKey(65));
    }

    public static void testContainsValue() {
        Map<Integer, String> mapTest = new HashMap<>();
        mapTest.put(-1, "one");
        mapTest.put(2, "two");
        mapTest.put(3, "three");
        assertEquals("MapTest.testContainsValue", true, mapTest.containsValue("one"));
        assertEquals("MapTest.testNotContainsValue", false, mapTest.containsValue("four"));
    }

    public static void testGet() {
        Map<String, String> mapTest = new HashMap<>();
        mapTest.put("1", "one");
        mapTest.put("2", "two");
        assertEquals("MapTest.testGet", "one", mapTest.get("1"));
    }

    public static void testPut() {
        Map<Integer, String> mapTest = new HashMap<>();
        mapTest.put(-1, "one");
        assertEquals("MapTest.testPut", "one", mapTest.put(-1, "one new"));
    }

    public static void testRemoveDependingOfTheValue() {
        Map<String, String> mapTest = new HashMap<>();
        mapTest.put("1", "one");
        mapTest.put("2", "two");
        assertEquals("MapTest.testNotRemoveDependingOfTheValue", false, mapTest.remove("2", "three"));
        assertEquals("MapTest.testRemoveDependingOfTheValue", true, mapTest.remove("2", "two"));
        assertEquals("MapTest.testNotRemoveDependingOfTheValue", false, mapTest.remove("2", "two"));
    }

    public static void testRemove() {
        Map<String, String> mapTest = new HashMap<>();
        mapTest.put("1", "one");
        mapTest.put("2", "two");
        assertEquals("MapTest.testRemove", "two", mapTest.remove("2"));
        assertEquals("MapTest.testRemove", null, mapTest.get("2"));
    }

    public static void testValues() {
        Map<String, String> mapTest = new HashMap<>();
        mapTest.put("1", "one");
        mapTest.put("2", "two");
        mapTest.put("3", "three");
        List<String> collectionTest = new ArrayList<>(mapTest.values());
        List<String> expected = new ArrayList<>();
        expected.add("one");
        expected.add("two");
        expected.add("three");
        assertEquals("MapTest.testValues", expected, collectionTest);
    }

    public static void testPutAll() {
        Map<String, String> mapTest = new HashMap<>();
        mapTest.put("1", "one");
        mapTest.put("2", "two");
        Map<String, String> mapNew = new HashMap<>();
        mapNew.putAll(mapTest);
        assertEquals("MapTest.testPutAll", mapNew, mapTest);
    }

    public static void testClear() {
        Map<String, String> mapTest = new HashMap<>();
        mapTest.put("1", "one");
        mapTest.put("2", "two");
        mapTest.clear();
        Map<String, String> mapExpected = new HashMap<>();
        assertEquals("MapTest.testClear", mapExpected, mapTest);
    }

    public static void testKeySet() {
        Map<Integer, String> mapTest = new HashMap<>();
        mapTest.put(-1, "one");
        mapTest.put(2, "two");
        mapTest.put(3, "three");
        mapTest.put(3, "new three");
        Set<Integer> setTest = mapTest.keySet();
        Set<Integer> setExpected = new HashSet<>();
        setExpected.add(-1);
        setExpected.add(3);
        setExpected.add(2);
        assertEquals("MapTest.testKeySet", setExpected, setTest);

    }

    public static void testEntrySet() {
        Map<Integer, String> mapTest = new HashMap<>();
        mapTest.put(-1, "one");
        mapTest.put(2, "two");
        mapTest.put(3, "three");
        Set<Map.Entry> setTest = new HashSet<>(mapTest.entrySet());
        assertEquals("MapTest.testEntrySet", "[-1=one, 3=three, 2=two]", setTest.toString());
    }

    public static void testReplaceDependingOfTheValue() {
        Map<String, String> mapTest = new HashMap<>();
        mapTest.put("1", "one");
        mapTest.put("2", "two");
        assertEquals("MapTest.testReplaceDependingOfTheValue", true, mapTest.replace("2", "two", "two new"));
        assertEquals("MapTest.testNotReplaceDependingOfTheValue", false, mapTest.replace("2", "t", "two new"));
        assertEquals("MapTest.testNotReplaceDependingOfTheValue", false, mapTest.replace("4", "three", "two new"));
    }

    public static void testReplace() {
        Map<String, String> mapTest = new HashMap<>();
        mapTest.put("1", "one");
        mapTest.put("2", "two");
        mapTest.replace("2", "two new");
        assertEquals("MapTest.testReplace", "two new", mapTest.get("2"));
    }

    public static void testMerge() {
        Map<Integer, String> mapTest = new HashMap<>();
        mapTest.put(1, "one");
        mapTest.put(2, "two");
        mapTest.merge(2, "merge", (oldElement, newElement) -> oldElement + newElement);
        assertEquals("MapTest.testMerge", "twomerge", mapTest.get(2));
    }
}