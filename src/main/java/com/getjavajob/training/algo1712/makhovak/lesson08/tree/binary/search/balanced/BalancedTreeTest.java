package com.getjavajob.training.algo1712.makhovak.lesson08.tree.binary.search.balanced;

import com.getjavajob.training.algo1712.makhovak.lesson07.tree.Node;

import static com.getjavajob.training.util.Assert.assertEquals;

public class BalancedTreeTest {
    public static void main(String[] args) {
        testReduceSubtreeHeightLeftLeftCase();
        testReduceSubtreeHeightRightRightCase();
        testReduceSubtreeHeightLeftRightCase();
        testReduceSubtreeHeightRightLeftCase();
    }

    private static void testReduceSubtreeHeightRightLeftCase() {
        BalanceableTree<Integer> tree = new BalancedTree<>();
        tree.add(10);
        tree.add(15);
        tree.add(12);
        tree.add(17);
        Node<Integer> node = tree.treeSearch(tree.root(), 12);
        tree.reduceSubtreeHeight(node);
        String expected = "12(10,15(null,17))";
        assertEquals("BalancedTreeTest.testReduceSubtreeHeightRightLeftCase", expected, tree.toString());
    }

    private static void testReduceSubtreeHeightLeftRightCase() {
        BalanceableTree<Integer> tree = new BalancedTree<>();
        tree.add(5);
        tree.add(3);
        tree.add(4);
        Node<Integer> node = tree.treeSearch(tree.root(), 4);
        tree.reduceSubtreeHeight(node);
        String expected = "4(3,5)";
        assertEquals("BalancedTreeTest.testReduceSubtreeHeightLeftRightCase", expected, tree.toString());
    }

    private static void testReduceSubtreeHeightRightRightCase() {
        BalanceableTree<Integer> tree = new BalancedTree<>();
        tree.add(3);
        tree.add(5);
        tree.add(7);
        tree.add(4);
        tree.add(1);
        Node<Integer> node = tree.treeSearch(tree.root(), 7);
        tree.reduceSubtreeHeight(node);
        String expected = "5(3(1,4),7)";
        assertEquals("BalancedTreeTest.testReduceSubtreeHeightRightRightCase", expected, tree.toString());
    }

    private static void testReduceSubtreeHeightLeftLeftCase() {
        BalanceableTree<Integer> tree = new BalancedTree<>();
        tree.add(5);
        tree.add(3);
        tree.add(2);
        Node<Integer> node = tree.treeSearch(tree.root(), 2);
        tree.reduceSubtreeHeight(node);
        String expected = "3(2,5)";
        assertEquals("BalancedTreeTest.testReduceSubtreeHeightLeftLeftCase", expected, tree.toString());
    }
}