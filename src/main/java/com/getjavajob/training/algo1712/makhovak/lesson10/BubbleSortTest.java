package com.getjavajob.training.algo1712.makhovak.lesson10;

import static com.getjavajob.training.util.Assert.assertEquals;

public class BubbleSortTest {
    public static void main(String[] args) {
        testSortOne();
        testSortTwo();
    }

    private static void testSortTwo() {
        BubbleSort bubble = new BubbleSort();
        int[] array = new int[]{1, 2, 3, 4, 4, 5};
        int[] expected = array;
        bubble.sort(array);
        assertEquals("BubbleSortTest.testSortTwoAlreadySorted", expected, array);
    }

    private static void testSortOne() {
        BubbleSort bubble = new BubbleSort();
        int[] array = new int[]{7, 1, 2, 3, 4};
        bubble.sort(array);
        int[] expected = new int[]{1, 2, 3, 4, 7};
        assertEquals("BubbleSortTest.testSortOne", expected, array);
    }
}