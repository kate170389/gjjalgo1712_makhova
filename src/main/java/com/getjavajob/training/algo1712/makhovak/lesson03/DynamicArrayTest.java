package com.getjavajob.training.algo1712.makhovak.lesson03;

import static com.getjavajob.training.util.Assert.assertEquals;
import static com.getjavajob.training.util.Assert.fail;

public class DynamicArrayTest {
    public static void main(String[] args) {
        testGrowCapacityDynamicArray();
        testToArray();
        testAdd();
        testAddBeginning();
        testAddMiddle();
        testAddEnd();
        testSet();
        testGet();
        testRemoveFromBeginning();
        testRemoveFromMiddle();
        testRemoveFromEnd();
        testRemoveObject();
        testSize();
        testIndexOf();
        testContains();
        testAddException();
        testSetException();
        testGetException();
        testRemoveException();

        testListIteratorHasNext();
        testListIteratorNext();
        testListIteratorHasPrevious();
        testListIteratorPrevious();
        testIteratorNextIndex();
        testIteratorPreviousIndex();
        testIteratorRemove();
        testIteratorSet();
        testIteratorAdd();
        testListIteratorNextNoSuchElementsException();
        testListIteratorPreviousNoSuchElementsException();
        testListIteratorNextConcurrentModificationException();
        testListIteratorPreviousConcurrentModificationException();
        testListIteratorRemoveIllegalStateException();
        testListIteratorRemoveConcurrentModificationException();
        testListIteratorSetIllegalStateException();
        testListIteratorSetConcurrentModificationException();
        testListIteratorAddConcurrentModificationException();
    }

    public static void testGrowCapacityDynamicArray() {
        DynamicArray testDA = new DynamicArray(5);
        testDA.add(10);
        testDA.add(10);
        testDA.add(10);
        testDA.add(10);
        testDA.add(10);
        testDA.add(10);
        assertEquals("DynamicArrayTest.testGrowCapacityDynamicArray", 7, testDA.capacity());
    }

    public static void testToArray() {
        Object testArray[] = new Object[]{10, 20, 30};
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add(20);
        testDA.add(30);
        assertEquals("DynamicArrayTest.testAdd", testArray, testDA.toArray());
    }

    public static void testAdd() {
        Object testArray[] = new Object[]{10, "pol", 1.5, null};
        DynamicArray testDA = new DynamicArray();
        assertEquals("DynamicArrayTest.testAdd", true, testDA.add(10));
        assertEquals("DynamicArrayTest.testAdd", true, testDA.add("pol"));
        assertEquals("DynamicArrayTest.testAdd", true, testDA.add(1.5));
        assertEquals("DynamicArrayTest.testAdd", true, testDA.add(null));
        assertEquals("DynamicArrayTest.testAdd", testArray, testDA.toArray());
    }

    public static void testAddBeginning() {
        Object testArray[] = new Object[]{null, 10, 20, 30};
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add(20);
        testDA.add(30);
        testDA.add(0, null);
        assertEquals("DynamicArrayTest.testAddBeginning", testArray, testDA.toArray());
    }

    public static void testAddMiddle() {
        Object testArray[] = new Object[]{10, 20, null, 30, 40};
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add(20);
        testDA.add(30);
        testDA.add(40);
        testDA.add(2, null);
        assertEquals("DynamicArrayTest.testAddMiddle", testArray, testDA.toArray());
    }

    public static void testAddEnd() {
        Object testArray[] = new Object[]{10, 20, 30, 40, null, 8};
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add(20);
        testDA.add(30);
        testDA.add(40);
        testDA.add(testDA.size(), null);
        testDA.add(testDA.size(), 8);
        assertEquals("DynamicArrayTest.testAddEnd", testArray, testDA.toArray());
    }

    public static void testSet() {
        Object testArray[] = new Object[]{10, null, 30, 40};
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add(20);
        testDA.add(30);
        testDA.add(40);
        assertEquals("DynamicArrayTest.testSet", 20, testDA.set(1, null));
        assertEquals("DynamicArrayTest.testSet", testArray, testDA.toArray());
    }

    public static void testGet() {
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add(20);
        testDA.add(30);
        testDA.add(40);
        assertEquals("DynamicArrayTest.testGet", 30, testDA.get(2));
    }

    public static void testRemoveFromBeginning() {
        Object testArray[] = new Object[]{20, 30, 40};
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add(20);
        testDA.add(30);
        testDA.add(40);
        assertEquals("DynamicArrayTest.testRemoveFromBeginning", 10, testDA.remove(0));
        assertEquals("DynamicArrayTest.testRemoveFromBeginning", testArray, testDA.toArray());
    }

    public static void testRemoveFromMiddle() {
        Object testArray[] = new Object[]{10, "remove", 40};
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add('p');
        testDA.add("remove");
        testDA.add(40);
        assertEquals("DynamicArrayTest.testRemoveFromMiddle", 'p', testDA.remove(1));
        assertEquals("DynamicArrayTest.testRemoveFromMiddle", testArray, testDA.toArray());
    }

    public static void testRemoveFromEnd() {
        Object testArray[] = new Object[]{10, 'p', "remove"};
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add('p');
        testDA.add("remove");
        testDA.add(40);
        assertEquals("DynamicArrayTest.testRemoveFromEnd", 40, testDA.remove(3));
        assertEquals("DynamicArrayTest.testRemoveFromEnd", testArray, testDA.toArray());
    }

    public static void testRemoveObject() {
        Object testArray[] = new Object[]{10, 'p', "remove", 90, 45};
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add('p');
        testDA.add("remove");
        testDA.add(40);
        testDA.add(90);
        testDA.add(45);
        Object testObject = 't';
        Object testObject2 = 40;
        assertEquals("DynamicArrayTest.testRemoveObject", true, testDA.remove(testObject2));
        assertEquals("DynamicArrayTest.testNotRemoveObject", false, testDA.remove(testObject));
        assertEquals("DynamicArrayTest.testRemoveObject", testArray, testDA.toArray());
    }

    public static void testSize() {
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add('p');
        testDA.add("remove");
        testDA.add(40);
        testDA.add(90);
        testDA.add(45);
        assertEquals("DynamicArrayTest.testSize", 6, testDA.size());
    }

    public static void testIndexOf() {
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add('p');
        testDA.add("remove");
        testDA.add(null);
        testDA.add(90);
        testDA.add(45);
        Object testObject = 'p';
        assertEquals("DynamicArrayTest.testIndexOf", 1, testDA.indexOf(testObject));
        assertEquals("DynamicArrayTest.testIndexOf", 3, testDA.indexOf(null));
        assertEquals("DynamicArrayTest.testIndexOf", -1, testDA.indexOf(80));
    }

    public static void testContains() {
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add('p');
        testDA.add("remove");
        testDA.add(90);
        testDA.add(45);
        Object testObject = 'p';
        assertEquals("DynamicArrayTest.testContains", true, testDA.contains(testObject));
        assertEquals("DynamicArrayTest.testNotContains", false, testDA.contains(null));
    }


    public static void testAddException() {
        DynamicArray testDA = new DynamicArray();
        try {
            testDA.add(10);
            testDA.add(20);
            testDA.add(30);
            testDA.add(4, null);
            fail("There are not exception"); //
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testAddException", "IndexOutOfBoundsException Index: 4, Size: 3 Available indexes: from 0 to 2", e.getMessage());
        }
    }

    public static void testSetException() {
        DynamicArray testDA = new DynamicArray();
        try {
            testDA.add('g');
            testDA.add('e');
            testDA.add('t');
            testDA.set(6, 35);
            fail("There are not exception"); //
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testSetException", "IndexOutOfBoundsException Index: 6, Size: 3 Available indexes: from 0 to 2", e.getMessage());
        }
    }

    public static void testGetException() {
        DynamicArray testDA = new DynamicArray();
        try {
            testDA.add(1.3);
            testDA.add("kiu");
            testDA.add(30);
            testDA.get(6);
            fail("There are not exception"); //
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testGetException", "IndexOutOfBoundsException Index: 6, Size: 3 Available indexes: from 0 to 2", e.getMessage());
        }
    }

    public static void testRemoveException() {
        DynamicArray testDA = new DynamicArray();
        try {
            testDA.add(1.3);
            testDA.add("kiu");
            testDA.add(30);
            testDA.remove(6);
            fail("There are not exception"); //
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testRemoveException", "IndexOutOfBoundsException Index: 6, Size: 3 Available indexes: from 0 to 2", e.getMessage());
        }
    }

    public static void testListIteratorHasNext() {
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add('p');
        testDA.add("remove");
        testDA.add(90);
        testDA.add(45);
        DynamicArray.ListIterator testListIterator = testDA.listIterator();
        assertEquals("DynamicArrayTest.testIteratorHasNextTrue", true, testListIterator.hasNext());
        testListIterator = testDA.listIterator(4);
        testDA.remove(0);
        assertEquals("DynamicArrayTest.testIteratorHasNextFalse", false, testListIterator.hasNext());
    }

    public static void testListIteratorNext() {
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add('p');
        testDA.add("remove");
        testDA.add(90);
        testDA.add(45);
        int index = 2;
        DynamicArray.ListIterator testListIterator = testDA.listIterator(index);
        while (testListIterator.hasNext()) {
            assertEquals("DynamicArrayTest.testIteratorNext", testDA.get(index++), testListIterator.next());
        }
    }

    public static void testListIteratorHasPrevious() {
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add(90);
        testDA.add(45);
        DynamicArray.ListIterator testListIterator = testDA.listIterator(2);
        assertEquals("DynamicArrayTest.testListIteratorHasPreviousTrue", true, testListIterator.hasPrevious());
        testListIterator = testDA.listIterator();
        assertEquals("DynamicArrayTest.testListIteratorHasPreviousFalse", false, testListIterator.hasPrevious());
    }

    public static void testListIteratorPrevious() {
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add('p');
        testDA.add("remove");
        testDA.add(90);
        testDA.add(45);
        DynamicArray.ListIterator testListIterator = testDA.listIterator(3);
        assertEquals("DynamicArrayTest.testListIteratorPrevious", "remove", testListIterator.previous());
    }

    public static void testIteratorNextIndex() {
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add('p');
        testDA.add("remove");
        testDA.add(90);
        testDA.add(45);
        DynamicArray.ListIterator testListIterator = testDA.listIterator();
        assertEquals("DynamicArrayTest.testIteratorNextIndex", 0, testListIterator.nextIndex());
        testListIterator.next();
        assertEquals("DynamicArrayTest.testIteratorNextIndex", 1, testListIterator.nextIndex());
    }

    public static void testIteratorPreviousIndex() {
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add('p');
        testDA.add("remove");
        testDA.add(90);
        testDA.add(45);
        DynamicArray.ListIterator testListIterator = testDA.listIterator(4);
        assertEquals("DynamicArrayTest.testIteratorNextIndex", 3, testListIterator.previousIndex());
    }

    public static void testIteratorRemove() {
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add(20);
        testDA.add(30);
        testDA.add(40);
        testDA.add(50);
        DynamicArray.ListIterator testListIterator = testDA.listIterator(3);
        testListIterator.next();
        testListIterator.remove();
        DynamicArray testDAAfterRemove = new DynamicArray();
        testDAAfterRemove.add(10);
        testDAAfterRemove.add(20);
        testDAAfterRemove.add(30);
        testDAAfterRemove.add(50);
        assertEquals("DynamicArrayTest.testIteratorRemove", testDAAfterRemove.toArray(), testDA.toArray());
    }

    public static void testIteratorSet() {
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add(20);
        testDA.add(30);
        DynamicArray.ListIterator testListIterator = testDA.listIterator(2);
        testListIterator.previous();
        testListIterator.set(50);
        DynamicArray testDAAfterRemove = new DynamicArray();
        testDAAfterRemove.add(10);
        testDAAfterRemove.add(50);
        testDAAfterRemove.add(30);
        assertEquals("DynamicArrayTest.testIteratorSet", testDAAfterRemove.toArray(), testDA.toArray());
    }

    public static void testIteratorAdd() {
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add(20);
        testDA.add(30);
        testDA.add(40);
        DynamicArray.ListIterator testListIterator = testDA.listIterator(2);
        testListIterator.next();
        testListIterator.add(50);
        DynamicArray testDAAfterRemove = new DynamicArray();
        testDAAfterRemove.add(10);
        testDAAfterRemove.add(20);
        testDAAfterRemove.add(30);
        testDAAfterRemove.add(50);
        testDAAfterRemove.add(40);
        assertEquals("DynamicArrayTest.testIteratorSet", testDAAfterRemove.toArray(), testDA.toArray());
    }

    public static void testListIteratorNextNoSuchElementsException() {
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add(90);
        testDA.add(45);
        DynamicArray.ListIterator testListIterator = testDA.listIterator();
        try {
            testListIterator.next();
            testListIterator.next();
            testListIterator.next();
            testListIterator.next();
            fail("There are not NoSuchElementsException");
        } catch (Exception e) {
            assertEquals("DynamicArrayTest. testListIteratorNextNoSuchElementsException", "No such elements", e.getMessage());
        }
    }

    public static void testListIteratorPreviousNoSuchElementsException() {
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add(90);
        testDA.add(45);
        DynamicArray.ListIterator testListIterator = testDA.listIterator(2);
        try {
            testListIterator.previous();
            testListIterator.previous();
            testListIterator.previous();
            fail("There are not NoSuchElementsException");
        } catch (Exception e) {
            assertEquals("DynamicArrayTest. testListIteratorPreviousNoSuchElementsException", "No such elements", e.getMessage());
        }
    }

    public static void testListIteratorNextConcurrentModificationException() {
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add(90);
        testDA.add(45);
        DynamicArray.ListIterator testListIterator = testDA.listIterator();
        try {
            testListIterator.next();
            testListIterator.next();
            testDA.add(50);
            testListIterator.next();
            fail("There are not ConcurrentModificationException");
        } catch (Exception e) {
            assertEquals("DynamicArrayTest. testListIteratorNextConcurrentModificationException", "ConcurrentModificationException: the array was changed", e.getMessage());
        }
    }

    public static void testListIteratorPreviousConcurrentModificationException() {
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add(90);
        testDA.add(45);
        DynamicArray.ListIterator testListIterator = testDA.listIterator(2);
        try {
            testListIterator.previous();
            testDA.add(50);
            testListIterator.previous();
            fail("There are not ConcurrentModificationException");
        } catch (Exception e) {
            assertEquals("DynamicArrayTest. testListIteratorNextConcurrentModificationException", "ConcurrentModificationException: the array was changed", e.getMessage());
        }
    }

    public static void testListIteratorRemoveIllegalStateException() {
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add(90);
        testDA.add(45);
        DynamicArray.ListIterator testListIterator = testDA.listIterator();
        try {
            testListIterator.remove();
            testListIterator.next();
            testListIterator.remove();
            fail("There are not IllegalStateException");
        } catch (Exception e) {
            assertEquals("DynamicArrayTest. testListIteratorRemoveIllegalStateException", "IllegalStateException: there are no returned elements", e.getMessage());
        }
    }

    public static void testListIteratorRemoveConcurrentModificationException() {
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add(90);
        testDA.add(45);
        DynamicArray.ListIterator testListIterator = testDA.listIterator();
        try {
            testListIterator.next();
            testListIterator.remove();
            testDA.remove(testDA.size() - 1);
            testListIterator.next();
            testListIterator.remove();
            fail("There are not ConcurrentModificationException");
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testListIteratorRemoveConcurrentModificationException", "ConcurrentModificationException: the array was changed", e.getMessage());
        }
    }

    public static void testListIteratorSetIllegalStateException() {
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add(90);
        testDA.add(45);
        DynamicArray.ListIterator testListIterator = testDA.listIterator();
        try {
            testListIterator.set(50);
            testListIterator.next();
            testListIterator.set(50);
            fail("There are not IllegalStateException");
        } catch (Exception e) {
            assertEquals("DynamicArrayTest. testListIteratorSetIllegalStateException", "IllegalStateException: there are no returned elements", e.getMessage());
        }
    }

    public static void testListIteratorSetConcurrentModificationException() {
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add(90);
        testDA.add(45);
        DynamicArray.ListIterator testListIterator = testDA.listIterator();
        try {
            testListIterator.next();
            testListIterator.set(30);
            testDA.remove(testDA.size() - 1);
            testListIterator.set(20);
            fail("There are not ConcurrentModificationException");
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testListIteratorSetConcurrentModificationException", "ConcurrentModificationException: the array was changed", e.getMessage());
        }
    }

    public static void testListIteratorAddConcurrentModificationException() {
        DynamicArray testDA = new DynamicArray();
        testDA.add(10);
        testDA.add(90);
        testDA.add(45);
        DynamicArray.ListIterator testListIterator = testDA.listIterator();
        try {
            testListIterator.add(30);
            testDA.remove(testDA.size() - 1);
            testListIterator.add(20);
            fail("There are not ConcurrentModificationException");
        } catch (Exception e) {
            assertEquals("DynamicArrayTest.testListIteratorAddConcurrentModificationException", "ConcurrentModificationException: the array was changed", e.getMessage());
        }
    }
}