package com.getjavajob.training.algo1712.makhovak.lesson07.tree.binary;

import com.getjavajob.training.algo1712.makhovak.lesson07.tree.Node;

import java.util.Iterator;

import static com.getjavajob.training.util.Assert.assertEquals;
import static com.getjavajob.training.util.Assert.fail;

public class LinkedBinaryTreeTest {
    public static void main(String[] args) {
        testAddRoot();
        testValidate();
        testAddLeft();
        testAddRight();
        testAdd();
        testSet();
        testRemove();
        testParent();
        testSize();
        testNodes();
        testSibling();
        testChildren();
        tetChildrenNumber();
        testPreOrder();
        testPostOrder();
        testInOrder();
        testBreadthFirst();
        testIsInternal();
        testIsExternal();
        testIsRoot();
        testIsEmpty();
        testIterator();
    }

    private static void testIterator() {
        try {
            BinaryTree<Integer> tree = new LinkedBinaryTree<>();
            tree.addRoot(1);
            tree.add(tree.root(), 2);
            tree.add(tree.root(), 3);
            tree.add(tree.left(tree.root()), 4);
            Iterator<Integer> it = tree.iterator();
            assertEquals("linkedBinaryTreeTest.testIteratorHasNext", true, it.hasNext());
            assertEquals("linkedBinaryTreeTest.testIteratorNext", tree.left(tree.left(tree.root())).toString(),
                    it.next().toString());
            it.remove();
            fail("There are not UnsupportedOperationException");
        } catch (UnsupportedOperationException e) {
            assertEquals("linkedBinaryTreeTest.testIteratorRemove",
                    "UnsupportedOperationException remove", e.getMessage());
        }
    }

    private static void testIsEmpty() {
        BinaryTree<Integer> tree = new LinkedBinaryTree<>();
        assertEquals("linkedBinaryTreeTest.testIsEmpty", true, tree.isEmpty());
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        tree.add(tree.left(tree.root()), 4);
        assertEquals("linkedBinaryTreeTest.testIsNotEmpty", false, tree.isEmpty());
    }

    private static void testIsRoot() {
        BinaryTree<Integer> tree = new LinkedBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        tree.add(tree.left(tree.root()), 4);
        assertEquals("linkedBinaryTreeTest.testIsRoot", true, tree.isRoot(tree.root()));
        assertEquals("linkedBinaryTreeTest.testIsNotRoot", false, tree.isRoot(tree.right(tree.root())));
    }

    private static void testIsExternal() {
        BinaryTree<Integer> tree = new LinkedBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        tree.add(tree.left(tree.root()), 4);
        assertEquals("linkedBinaryTreeTest.testIsNotExternal", false, tree.isExternal(tree.root()));
        assertEquals("linkedBinaryTreeTest.testIsExternal", true, tree.isExternal(tree.right(tree.root())));
    }

    private static void testIsInternal() {
        BinaryTree<Integer> tree = new LinkedBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        tree.add(tree.left(tree.root()), 4);
        assertEquals("linkedBinaryTreeTest.testIsInternal", true, tree.isInternal(tree.left(tree.root())));
        assertEquals("linkedBinaryTreeTest.testIsNotInternal", false, tree.isInternal(tree.right(tree.root())));
    }

    private static void testBreadthFirst() {
        LinkedBinaryTree<Integer> tree = new LinkedBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        tree.add(tree.left(tree.root()), 4);
        tree.add(tree.left(tree.root()), 5);
        tree.add(tree.right(tree.root()), 6);
        String actual = "[1, 2, 3, 4, 5, 6]";
        assertEquals("linkedBinaryTreeTest.testBreadthFirst", actual, tree.breadthFirst().toString());
    }

    private static void testPreOrder() {
        LinkedBinaryTree<Integer> tree = new LinkedBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        tree.add(tree.left(tree.root()), 4);
        tree.add(tree.left(tree.root()), 5);
        tree.add(tree.right(tree.root()), 6);
        String actual = "[1, 2, 4, 5, 3, 6]";
        assertEquals("linkedBinaryTreeTest.testPreOrder", actual, tree.preOrder().toString());
    }

    private static void testPostOrder() {
        LinkedBinaryTree<Integer> tree = new LinkedBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        tree.add(tree.left(tree.root()), 4);
        tree.add(tree.left(tree.root()), 5);
        tree.add(tree.right(tree.root()), 6);
        String actual = "[4, 5, 2, 6, 3, 1]";
        assertEquals("linkedBinaryTreeTest.testPostOrder", actual, tree.postOrder().toString());
    }

    private static void tetChildrenNumber() {
        BinaryTree<Integer> tree = new LinkedBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        assertEquals("linkedBinaryTreeTest.tetChildrenNumber", 2, tree.childrenNumber(tree.root()));
    }

    private static void testChildren() {
        BinaryTree<Integer> tree = new LinkedBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        String expected = "[2, 3]";
        assertEquals("linkedBinaryTreeTest.testChildren", expected, tree.children(tree.root()).toString());
    }

    private static void testSibling() {
        BinaryTree<Integer> tree = new LinkedBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        tree.add(tree.left(tree.root()), 4);
        assertEquals("linkedBinaryTreeTest.testSiblingNull", null, tree.sibling(null));
        assertEquals("linkedBinaryTreeTest.testSiblingRoot", null, tree.sibling(tree.root()));
        assertEquals("linkedBinaryTreeTest.testSibling", tree.right(tree.root()),
                tree.sibling(tree.left(tree.root())));
        assertEquals("linkedBinaryTreeTest.testNotSibling", null,
                tree.sibling(tree.left(tree.left(tree.root()))));
    }

    private static void testNodes() {
        BinaryTree<Integer> tree = new LinkedBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        tree.add(tree.left(tree.root()), 4);
        tree.add(tree.left(tree.root()), 5);
        tree.add(tree.right(tree.root()), 6);
        String actual = "[4, 2, 5, 1, 6, 3]";
        assertEquals("linkedBinaryTreeTest.testNodes", actual, tree.nodes().toString());
    }

    private static void testInOrder() {
        LinkedBinaryTree<Integer> tree = new LinkedBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        tree.add(tree.left(tree.root()), 4);
        tree.add(tree.left(tree.root()), 5);
        tree.add(tree.right(tree.root()), 6);
        String actual = "[4, 2, 5, 1, 6, 3]";
        assertEquals("linkedBinaryTreeTest.testInOrder", actual, tree.inOrder().toString());
    }

    private static void testSize() {
        BinaryTree<Integer> tree = new LinkedBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        assertEquals("linkedBinaryTreeTest.testSize", 3, tree.size());
    }

    private static void testParent() {
        BinaryTree<Integer> tree = new LinkedBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        assertEquals("linkedBinaryTreeTest.testParent", tree.root(), tree.parent(tree.left(tree.root())));
    }

    private static void testRemove() {
        BinaryTree<Integer> tree = new LinkedBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        assertEquals("linkedBinaryTreeTest.testNotRemove", null, tree.remove(tree.root()));
        assertEquals("linkedBinaryTreeTest.testRemove", (Integer) 2, tree.remove(tree.left(tree.root())));
        assertEquals("linkedBinaryTreeTest.testRemoveRoot", (Integer) 1, tree.remove(tree.root()));
    }

    private static void testSet() {
        BinaryTree<Integer> tree = new LinkedBinaryTree<>();
        tree.addRoot(1);
        tree.add(tree.root(), 2);
        tree.add(tree.root(), 3);
        assertEquals("linkedBinaryTreeTest.testSet", (Integer) 2, tree.set(tree.left(tree.root()), 5));
    }

    private static void testAdd() {
        BinaryTree<Integer> tree = new LinkedBinaryTree<>();
        tree.addRoot(1);
        Node<Integer> actual = tree.add(tree.root(), 2);
        assertEquals("linkedBinaryTreeTest.testAddOne", tree.left(tree.root()), actual);
        actual = tree.add(tree.root(), 3);
        assertEquals("linkedBinaryTreeTest.testAddTwo", tree.right(tree.root()), actual);
        actual = tree.add(tree.root(), 4);
        assertEquals("linkedBinaryTreeTest.testNotAdd", null, actual);
    }

    private static void testAddRight() {
        BinaryTree<Integer> tree = new LinkedBinaryTree<>();
        tree.addRoot(1);
        Node<Integer> actual = tree.addRight(tree.root(), 2);
        assertEquals("linkedBinaryTreeTest.testAddRight", tree.right(tree.root()), actual);
    }

    private static void testAddLeft() {
        BinaryTree<Integer> tree = new LinkedBinaryTree<>();
        tree.addRoot(1);
        Node<Integer> actual = tree.addLeft(tree.root(), 2);
        assertEquals("linkedBinaryTreeTest.testAddLeft", tree.left(tree.root()), actual);
    }

    private static void testValidate() {
        try {
            LinkedBinaryTree<Integer> tree = new LinkedBinaryTree<>();
            Node<Integer> node = new LinkedBinaryTree.NodeImpl<>(2, null);
            assertEquals("linkedBinaryTreeTest.testValidate",
                    node, tree.validate(node));
            tree.validate(null);
            tree.validate(node);
            fail("There are not IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            assertEquals("linkedBinaryTreeTest.testValidateException",
                    "IllegalArgumentException: the node is not an instance of supported", e.getMessage());
        }
    }

    private static void testAddRoot() {
        try {
            BinaryTree<Integer> tree = new LinkedBinaryTree<>();
            LinkedBinaryTree.NodeImpl<Integer> actual = (LinkedBinaryTree.NodeImpl) tree.addRoot(1);
            String actualStr = "element:" + actual.getElement() + " left:" + actual.getLeft() + " parent:" +
                    actual.getParent() + " right:" + actual.getRight();
            String expected = "element:1 left:null parent:null right:null";
            assertEquals("linkedBinaryTreeTest.testAddRoot", expected, actualStr);
            tree.addRoot(2);
            fail("There are not IllegalStateException");
        } catch (IllegalStateException e) {
            assertEquals("linkedBinaryTreeTest.testAddRootException",
                    "IllegalStateException: root already exists", e.getMessage());
        }
    }
}