package com.getjavajob.training.algo1712.makhovak.lesson04;

import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.training.util.Assert.assertEquals;

public class SinglyLinkedListTest {
    public static void main(String[] args) {
        testAdd();
        testGet();
        testSize();
        testAsList();
        testReverse();
    }

    private static void testReverse() {
        SinglyLinkedList<Integer> testSLL = new SinglyLinkedList<>();
        testSLL.add(1);
        testSLL.add(2);
        testSLL.add(3);
        testSLL.add(4);
        testSLL.reverse();
        Object[] exp = {4, 3, 2, 1};
        assertEquals("SLLTest.testReverse", exp, testSLL.asList().toArray());
    }

    private static void testAsList() {
        SinglyLinkedList<Integer> testSLL = new SinglyLinkedList<>();
        testSLL.add(1);
        testSLL.add(2);
        testSLL.add(3);
        List<Object> exp = new ArrayList<>(3);
        exp.add(1);
        exp.add(2);
        exp.add(3);
        assertEquals("SLLTest.testAsList", exp, testSLL.asList());
    }

    private static void testSize() {
        SinglyLinkedList<Integer> testSLL = new SinglyLinkedList<>();
        testSLL.add(1);
        testSLL.add(2);
        testSLL.add(3);
        assertEquals("SLLTest.testSize", 3, testSLL.size());
    }

    private static void testGet() {
        SinglyLinkedList<Integer> testSLL = new SinglyLinkedList<>();
        testSLL.add(1);
        testSLL.add(2);
        testSLL.add(3);
        assertEquals("SLLTest.testGet", (Integer) 2, testSLL.get(1));
    }

    private static void testAdd() {
        SinglyLinkedList<Integer> testSLL = new SinglyLinkedList<>();
        testSLL.add(1);
        testSLL.add(2);
        testSLL.add(3);
        Object[] exp = {1, 2, 3};
        assertEquals("SLLTest.testAdd", exp, testSLL.asList().toArray());
    }
}