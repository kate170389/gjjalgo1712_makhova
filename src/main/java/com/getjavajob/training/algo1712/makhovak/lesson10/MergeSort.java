package com.getjavajob.training.algo1712.makhovak.lesson10;

public class MergeSort {
    private byte[] array;
    private byte[] tmp;

    public void sort(byte[] array) {
        this.array = array;
        this.tmp = new byte[array.length];
        mergeSort(0, array.length - 1);
    }

    private void mergeSort(int lo, int hi) {
        if (lo < hi) {
            int mid = lo + (hi - lo) / 2;
            mergeSort(lo, mid);
            mergeSort(mid + 1, hi);
            merge(lo, mid, hi);
        }
    }

    private void merge(int lo, int mid, int hi) {
        System.arraycopy(array, lo, tmp, lo, hi + 1 - lo);
        int i = lo, j = mid + 1, k = lo;
        while (i <= mid && j <= hi) {
            if (tmp[i] <= tmp[j]) {
                array[k] = tmp[i];
                i++;
            } else {
                array[k] = tmp[j];
                j++;
            }
            k++;
        }
        while (i <= mid) {
            array[k] = tmp[i];
            k++;
            i++;
        }
    }
}