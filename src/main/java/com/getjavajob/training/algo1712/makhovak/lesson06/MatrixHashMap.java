package com.getjavajob.training.algo1712.makhovak.lesson06;

import java.util.HashMap;
import java.util.Map;

public class MatrixHashMap<V> implements Matrix<V> {
    private Map<Key, V> map;
    private static int dimension;

    public MatrixHashMap(int n) {
        if (n <= 0) {
            throw new IllegalArgumentException("Illegal Size Matrix: The size must be more than 0 ");
        }
        map = new HashMap<>(n);
        dimension = n;
    }

    private static class Key {
        private final int i;
        private final int j;

        Key(int i, int j) {
            this.i = i;
            this.j = j;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || o.getClass() != this.getClass()) {
                return false;
            }
            Key key = (Key) o;
            return i == key.i && j == key.j;
        }

        @Override
        public int hashCode() {
            Integer element = i * dimension + j;
            return element.hashCode();
        }
    }

    @Override
    public V get(int i, int j) {
        if (i >= dimension || j >= dimension || i < 0 || j < 0) {
            throw new IndexOutOfBoundsException("IndexOutOfBoundsException Index: " + i + " , " + j + ", Size: " +
                    dimension + " Available indexes: " + "from 0 to " + (dimension - 1));
        }
        Key key = new Key(i, j);
        return map.get(key);
    }

    @Override
    public void set(int i, int j, V value) {
        if (i >= dimension || j >= dimension || i < 0 || j < 0) {
            throw new IndexOutOfBoundsException("IndexOutOfBoundsException Index: " + i + " , " + j + ", Size: " +
                    dimension + " Available indexes: " + "from 0 to " + (dimension - 1));
        }
        Key key = new Key(i, j);
        map.put(key, value);
    }

    public Object[] toArray() {
        return map.values().toArray();
    }

    public Map<Integer, V> hashMap() {
        Map<Integer, V> newMap = new HashMap<>();
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                V var = get(i, j);
                if (var != null) {
                    newMap.put(new Key(i, j).hashCode(), var);
                }
            }
        }
        return newMap;
    }
}