package com.getjavajob.training.algo1712.makhovak.lesson10;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static com.getjavajob.training.util.Assert.assertEquals;

public class ArraysTests {
    public static void main(String[] args) {
        testAsList();
        testBinarySearch();
        testCopyOf();
        testCopyOfRange();
        testDeepEquals();
        testEquals();
        testDeepHashCode();
        testHashCode();
        testDeepToString();
        testToString();
        testSort();
        testFill();
    }

    private static void testFill() {
        int[] array = new int[]{1, 4, 2, 7, 2};
        int[] expected = new int[]{2, 2, 2, 2, 2};
        Arrays.fill(array, 2);
        assertEquals("ArraysTests.testFill", expected, array);
        Arrays.fill(array, 1, 3, 3);
        expected = new int[]{2, 3, 3, 2, 2};
        assertEquals("ArraysTests.testFillFromIndToInd", expected, array);
    }

    private static void testSort() {
        int[] array = new int[]{1, 4, 2, 7, 2, 0};
        int[] expected = new int[]{1, 2, 4, 7, 2, 0};
        Arrays.sort(array, 1, 4);
        assertEquals("ArraysTests.testSortFromIndToInd", expected, array);
        expected = new int[]{0, 1, 2, 2, 4, 7};
        Arrays.sort(array);
        assertEquals("ArraysTests.testSort", expected, array);
    }

    private static void testToString() {
        int[] array = new int[]{1, 4, 2, 7, 2, 0};
        String expected = "[1, 4, 2, 7, 2, 0]";
        assertEquals("ArraysTests.testToString", expected, Arrays.toString(array));
    }

    private static void testDeepToString() {
        int[] p = new int[]{1, 4, 2, 7, 2};
        Object[] array = new Object[]{p};
        String expected = "[[1, 4, 2, 7, 2]]";
        assertEquals("ArraysTests.testDeepToString", expected, Arrays.deepToString(array));
    }

    private static void testHashCode() {
        Integer[] array = new Integer[]{1};
        List<Integer> a = Arrays.asList(array);  //it will be 32
        assertEquals("ArraysTests.testHashCode", Objects.hashCode(a), Arrays.hashCode(array));
    }

    private static void testDeepHashCode() {
        int[] p = new int[]{1};
        Object[] array = new Object[]{p};
        int f = Arrays.hashCode(p);  //it will be 32
        int expected = Arrays.hashCode(new Object[]{f}); //it will be 63
        assertEquals("ArraysTests.testDeepHashCode", expected, Arrays.deepHashCode(array));
    }

    private static void testEquals() {
        String[] p = new String[]{"2"};
        Object[] arrayOne = new Object[]{"1", p};
        p = new String[]{"2"};
        Object[] arrayTwo = new Object[]{"1", p};
        assertEquals("ArraysTests.testNotEquals", false, Arrays.equals(arrayOne, arrayTwo));
        arrayTwo = arrayOne;
        assertEquals("ArraysTests.testEquals", true, Arrays.equals(arrayOne, arrayTwo));
        arrayOne = new String[]{"1", "2"};
        arrayTwo = new String[]{"1", "2"};
        assertEquals("ArraysTests.testEquals", true, Arrays.equals(arrayOne, arrayTwo));
        arrayTwo = new String[]{"1", "3"};
        assertEquals("ArraysTests.testNotEquals", false, Arrays.equals(arrayOne, arrayTwo));
    }

    private static void testDeepEquals() {
        String[] p = new String[]{"2"};
        Object[] arrayOne = new Object[]{"1", p};
        p = new String[]{"2"};
        Object[] arrayTwo = new Object[]{"1", p};
        assertEquals("ArraysTests.testDeepEquals", true, Arrays.deepEquals(arrayOne, arrayTwo));
        arrayTwo = new Object[]{"1", "3"};
        assertEquals("ArraysTests.testNotDeepEquals", false, Arrays.deepEquals(arrayOne, arrayTwo));
    }

    private static void testCopyOfRange() {
        int[] array = new int[]{1, 4, 2, 7, 2};
        int[] expected = new int[]{4, 2};
        assertEquals("ArraysTests.testCopyOfRangeLessThanLength", expected, Arrays.copyOfRange(array, 1, 3));
        expected = new int[]{4, 2, 7, 2, 0, 0};
        assertEquals("ArraysTests.testCopyOfRangeMoreThanLength", expected, Arrays.copyOfRange(array, 1, 7));
    }

    private static void testCopyOf() {
        int[] array = new int[]{1, 4, 2, 7, 2};
        int[] expected = new int[]{1, 4, 2};
        assertEquals("ArraysTests.testCopyOfLessThanLength", expected, Arrays.copyOf(array, 3));
        expected = new int[]{1, 4, 2, 7, 2, 0, 0};
        assertEquals("ArraysTests.testCopyOfMoreThanLength", expected, Arrays.copyOf(array, 7));
    }

    private static void testBinarySearch() {
        String[] array = new String[]{"1", "2", "3"};
        assertEquals("ArraysTests.testBinarySearch", 1, Arrays.binarySearch(array, "2"));
        assertEquals("ArraysTests.testBinarySearchNot Found", -4, Arrays.binarySearch(array, "9"));
        assertEquals("ArraysTests.testBinarySearchFromIndexToIndex",
                2, Arrays.binarySearch(array, 0, 3, "3"));
        assertEquals("ArraysTests.testBinarySearchFromIndexToIndexNotFound",
                -3, Arrays.binarySearch(array, 0, 2, "3"));
    }

    private static void testAsList() {
        String[] array = new String[]{"1", "2", "3"};
        List<String> list = new ArrayList<>();
        list.add("1");
        list.add("2");
        list.add("3");
        assertEquals("ArraysTests.testAsList", list, Arrays.asList(array));
    }
}