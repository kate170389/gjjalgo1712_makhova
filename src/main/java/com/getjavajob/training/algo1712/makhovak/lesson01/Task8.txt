a) type extension for positive:
if the size of the whole number component is too large to fit into the target integer type,then that value will be
reduced modulo the target type’s range.
(if max_number_of_bits/number is 0 or even, then result == max_number_of_bits%number.
If max_number_of_bits/number is add, then result ==-(max_number_of_bits - max_number_of_bits%number)
 And then the first bit is the sign. In left positions adds missing bits for the target type, according the sign bit.

example:
byte b=(byte)267   //      267/128==2(even) -> 267%128==11 -> (byte)267==11==00001011
                   // or:  (byte)267==(delete first bit)(sign bit==0)1_00001011==00001011==11
(int)b ==00000000_00001011==11

byte b=8
b==00001000
(int)b == 00000000_00001000

byte b=(byte)136   //     136/128==1 (odd) -> byte(136)== - (128-136%128)==-(128-8)==-120
                   //or:  (byte)136==(sign bit==1)10001000==-(~(0001000-1_==~0000111==1111000 (120))==-120
(int)b==11111111_10001000==-120




b)type extension for negative:
If the size of the whole number is norm, then in the left positions adds bits=1 for the target type.
If the size of the whole number component is too large to fit into the target integer type,then that value will be
reduced modulo the target type’s range. And then the first bit is the sign.
(if max_number_of_bits/number is 0 or even, then result == - max_number_of_bits%number.
 If max_number_of_bits/number is add, then result  == +(max_number_of_bits - max_number_of_bits%number)
 In left positions adds missing bits for the target type, according the sign bit.

byte b=(byte)-267   //     -267/128==-2(even) -> (byte)-267 == - 267%128==-11
                    //or:  (byte)-267==~100001011+1==011110100+1==(first sign bit==-1)1_1110101==-(~(1110101-1))==
                                                                                     == -(~1110100)==-0001011==-11
(int)b==11111111_11110101==-11

byte b=(byte)-136   //      -136/128==-1(odd) -> (byte)-136 ==128-136%128==128-8==120
                    // or:  (byte)-136==~10001000+1==01110111+1==01111000==120
(int)b==00000000_01111000==120

byte b=-15
(byte)b==(sign bit==1)11110001==-15
(int)b==11111111_11110001==-15