package com.getjavajob.training.algo1712.makhovak.lesson10;

import java.util.*;

import static com.getjavajob.training.util.Assert.assertEquals;

public class CollectionsTests {
    public static void main(String[] args) {
        testAddAll();
        testAsLifoQueue();
        testBinarySearch();
        testCopy();
        testDisjoint();
        testEmptyEnumeration();
        testCheckedCollection();
        testFill();
        testFrequency();
        testIndexOfSubList();
        testLastIndexOfSubList();
        testMax();
        testMin();
        testNCopies();
        testReplaceAll();
        testReverse();
        testReverseOrder();
        testRotate();
        testNewSetFromMap();
        testSingletonList();
        testSort();
        testSwap();
        testUnmodifiableList();
    }

    private static void testUnmodifiableList() {
        List<Integer> list = new ArrayList<>();
        list.add(13);
        list.add(9);
        list.add(1);
        list.add(6);
        List<Integer> actual = Collections.unmodifiableList(list);
        try {
            actual.add(20);
        } catch (Exception e) {
            assertEquals("CollectionsTests.testUnmodifiableListException",
                    "java.lang.UnsupportedOperationException", e.toString());
        }
    }

    private static void testSwap() {
        List<Integer> list = new ArrayList<>();
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(1);
        list.add(2);
        Collections.swap(list, 1, 5);
        List<Integer> expected = new ArrayList<>();
        expected.add(3);
        expected.add(2);
        expected.add(5);
        expected.add(6);
        expected.add(1);
        expected.add(4);
        assertEquals("CollectionsTests.testSwap", expected, list);
    }

    private static void testSort() {
        List<Integer> list = new ArrayList<>();
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(1);
        list.add(2);
        List<Integer> expected = new ArrayList<>();
        expected.add(1);
        expected.add(2);
        expected.add(3);
        expected.add(4);
        expected.add(5);
        expected.add(6);
        Collections.sort(list);
        assertEquals("CollectionsTests.testSort", expected, list);
    }

    private static void testSingletonList() {
        List<Integer> actual = Collections.singletonList(4);
        List<Integer> expected = new ArrayList<>();
        expected.add(4);
        assertEquals("CollectionsTests.testSingletonList",
                expected, actual);
        try {
            actual.add(1);
        } catch (Exception e) {
            assertEquals("CollectionsTests.testSingletonListException",
                    "java.lang.UnsupportedOperationException", e.toString());
        }
    }

    private static void testNewSetFromMap() {
        Map<Integer, Boolean> map = new HashMap<>();
        map.put(1, true);
        map.put(2, true);
        Map<Integer, Boolean> actual = new HashMap<>();
        Set<Integer> set = Collections.newSetFromMap(actual);
        set.add(1);
        set.add(2);
        assertEquals("CollectionsTests.testNewSetFromMap", map, actual);
    }

    private static void testRotate() {
        List<Integer> myList = new ArrayList<>();
        myList.add(1);
        myList.add(2);
        myList.add(3);
        myList.add(4);
        myList.add(5);
        myList.add(6);
        List<Integer> expected = new ArrayList<>();
        expected.add(3);
        expected.add(4);
        expected.add(5);
        expected.add(6);
        expected.add(1);
        expected.add(2);
        Collections.rotate(myList, 4);
        assertEquals("CollectionsTests.testRotate", expected, myList);
    }

    private static void testReverseOrder() {
        Comparator<Integer> actual = Collections.reverseOrder();
        Integer o1 = 1;
        Integer o2 = 2;
        assertEquals("CollectionsTests.testReverseOrder", o1.compareTo(o2), actual.compare(o2, o1));
    }

    private static void testReverse() {
        List<String> myList = new ArrayList<>();
        myList.add("one");
        myList.add("two");
        myList.add("three");
        List<String> expected = new ArrayList<>();
        expected.add("three");
        expected.add("two");
        expected.add("one");
        Collections.reverse(myList);
        assertEquals("CollectionsTests.testReverse", expected, myList);
    }

    private static void testReplaceAll() {
        List<String> myList = new ArrayList<>();
        myList.add("one");
        myList.add("two");
        myList.add("three");
        myList.add("one");
        myList.add("two");
        List<String> expected = new ArrayList<>();
        expected.add("REPLACED");
        expected.add("two");
        expected.add("three");
        expected.add("REPLACED");
        expected.add("two");
        Collections.replaceAll(myList, "one", "REPLACED");
        assertEquals("CollectionsTests.testReplaceAll", expected, myList);
    }

    private static void testNCopies() {
        List<Integer> actual = Collections.nCopies(3, 5);
        List<Integer> expected = new ArrayList<>();
        expected.add(5);
        expected.add(5);
        expected.add(5);
        assertEquals("CollectionsTests.testNCopies", expected, actual);
    }

    private static void testMin() {
        List<Integer> myList = new ArrayList<>();
        myList.add(1);
        myList.add(4);
        myList.add(1);
        myList.add(8);
        myList.add(1);
        assertEquals("CollectionsTests.testMin", (Integer) 1, Collections.min(myList));
    }

    private static void testMax() {
        List<Integer> myList = new ArrayList<>();
        myList.add(1);
        myList.add(4);
        myList.add(1);
        myList.add(8);
        myList.add(1);
        assertEquals("CollectionsTests.testMax", (Integer) 8, Collections.max(myList));
    }

    private static void testLastIndexOfSubList() {
        List<String> myList = new ArrayList<>();
        myList.add("one");
        myList.add("two");
        myList.add("three");
        myList.add("four");
        myList.add("one");
        myList.add("two");
        myList.add("one");
        myList.add("two");
        List<String> subList = new ArrayList<>();
        subList.add("one");
        subList.add("two");
        assertEquals("CollectionsTests.testLastIndexOfSubList", 6, Collections.lastIndexOfSubList(myList, subList));
        subList.add("four");
        assertEquals("CollectionsTests.testLastIndexOfSubListNotIncludeList", -1,
                Collections.lastIndexOfSubList(myList, subList));
    }

    private static void testIndexOfSubList() {
        List<String> myList = new ArrayList<>();
        myList.add("one");
        myList.add("two");
        myList.add("three");
        myList.add("four");
        List<String> subList = new ArrayList<>();
        subList.add("two");
        subList.add("three");
        assertEquals("CollectionsTests.testIndexOfSubList", 1, Collections.indexOfSubList(myList, subList));
        subList.add("three");
        assertEquals("CollectionsTests.testIndexOfSubListNotIncludeList", -1,
                Collections.indexOfSubList(myList, subList));
    }

    private static void testFrequency() {
        List<Integer> myList = new ArrayList<>();
        myList.add(1);
        myList.add(4);
        myList.add(1);
        myList.add(8);
        myList.add(1);
        assertEquals("CollectionsTests.testFrequency", 3, Collections.frequency(myList, 1));
        assertEquals("CollectionsTests.testFrequencyNotFound", 0, Collections.frequency(myList, 10));
    }

    private static void testFill() {
        List<String> myList = new ArrayList<>();
        myList.add("one");
        myList.add("two");
        List<String> expected = new ArrayList<>();
        expected.add("FILL");
        expected.add("FILL");
        Collections.fill(myList, "FILL");
        assertEquals("CollectionsTests.testFill", expected, myList);

    }

    private static void testCheckedCollection() {
        List myList = new ArrayList();
        myList.add("one");
        myList.add("two");
        myList.add("three");
        myList.add("four");
        Collection chkList = Collections.checkedCollection(myList, String.class);
        assertEquals("CollectionsTests.testCheckedCollection", true, myList.add(10));
        try {
            //you cannot add any type of elements to chkList object, doing so
            //throws ClassCastException
            chkList.add(10); //throws ClassCastException
        } catch (Exception cce) {
            assertEquals("CollectionsTests.testCheckedCollectionClassCastException",
                    "Attempt to insert class java.lang.Integer element into collection with element type class " +
                            "java.lang.String", cce.getMessage());
        }
    }

    private static void testEmptyEnumeration() {
        List<String> ls = new ArrayList<>();
        ls.add("one");
        Enumeration<String> enm = Collections.enumeration(ls);
        assertEquals("CollectionsTests.testEmptyEnumerationTrue", true, enm.hasMoreElements());
        assertEquals("CollectionsTests.testEmptyEnumerationNextElement", "one", enm.nextElement());
        assertEquals("CollectionsTests.testEmptyEnumerationFalse", false, enm.hasMoreElements());
    }

    private static void testDisjoint() {
        List<String> names = new ArrayList<>();
        Collections.addAll(names, "Alex", "Bob", "Morgan");
        List<String> namesTwo = new ArrayList<>();
        Collections.addAll(namesTwo, "A", "B", "C");
        assertEquals("CollectionsTests.testDisjointTrue", true, Collections.disjoint(names, namesTwo));
        namesTwo.add("Alex");
        assertEquals("CollectionsTests.testDisjointFalse", false, Collections.disjoint(names, namesTwo));
    }

    private static void testCopy() {
        List<String> names = new ArrayList<>();
        Collections.addAll(names, "Alex", "Bob", "Morgan");
        List<String> namesTwo = new ArrayList<>();
        Collections.addAll(namesTwo, "A", "B", "C");
        Collections.copy(namesTwo, names);
        assertEquals("CollectionsTests.testCopy", names, namesTwo);
    }

    private static void testBinarySearch() {
        List<String> names = new ArrayList<>();
        //it must be sorted, If not - the binarySearch will be (-(insertion point) - 1)
        Collections.addAll(names, "Alex", "Bob", "Ann");
        assertEquals("CollectionsTests.testBinarySearch", 1, Collections.binarySearch(names, "Bob"));
        assertEquals("CollectionsTests.testBinarySearchNotSorted",
                -2, Collections.binarySearch(names, "Ann"));
        assertEquals("CollectionsTests.testBinarySearchNotFound",
                -1, Collections.binarySearch(names, "A"));
    }

    private static void testAsLifoQueue() {
        Deque<String> deque = new ArrayDeque<>();
        deque.add("one");
        deque.add("two");
        deque.add("three");
        String expected = deque.pop();
        Collections.asLifoQueue(deque).add("one");
        String actual = Collections.asLifoQueue(deque).remove();
        assertEquals("CollectionsTests.testAsLifoQueue", expected, actual);
    }

    private static void testAddAll() {
        List<String> names = new ArrayList<>();
        Collections.addAll(names, "Alex", "Bob");
        String[] array = new String[]{"Alex", "Bob"};
        List<String> expected = Arrays.asList(array);
        assertEquals("CollectionsTests.testAddAll", expected, names);
    }
}