
-------- Addition to the beginning --------
DynamicArray.add(0,e):8179ms
ArrayList.add(0,e):7680ms

-------- Addition to the middle --------
DynamicArray.add(3,e):7948ms
ArrayList.add(3,e):7755ms

-------- Addition to the end --------
DynamicArray.add(size,e):11513ms
ArrayList.add(size,e):11915ms

-------- Remove from the beginning --------
DynamicArray.remove(0):8414ms
ArrayList.remove(0):7662ms

-------- Remove from the middle --------
DynamicArray.remove(i):4045ms
ArrayList.remove(i):3812ms

-------- Remove from the end --------
DynamicArray.remove(last):128ms
ArrayList.remove(last):109ms


-------- Remove from the end --------
DynamicArray.remove(last):543ms
ArrayList.remove(last):505ms

(if more Java heap space)






