package com.getjavajob.training.algo1712.makhovak.lesson05;

import static com.getjavajob.training.util.Assert.assertEquals;

public class ExpressionCalculatorTest {
    public static void main(String[] args) {
        testIsOperator();
        testIsDouble();
        testCheckExpression();
        testPriorityOperators();
        testCalculateBinaryOperation();
        testCalculatesResultExpression();
    }

    private static void testIsOperator() {
        ExpressionCalculator calculator = new ExpressionCalculator("1+1");
        assertEquals("ExpressionCalculatorTest.testIsOperatorTrue", true,
                calculator.isOperator("*"));
        assertEquals("ExpressionCalculatorTest.testIsOperatorFalse", false,
                calculator.isOperator("4256"));
    }

    private static void testIsDouble() {
        ExpressionCalculator calculator = new ExpressionCalculator("1+1");
        assertEquals("ExpressionCalculatorTest.testIsDoubleTrue", true,
                calculator.isDouble("4256"));
        assertEquals("ExpressionCalculatorTest.testIsDoubleFalse", false,
                calculator.isDouble("hgf+-*"));
    }

    private static void testCheckExpression() {
        String test = "1+1   -   ( -7+5  )";
        ExpressionCalculator calculator = new ExpressionCalculator(test);
        String[] exp = {"1", "+", "1", "-", "(", "0", "-", "7", "+", "5", ")"};
        assertEquals("ExpressionCalculatorTest.testCheckExpression", exp, calculator.checkExpression(test));
    }

    private static void testPriorityOperators() {
        ExpressionCalculator calculator = new ExpressionCalculator("1+1");
        assertEquals("ExpressionCalculatorTest.testPriorityOperators-", 1,
                calculator.priorityOperators("-"));
        assertEquals("ExpressionCalculatorTest.testPriorityOperators+", 1,
                calculator.priorityOperators("+"));
        assertEquals("ExpressionCalculatorTest.testPriorityOperators*", 2,
                calculator.priorityOperators("*"));
        assertEquals("ExpressionCalculatorTest.testPriorityOperators/", 2,
                calculator.priorityOperators("/"));
    }

    private static void testCalculateBinaryOperation() {
        ExpressionCalculator calculator = new ExpressionCalculator("1+1");
        assertEquals("ExpressionCalculatorTest.testCalculateBinaryOperation-", "1.0",
                calculator.calculateBinaryOperation(6.0, 5.0, "-"));
        assertEquals("ExpressionCalculatorTest.testCalculateBinaryOperation+", "11.0",
                calculator.calculateBinaryOperation(6.0, 5.0, "+"));
        assertEquals("ExpressionCalculatorTest.testCalculateBinaryOperation*", "30.0",
                calculator.calculateBinaryOperation(6.0, 5.0, "*"));
        assertEquals("ExpressionCalculatorTest.testCalculateBinaryOperation/", "2.0",
                calculator.calculateBinaryOperation(6.0, 3.0, "/"));
    }

    private static void testCalculatesResultExpression() {
        ExpressionCalculator calculator = new ExpressionCalculator("2-((3+1-2)*7)+100");
        assertEquals("ExpressionCalculatorTest.testIsOperator", "88.0",
                calculator.calculatesResultExpression());
    }
}