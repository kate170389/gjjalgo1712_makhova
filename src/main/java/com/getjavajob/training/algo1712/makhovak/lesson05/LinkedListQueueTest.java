package com.getjavajob.training.algo1712.makhovak.lesson05;

import java.util.LinkedList;
import java.util.List;

import static com.getjavajob.training.util.Assert.assertEquals;


public class LinkedListQueueTest {
    public static void main(String[] args) {
        testAdd();
        testRemove();
    }

    public static void testAdd() {
        LinkedListQueue<Integer> queue = new LinkedListQueue<>();
        queue.add(1);
        queue.add(2);
        queue.add(3);
        queue.add(4);
        List<Integer> exp = new LinkedList<>();
        exp.add(4);
        exp.add(3);
        exp.add(2);
        exp.add(1);
        assertEquals("LLQueueTest.testAdd", exp, queue.asList());
    }

    public static void testRemove() {
        LinkedListQueue<Integer> queue = new LinkedListQueue<>();
        queue.add(1);
        queue.add(2);
        queue.add(3);
        queue.add(4);
        queue.add(5);
        List<Integer> exp = new LinkedList<>();
        exp.add(5);
        exp.add(4);
        exp.add(3);
        assertEquals("LLQueueTest.testRemove", (Integer) 1, queue.remove());
        queue.remove();
        assertEquals("LLQueueTest.testRemove", exp, queue.asList());
    }
}