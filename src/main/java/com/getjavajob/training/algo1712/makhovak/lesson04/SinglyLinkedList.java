package com.getjavajob.training.algo1712.makhovak.lesson04;

import java.util.ArrayList;
import java.util.List;

public class SinglyLinkedList<V> {
    private Node<V> head;
    private int size;

    public void add(V val) {
        Node<V> e = new Node<>(null, val);
        Node<V> curElement = head;
        if (head == null) {
            head = e;
        } else {
            for (int i = 1; i < size; i++) {
                curElement = curElement.next;
            }
            curElement.next = e;
        }
        size++;
    }

    public V get(int index) {
        handleIndicesRangeForAdd(index);
        Node<V> pointer = head;
        for (int i = 0; i < index; i++) {
            pointer = pointer.next;
        }
        return pointer.val;
    }

    public int size() {
        return size;
    }

    public void reverse() {
        if (size > 0) {
            Node<V> last = head;
            Node<V> prev = head.next;
            Node<V> next;
            relink(last, null);
            while (prev != null) {
                next = prev.next;
                relink(prev, last);
                last = prev;
                prev = next;
            }
            head = last;
        }
    }

    private void relink(Node<V> current, Node<V> next) {
        current.next = next;
    }

    public List<V> asList() {
        List<V> a = new ArrayList<>(size);
        Node<V> element = head;
        for (int i = 0; i < size; i++) {
            a.add(i, element.val);
            element = element.next;
        }
        return a;
    }

    private static class Node<V> {
        private Node<V> next;
        private V val;

        Node(Node<V> next, V element) {
            this.val = element;
            this.next = next;
        }
    }

    private void handleIndicesRangeForAdd(int index) {
        if (index > size || index < 0) {
            throw new IndexOutOfBoundsException("IndexOutOfBoundsException Index: " + index + ", Size: " + size +
                    " Available indexes: from 0 to " + (size - 1));
        }
    }
}