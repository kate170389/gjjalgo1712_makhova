package com.getjavajob.training.algo1712.makhovak.lesson09;

import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;

import static com.getjavajob.training.util.Assert.assertEquals;

public class SortedSetTest {
    public static void main(String[] args) {
        testComparator();
        testSubSet();
        testHeadSet();
        testTailSet();
        testFirst();
        testLast();
    }

    private static void testLast() {
        SortedSet<Integer> tree = new TreeSet<>();
        tree.add(20);
        tree.add(2);
        tree.add(1);
        tree.add(35);
        tree.add(6);
        tree.add(40);
        tree.add(21);
        assertEquals("SortedSetTest.testLast", (Integer) 40, tree.last());
    }

    private static void testFirst() {
        SortedSet<Integer> tree = new TreeSet<>();
        tree.add(20);
        tree.add(2);
        tree.add(1);
        tree.add(35);
        tree.add(6);
        tree.add(40);
        tree.add(21);
        assertEquals("SortedSetTest.testFirst", (Integer) 1, tree.first());
    }

    private static void testTailSet() {
        SortedSet<Integer> tree = new TreeSet<>();
        tree.add(20);
        tree.add(2);
        tree.add(1);
        tree.add(35);
        tree.add(6);
        tree.add(40);
        tree.add(21);
        SortedSet<Integer> expected = new TreeSet<>();
        expected.add(20);
        expected.add(35);
        expected.add(21);
        expected.add(40);
        String expectedString = "[20, 21, 35, 40]";
        assertEquals("SortedSetTest.testTailSet", expected, tree.tailSet(20));
        assertEquals("SortedSetTest.testTailSetToString", expectedString, tree.tailSet(20).toString());
    }

    private static void testHeadSet() {
        SortedSet<Integer> tree = new TreeSet<>();
        tree.add(20);
        tree.add(2);
        tree.add(1);
        tree.add(35);
        tree.add(6);
        tree.add(40);
        tree.add(21);
        SortedSet<Integer> expected = new TreeSet<>();
        expected.add(20);
        expected.add(6);
        expected.add(1);
        expected.add(2);
        String expectedString = "[1, 2, 6, 20]";
        assertEquals("SortedSetTest.testHeadSet", expected, tree.headSet(21));
        assertEquals("SortedSetTest.testHeadSetToString", expectedString, tree.headSet(21).toString());
    }

    private static void testSubSet() {
        SortedSet<Integer> tree = new TreeSet<>();
        tree.add(20);
        tree.add(2);
        tree.add(1);
        tree.add(35);
        tree.add(6);
        tree.add(40);
        tree.add(21);
        SortedSet<Integer> expected = new TreeSet<>();
        expected.add(20);
        expected.add(6);
        expected.add(21);
        String expectedString = "[6, 20, 21]";
        assertEquals("SortedSetTest.testSubSet", expected, tree.subSet(6, 35));
        assertEquals("SortedSetTest.testSubSetToString", expectedString, tree.subSet(6, 35).toString());
    }

    private static void testComparator() {
        Comparator<Integer> comp = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1.compareTo(o2);
            }
        };
        SortedSet<Integer> tree = new TreeSet<>(comp);
        tree.add(5);
        tree.add(2);
        tree.add(1);
        assertEquals("SortedSetTest.testComparator", comp, tree.comparator());
        tree = new TreeSet<>();
        assertEquals("SortedSetTest.testComparatorNull", null, tree.comparator());
    }
}