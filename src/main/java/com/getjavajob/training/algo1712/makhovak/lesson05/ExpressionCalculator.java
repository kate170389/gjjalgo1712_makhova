package com.getjavajob.training.algo1712.makhovak.lesson05;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.StringTokenizer;

public class ExpressionCalculator {
    private Deque<String> postfixNotation = new ArrayDeque<>();
    private Deque<String> operatorStack = new ArrayDeque<>();
    private String[] expression;

    ExpressionCalculator(String expression) {
        this.expression = checkExpression(expression);
    }

    public String[] checkExpression(String expression) {
        if (!checkValidInput(expression) || !checkBrackets(expression)) {
            throw new IllegalArgumentException("There are not valid expression");
        }
        return inputValidExpression(createNewExpression(expression));
    }

    private String[] inputValidExpression(String expression) {
        String[] expressionArray = expression.split(" ");
        if (expressionArray[0].equals("-")) {
            String[] newExpressionArray = new String[expressionArray.length + 1];
            System.arraycopy(expressionArray, 0, newExpressionArray, 1, expressionArray.length);
            newExpressionArray[0] = "0";
            expressionArray = newExpressionArray;
        }

        if (expressionArray.length < 3 || isOperator(expressionArray[0]) || isOperator(expressionArray[expressionArray.length - 1])) {
            throw new IllegalArgumentException("There are not valid expression");
        }

        for (int i = 1, flag = 0; i < expressionArray.length - 1; i++) {
            if (isDouble(expressionArray[i])) {
                flag = 1;
            }
            if (isOperator(expressionArray[i])) {
                flag = 2;
            }
            if (expressionArray[i].equals("(")) {
                flag = 3;
            }
            if (expressionArray[i].equals(")")) {
                flag = 4;
            }
            switch (flag) {
                case 1:
                    if (!(isOperator(expressionArray[i + 1]) || expressionArray[i + 1].equals(")"))) {
                        throw new IllegalArgumentException("There are not valid expression");
                    }
                    if (!(isOperator(expressionArray[i - 1]) || expressionArray[i - 1].equals("("))) {
                        throw new IllegalArgumentException("There are not valid expression");
                    }
                    i++;
                    break;
                case 2:
                    if (!(isDouble(expressionArray[i + 1]) || expressionArray[i + 1].equals("("))) {
                        throw new IllegalArgumentException("There are not valid expression");
                    }
                    if (!(isDouble(expressionArray[i - 1]) || expressionArray[i - 1].equals("(") ||
                            expressionArray[i - 1].equals(")"))) {
                        throw new IllegalArgumentException("There are not valid expression");
                    }
                    if (expressionArray[i].equals("-") && expressionArray[i - 1].equals("(")) {
                        String[] newExpressionArray = new String[expressionArray.length + 1];
                        System.arraycopy(expressionArray, 0, newExpressionArray, 0, i);
                        System.arraycopy(expressionArray, i, newExpressionArray, i + 1,
                                expressionArray.length - i);
                        newExpressionArray[i] = "0";
                        expressionArray = newExpressionArray;
                        i++;
                    }
                    i++;
                    break;
                case 3:
                    if (!(isDouble(expressionArray[i + 1]) || expressionArray[i + 1].equals("-")) ||
                            expressionArray[i + 1].equals("(")) {
                        throw new IllegalArgumentException("There are not valid expression");
                    }
                    if (!(isDouble(expressionArray[i - 1]) || expressionArray[i - 1].equals("("))) {
                        throw new IllegalArgumentException("There are not valid expression");
                    }
                    if (expressionArray[i + 1].equals("-")) {
                        String[] newExpressionArray = new String[expressionArray.length + 1];
                        System.arraycopy(expressionArray, 0, newExpressionArray, 0, i);
                        System.arraycopy(expressionArray, i, newExpressionArray, i + 1,
                                expressionArray.length - i);
                        newExpressionArray[i] = "0";
                        expressionArray = newExpressionArray;
                        i++;
                    }
                    i++;
                    break;
                case 4:
                    if (!isOperator(expressionArray[i + 1])) {
                        throw new IllegalArgumentException("There are not valid expression");
                    }
                    if (!isDouble(expressionArray[i - 1])) {
                        throw new IllegalArgumentException("There are not valid expression");
                    }
                    i++;
                    break;
                case 0:
                    throw new IllegalArgumentException("There are not valid expression");
            }
        }
        return expressionArray;
    }

    private String createNewExpression(String expression) {
        StringBuilder newExpression = new StringBuilder();
        StringTokenizer str = new StringTokenizer(expression, " +-*/()", true);
        while (str.hasMoreTokens()) {
            String element = str.nextToken();
            if (!" ".equals(element)) {
                newExpression.append(element);
                newExpression.append(' ');
            }
        }
        return newExpression.toString();
    }

    private boolean checkValidInput(String expression) {
        return expression.matches("[0-9 -*/+()-]+");
    }

    private boolean checkBrackets(String expression) {
        int countBrackets = 0;
        int lastLeftBrackets = -1;
        int lastRightBrackets = -1;
        for (int i = 0; i < expression.length(); i++) {
            if (lastLeftBrackets > lastRightBrackets && lastRightBrackets != -1 && countBrackets <= 0) {
                i = expression.length();
            } else {
                if (expression.charAt(i) == '(') {
                    countBrackets++;
                    lastLeftBrackets = i;
                } else {
                    if (expression.charAt(i) == ')') {
                        countBrackets--;
                        lastRightBrackets = i;
                    }
                }
            }
            if (countBrackets < 0) {
                i = expression.length();
            }
        }
        return countBrackets == 0;
    }

    public boolean isDouble(String element) {
        try {
            Double.parseDouble(element);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    public boolean isOperator(String element) {
        return "+".equals(element) || "-".equals(element) || "*".equals(element) || "/".equals(element);
    }

    public int priorityOperators(String element) {
        if ("+".equals(element) || "-".equals(element)) {
            return 1;
        }
        if ("*".equals(element) || "/".equals(element)) {
            return 2;
        }
        return 0;
    }

    private String reversePolishNotation(String[] expression) {
        for (String element : expression) {
            if (isDouble(element)) {
                postfixNotation.push(element);
            } else {
                if (operatorStack.isEmpty()) {
                    operatorStack.push(element);
                } else {
                    checkOperators(element);
                }
            }
        }
        while (!operatorStack.isEmpty()) {
            postfixNotation.push(operatorStack.pop());
        }
        return postfixNotation.toString();
    }

    public String calculateBinaryOperation(Double firstOperand, Double secondOperand, String operator) {
        if ("+".equals(operator)) {
            return Double.toString(firstOperand + secondOperand);
        }
        if ("-".equals(operator)) {
            return Double.toString(firstOperand - secondOperand);
        }
        if ("*".equals(operator)) {
            return Double.toString(firstOperand * secondOperand);
        }
        if ("/".equals(operator)) {
            return Double.toString(firstOperand / secondOperand);
        }
        return null;
    }

    public String calculatesResultExpression() {
        if (expression == null) {
            throw new NullPointerException("There are not valid expression");
        }
        reversePolishNotation(expression);
        Deque<String> result = new ArrayDeque<>();
        while (!postfixNotation.isEmpty()) {
            if (isDouble(postfixNotation.peekLast())) {
                result.push(postfixNotation.pollLast());
            } else {
                double secondOperand = Double.parseDouble(result.pop());
                double firstOperand = Double.parseDouble(result.pop());
                result.push(calculateBinaryOperation(firstOperand, secondOperand, postfixNotation.pollLast()));
            }
        }
        return result.pop();
    }

    private void checkOperators(String operator) {
        String lastInStack = operatorStack.peek();
        if ("(".equals(operator) || ")".equals(operator)) {
            if ("(".equals(operator)) {
                operatorStack.push(operator);
            }
            if (")".equals(operator)) {
                while (lastInStack != null && !lastInStack.equals("(")) {
                    postfixNotation.push(operatorStack.pop());
                    lastInStack = operatorStack.peek();
                }
                operatorStack.pop();
            }
        } else {
            while (lastInStack != null && (priorityOperators(lastInStack) == priorityOperators(operator) ||
                    priorityOperators(lastInStack) > priorityOperators(operator)) && !(lastInStack.equals("("))) {
                postfixNotation.push(operatorStack.pop());
                lastInStack = operatorStack.peek();
            }
            operatorStack.push(operator);
        }
    }
}