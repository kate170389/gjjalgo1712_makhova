package com.getjavajob.training.algo1712.makhovak.lesson09;

import java.util.Iterator;
import java.util.NavigableSet;
import java.util.SortedSet;
import java.util.TreeSet;

import static com.getjavajob.training.util.Assert.assertEquals;

public class NavigableSetTest {
    public static void main(String[] args) {

        testCeiling();
        testDescendingIterator();
        testSubSet();
        testDescendingSet();
        testFloor();
        testHeadSet();
        testHigher();
        testIterator();
        testLower();
        testPollFirst();
        testPollLast();
        testTailSet();
    }

    private static void testTailSet() {
        NavigableSet<String> tree = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
        tree.add("mo");
        tree.add("m");
        tree.add("MN");
        tree.add("mos");
        tree.add("Moscow");
        tree.add("mo n");
        tree.add("mr");
        tree.add("mp");
        NavigableSet<String> expected = new TreeSet<>();
        expected.add("mos");
        expected.add("mp");
        expected.add("Moscow");
        expected.add("mr");
        assertEquals("NavigableSetTest.testTailSet", expected, tree.tailSet("mos"));
        SortedSet<String> expectedTwo = new TreeSet<>();
        expectedTwo.add("mr");
        expectedTwo.add("mp");
        expectedTwo.add("Moscow");
        assertEquals("NavigableSetTest.testTailSetNotIncludeVal", expectedTwo, tree.tailSet("mos", false));
    }

    private static void testPollLast() {
        NavigableSet<Integer> tree = new TreeSet<>();
        assertEquals("NavigableSetTest.testPollLastNull", null, tree.pollLast());
        tree.add(2);
        tree.add(4);
        tree.add(10);
        tree.add(6);
        NavigableSet<Integer> expected = new TreeSet<>();
        expected.add(4);
        expected.add(6);
        expected.add(2);
        assertEquals("NavigableSetTest.testPollLast", (Integer) 10, tree.pollLast());
        assertEquals("NavigableSetTest.testPollLast", expected, tree);
    }

    private static void testPollFirst() {
        NavigableSet<Integer> tree = new TreeSet<>();
        assertEquals("NavigableSetTest.testPollFirstNull", null, tree.pollFirst());
        tree.add(2);
        tree.add(4);
        tree.add(10);
        tree.add(6);
        NavigableSet<Integer> expected = new TreeSet<>();
        expected.add(4);
        expected.add(6);
        expected.add(10);
        assertEquals("NavigableSetTest.testPollFirst", (Integer) 2, tree.pollFirst());
        assertEquals("NavigableSetTest.testPollFirst", expected, tree);
    }

    private static void testLower() {
        NavigableSet<Integer> tree = new TreeSet<>();
        tree.add(20);
        tree.add(4);
        tree.add(10);
        tree.add(35);
        tree.add(6);
        assertEquals("NavigableSetTest.testLower", (Integer) 4, tree.lower(6));
        assertEquals("NavigableSetTest.testLowerNull", null, tree.lower(4));
    }

    private static void testIterator() {
        NavigableSet<Integer> tree = new TreeSet<>();
        tree.add(20);
        tree.add(2);
        tree.add(1);
        tree.add(35);
        tree.add(6);
        StringBuilder actual = new StringBuilder();
        Iterator<Integer> it = tree.iterator();
        while (it.hasNext()) {
            actual.append(it.next() + " ");
        }
        String expected = "1 2 6 20 35 ";
        assertEquals("NavigableSetTest.testIterator", expected, actual.toString());
    }

    private static void testHigher() {
        NavigableSet<Integer> tree = new TreeSet<>();
        tree.add(20);
        tree.add(4);
        tree.add(10);
        tree.add(35);
        tree.add(6);
        assertEquals("NavigableSetTest.testHigher", (Integer) 20, tree.higher(10));
        assertEquals("NavigableSetTest.testHigherNull", null, tree.higher(35));
    }

    private static void testHeadSet() {
        NavigableSet<Integer> tree = new TreeSet<>();
        tree.add(20);
        tree.add(4);
        tree.add(10);
        tree.add(35);
        tree.add(6);
        SortedSet<Integer> expected = new TreeSet<>();
        expected.add(4);
        expected.add(6);
        assertEquals("NavigableSetTest.testHeadSet", expected, tree.headSet(10));
        expected.add(10);
        assertEquals("NavigableSetTest.testHeadSetIncludeVal", expected, tree.headSet(10, true));
    }

    private static void testFloor() {
        NavigableSet<Integer> tree = new TreeSet<>();
        tree.add(20);
        tree.add(4);
        tree.add(10);
        tree.add(35);
        tree.add(6);
        assertEquals("NavigableSetTest.testFloor", (Integer) 20, tree.floor(22));
        assertEquals("NavigableSetTest.testFloorNull", null, tree.floor(3));
    }

    private static void testDescendingSet() {
        NavigableSet<Integer> tree = new TreeSet<>();
        tree.add(20);
        tree.add(2);
        tree.add(1);
        tree.add(35);
        tree.add(6);
        String expected = "[35, 20, 6, 2, 1]";
        assertEquals("NavigableSetTest.testDescendingSet", expected, tree.descendingSet().toString());
    }

    private static void testDescendingIterator() {
        NavigableSet<Integer> tree = new TreeSet<>();
        tree.add(20);
        tree.add(2);
        tree.add(1);
        tree.add(35);
        tree.add(6);
        StringBuilder actual = new StringBuilder();
        Iterator<Integer> it = tree.descendingIterator();
        while (it.hasNext()) {
            actual.append(it.next() + " ");
        }
        String expected = "35 20 6 2 1 ";
        assertEquals("NavigableSetTest.testDescendingIterator", expected, actual.toString());
    }

    private static void testCeiling() {
        NavigableSet<Integer> tree = new TreeSet<>();
        tree.add(20);
        tree.add(2);
        tree.add(1);
        tree.add(35);
        tree.add(6);
        tree.add(40);
        tree.add(21);
        assertEquals("NavigableSetTest.testCeilingMoreThanVal", (Integer) 20, tree.ceiling(15));
        assertEquals("NavigableSetTest.testCeilingEqualsVal", (Integer) 20, tree.ceiling(20));
        assertEquals("NavigableSetTest.testCeilingNull", null, tree.ceiling(50));
    }

    private static void testSubSet() {
        NavigableSet<String> tree = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
        tree.add("mo");
        tree.add("m");
        tree.add("MN");
        tree.add("mos");
        tree.add("Moscow");
        tree.add("mo n");
        tree.add("mr");
        tree.add("mp");
        NavigableSet<String> expected = new TreeSet<>();
        expected.add("mos");
        expected.add("mp");
        expected.add("Moscow");
        assertEquals("NavigableSetTest.testSubSet", expected, tree.subSet("mos", "mr"));
        SortedSet<String> expectedTwo = new TreeSet<>();
        expectedTwo.add("mr");
        expectedTwo.add("mp");
        expectedTwo.add("Moscow");
        assertEquals("NavigableSetTest.testSubSetIncludeVal", expectedTwo, tree.subSet("mos", false, "mr", true));
    }
}