package com.getjavajob.training.algo1712.makhovak.lesson03;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

public class DynamicArray {
    private Object[] dynamicArray;
    private int sizeDynamicArray;
    private int capacityDynamicArray;
    private int modificationsCount;

    public DynamicArray() {
        capacityDynamicArray = 10;
        dynamicArray = new Object[capacityDynamicArray];
    }

    public DynamicArray(int capacityDynamicArray) {
        if (capacityDynamicArray < 0) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(capacityDynamicArray));
        }
        dynamicArray = new Object[capacityDynamicArray];
        this.capacityDynamicArray = capacityDynamicArray;
    }

    /**
     * grow capacity in 1.5 times if array is full
     */
    private void growCapacityDynamicArray() {
        if (sizeDynamicArray == capacityDynamicArray) {
            int oldCapacityDynamicArray = capacityDynamicArray;
            capacityDynamicArray = oldCapacityDynamicArray + oldCapacityDynamicArray / 2;
            dynamicArray = Arrays.copyOf(dynamicArray, capacityDynamicArray);
        }
    }

    /**
     * adds element to the end. returns whether element was added. grow in 1.5 times if it is full.
     */
    public boolean add(Object e) {
        growCapacityDynamicArray();
        dynamicArray[sizeDynamicArray++] = e;
        modificationsCount++;
        return true;
    }

    public void add(int index, Object e) {
        handleIndicesRangeForAdd(index);
        growCapacityDynamicArray();
        System.arraycopy(dynamicArray, index, dynamicArray, index + 1, sizeDynamicArray - index);
        dynamicArray[index] = e;
        modificationsCount++;
        sizeDynamicArray++;
    }

    public Object set(int index, Object e) {
        handleIndicesRange(index);
        Object prevElement = dynamicArray[index];
        dynamicArray[index] = e;
        modificationsCount++;
        return prevElement;
    }

    public Object get(int index) {
        handleIndicesRange(index);
        return dynamicArray[index];
    }

    private void simpleRemove(int index) {
        if (index + 1 < sizeDynamicArray) {
            System.arraycopy(dynamicArray, index + 1, dynamicArray, index, sizeDynamicArray - index - 1);
        }
        dynamicArray[--sizeDynamicArray] = null;
        modificationsCount++;
    }

    public Object remove(int index) {
        handleIndicesRange(index);
        Object prevElement = dynamicArray[index];
        simpleRemove(index);
        return prevElement;
    }

    public boolean remove(Object e) {
        if (e == null) {
            for (int index = 0; index < dynamicArray.length; index++) {
                if (dynamicArray[index] == null) {
                    simpleRemove(index);
                    return true;
                }
            }
        } else {
            for (int index = 0; index < dynamicArray.length; index++) {
                if (e.equals(dynamicArray[index])) {
                    simpleRemove(index);
                    return true;
                }
            }
        }
        return false;
    }

    public int size() {
        return sizeDynamicArray;
    }

    public int capacity() {
        return dynamicArray.length;
    }

    public int indexOf(Object e) {
        if (e == null) {
            for (int index = 0; index < sizeDynamicArray; index++) {
                if (dynamicArray[index] == null) {
                    return index;
                }
            }
        } else {
            for (int index = 0; index < dynamicArray.length; index++) {
                if (e.equals(dynamicArray[index])) {
                    return index;
                }
            }
        }
        return -1;
    }

    public boolean contains(Object e) {
        return indexOf(e) >= 0;
    }


    public Object[] toArray() {
        return Arrays.copyOf(dynamicArray, sizeDynamicArray);
    }

    /**
     * throws runtime exception if index not is in range.
     */
    private void handleIndicesRange(int index) {
        if (index >= sizeDynamicArray || index < 0) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
    }

    /**
     * handleIndicesRange for add elements
     */
    private void handleIndicesRangeForAdd(int index) {
        if (index > sizeDynamicArray || index < 0) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
    }

    private String outOfBoundsMsg(int index) {
        return "IndexOutOfBoundsException Index: " + index + ", Size: " + sizeDynamicArray + " Available indexes: " +
                "from 0 to " + (sizeDynamicArray - 1);
    }

    public ListIterator listIterator() {
        return new ListIterator(0);
    }

    public ListIterator listIterator(int index) {
        if (index < 0 || index >= sizeDynamicArray) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
        return new ListIterator(index);
    }

    class ListIterator {
        private int currentIndex;
        private int iteratorModificationsCount = DynamicArray.this.modificationsCount;
        private int lastIndexReturned = -1;


        ListIterator(int index) {
            currentIndex = index;
        }

        public boolean hasNext() {
            return currentIndex != sizeDynamicArray;
        }

        public Object next() {
            checkForModification();
            if (currentIndex >= DynamicArray.this.sizeDynamicArray) {
                throw new NoSuchElementException("No such elements");
            }
            lastIndexReturned = currentIndex++;
            return DynamicArray.this.dynamicArray[lastIndexReturned];
        }

        public boolean hasPrevious() {
            return currentIndex - 1 >= 0;
        }

        public Object previous() {
            checkForModification();
            if (currentIndex - 1 < 0) {
                throw new NoSuchElementException("No such elements");
            }
            lastIndexReturned = --currentIndex;
            return DynamicArray.this.dynamicArray[lastIndexReturned];
        }

        public int nextIndex() {
            return currentIndex;
        }

        public int previousIndex() {
            return currentIndex - 1;
        }

        public void remove() {
            checkForModification();
            if (lastIndexReturned < 0) {
                throw new IllegalStateException("IllegalStateException: there are no returned elements");
            }
            DynamicArray.this.remove(lastIndexReturned);
            currentIndex--;
            lastIndexReturned = -1;
            iteratorModificationsCount = DynamicArray.this.modificationsCount;
        }

        public void set(Object e) {
            checkForModification();
            if (lastIndexReturned < 0) {
                throw new IllegalStateException("IllegalStateException: there are no returned elements");
            }
            DynamicArray.this.set(lastIndexReturned, e);
            iteratorModificationsCount = DynamicArray.this.modificationsCount;
        }

        public void add(Object e) {
            checkForModification();
            DynamicArray.this.add(currentIndex, e);
            currentIndex++;
            iteratorModificationsCount = DynamicArray.this.modificationsCount;
            lastIndexReturned = -1;
        }

        final void checkForModification() {
            if (iteratorModificationsCount != modificationsCount) {
                throw new ConcurrentModificationException("ConcurrentModificationException: the array was changed");
            }
        }
    }
}