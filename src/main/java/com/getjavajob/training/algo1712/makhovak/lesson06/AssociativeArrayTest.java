package com.getjavajob.training.algo1712.makhovak.lesson06;

import static com.getjavajob.training.util.Assert.assertEquals;

public class AssociativeArrayTest {
    public static void main(String[] args) {
        testAdd();
        testGet();
        testRemove();
        testAddKeyPoly();
        testAddKeyRandom();
    }

    /**
     * String "polygenelubricants" has hashCode==-2147483648;
     * This hashCode too large for Match.abs, and so that the result will be negative
     * in AA.add("polygenelubricant") must be applied Match.abs(key.hashCode() % array.length)
     * the result will be positive number
     */
    private static void testAddKeyPoly() {
        AssociativeArray<String, Integer> testAA = new AssociativeArray<>(15);
        testAA.add("pop", 2);
        testAA.add("one", 1);
        testAA.add("polygenelubricants", 4);
        assertEquals("AssociativeArrayTest.testAddKeyPoly", (Integer) 4, testAA.get("polygenelubricants"));
    }

    /**
     * String "random" has hashCode==--938285885;
     * This hashCode is negative
     * in AA.add("random") must be applied Match.abs(key.hashCode() % array.length)
     * the result will be positive number
     */
    private static void testAddKeyRandom() {
        AssociativeArray<String, Integer> testAA = new AssociativeArray<>(15);
        testAA.add("pop", 2);
        testAA.add("one", 1);
        testAA.add("random", 4);
        assertEquals("AssociativeArrayTest.testAddKeyRandom", (Integer) 4, testAA.get("random"));
    }

    private static void testRemove() {
        AssociativeArray<Integer, String> testAA = new AssociativeArray<>();
        testAA.add(0, "pop");
        testAA.add(1, "one");
        testAA.add(3, "three");
        testAA.add(4, "four");
        assertEquals("AssociativeArrayTest.testRemove", "three", testAA.remove(3));
        assertEquals("AssociativeArrayTest.testRemoveFalse", null, testAA.remove(3));
    }

    private static void testGet() {
        AssociativeArray<Integer, String> testAA = new AssociativeArray<>();
        testAA.add(0, "pop");
        testAA.add(1, "one");
        testAA.add(3, "three");
        testAA.add(4, "four");
        assertEquals("AssociativeArrayTest.testGet", "three", testAA.get(3));
        testAA.add(3, "new three");
        assertEquals("AssociativeArrayTest.testGetAfterOverwriting", "new three", testAA.get(3));
        assertEquals("AssociativeArrayTest.testGetFalse", null, testAA.get(6));
    }

    private static void testAdd() {
        AssociativeArray<Integer, String> testAA = new AssociativeArray<>();
        testAA.add(0, "pop");
        testAA.add(1, "one");
        testAA.add(3, "three");
        testAA.add(4, "four");
        testAA.add(5, "five");
        testAA.add(6, "six");
        testAA.add(7, "seven");
        testAA.add(8, "eight");
        testAA.add(16, "sixteen");
        assertEquals("AssociativeArrayTest.testAddNewElement", null, testAA.add(2, "two"));
        assertEquals("AssociativeArrayTest.testAddElementWithExistingKey", "one", testAA.add(1, "new one"));
        testAA.add(17, "09");
        assertEquals("LLStackTest.testAddWithExistingIndex", 11, testAA.getSize());
        testAA.add(9, "nine");
        testAA.add(10, "ten");
        assertEquals("LLStackTest.testAddCapacityAfterResize", 32, testAA.getCapacity());
    }
}