package com.getjavajob.training.algo1712.makhovak.lesson05;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class LinkedListQueue<V> extends AbstractQueue<V> implements Queue<V> {

    private Node<V> front;
    private Node<V> back;
    private int size;

    public int size() {
        return size;
    }

    public boolean add(V val) {
        Node<V> e = new Node<>(null, val);
        if (front == null) {
            front = back = e;

        } else {
            back.next = e;
            back = e;
        }
        size++;
        return true;
    }

    public V remove() {
        if (front == null) {
            throw new NullPointerException("There are not elements");
        }
        V returned = front.val;
        Node<V> deleted = front;
        front = front.next;
        deleted.val = null;
        deleted.next = null;
        size--;
        return returned;
    }

    public List<V> asList() {
        List<V> a = new LinkedList<>();
        Node<V> element = front;
        for (int i = 0; i < size; i++) {
            a.add(0, element.val);
            element = element.next;
        }
        return a;
    }

    private static class Node<V> {
        private Node<V> next;
        private V val;

        Node(Node<V> next, V element) {
            this.val = element;
            this.next = next;
        }
    }
}