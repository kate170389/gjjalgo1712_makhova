package com.getjavajob.training.algo1712.makhovak.lesson01;

public class Task7 {
    public long var1;
    public long var2;

    Task7(long var1, long var2) {
        this.var1 = var1;
        this.var2 = var2;
    }

    public void swapTwoVarsBitwiseSolution1() {
        var1 ^= var2;
        var2 ^= var1;
        var1 ^= var2;
    }

    public void swapTwoVarsBitwiseSolution2() {
        var1 <<= 32;
        var2 <<= 32;
        var2 >>>= 32;
        var1 = var1 | var2;
        var2 = var1 >> 32;
        var1 <<= 32;
        var1 >>= 32;
    }

    public void swapTwoVarsArithmeticSolution1() {
        var1 += var2;
        var2 = var1 - var2;
        var1 -= var2;
    }

    public void swapTwoVarsArithmeticSolution2() {
        var1 *= var2;
        var2 = var1 / var2;
        var1 /= var2;
    }
}