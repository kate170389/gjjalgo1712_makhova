package com.getjavajob.training.algo1712.makhovak.lesson04;

import java.util.*;

class DynamicArray<V> extends AbstractListDLL<V> implements List<V> {
    private V[] dynamicArray;
    private int sizeDynamicArray;
    private int capacityDynamicArray;
    private int modificationsCount;

    public DynamicArray() {
        capacityDynamicArray = 10;
        dynamicArray = (V[]) new Object[capacityDynamicArray];
    }

    public DynamicArray(int capacityDynamicArray) {
        if (capacityDynamicArray < 0) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(capacityDynamicArray));
        }
        dynamicArray = (V[]) new Object[capacityDynamicArray];
        this.capacityDynamicArray = capacityDynamicArray;
    }

    /**
     * grow capacity in 1.5 times if array is full
     */
    private void growCapacityDynamicArray() {
        if (sizeDynamicArray == capacityDynamicArray) {
            int oldCapacityDynamicArray = capacityDynamicArray;
            capacityDynamicArray = oldCapacityDynamicArray + oldCapacityDynamicArray / 2;
            dynamicArray = Arrays.copyOf(dynamicArray, capacityDynamicArray);
        }
    }

    /**
     * adds element to the end. returns whether element was added. grow in 1.5 times if it is full.
     */
    public boolean add(V e) {
        growCapacityDynamicArray();
        dynamicArray[sizeDynamicArray++] = e;
        modificationsCount++;
        return true;
    }

    public void add(int index, V e) {
        handleIndicesRangeForAdd(index);
        growCapacityDynamicArray();
        System.arraycopy(dynamicArray, index, dynamicArray, index + 1, sizeDynamicArray - index);
        dynamicArray[index] = e;
        modificationsCount++;
        sizeDynamicArray++;
    }

    public V set(int index, V e) {
        handleIndicesRange(index);
        V prevElement = dynamicArray[index];
        dynamicArray[index] = e;
        modificationsCount++;
        return prevElement;
    }

    public V get(int index) {
        handleIndicesRange(index);
        return dynamicArray[index];
    }

    private void simpleRemove(int index) {
        if (index + 1 < sizeDynamicArray) {
            System.arraycopy(dynamicArray, index + 1, dynamicArray, index, sizeDynamicArray - index - 1);
        }
        dynamicArray[--sizeDynamicArray] = null;
        modificationsCount++;
    }

    public V remove(int index) {
        handleIndicesRange(index);
        V prevElement = dynamicArray[index];
        simpleRemove(index);
        return prevElement;
    }

    public boolean remove(Object e) {
        if (e == null) {
            for (int index = 0; index < dynamicArray.length; index++) {
                if (dynamicArray[index] == null) {
                    simpleRemove(index);
                    return true;
                }
            }
        } else {
            for (int index = 0; index < dynamicArray.length; index++) {
                if (e.equals(dynamicArray[index])) {
                    simpleRemove(index);
                    return true;
                }
            }
        }
        return false;
    }

    public int size() {
        return sizeDynamicArray;
    }

    public int capacity() {
        return dynamicArray.length;
    }

    public int indexOf(Object e) {
        if (e == null) {
            for (int index = 0; index < sizeDynamicArray; index++) {
                if (dynamicArray[index] == null) {
                    return index;
                }
            }
        } else {
            for (int index = 0; index < dynamicArray.length; index++) {
                if (e.equals(dynamicArray[index])) {
                    return index;
                }
            }
        }
        return -1;
    }

    public boolean contains(Object e) {
        return indexOf(e) >= 0;
    }


    public Object[] toArray() {
        return Arrays.copyOf(dynamicArray, sizeDynamicArray);
    }

    /**
     * throws runtime exception if index not is in range.  This method does *not* check if the index is
     */
    private void handleIndicesRange(int index) {
        if (index >= sizeDynamicArray || index < 0) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
    }

    /**
     * handleIndicesRange for add elements
     */
    private void handleIndicesRangeForAdd(int index) {
        if (index > sizeDynamicArray || index < 0) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
    }

    /**
     * return the message about error
     */
    private String outOfBoundsMsg(int index) {
        return "IndexOutOfBoundsException Index: " + index + ", Size: " + sizeDynamicArray + " Available indexes: " +
                "from 0 to " + (sizeDynamicArray - 1);
    }

    public ListIteratorImpl listIterator() {
        return new ListIteratorImpl(0);
    }

    public ListIteratorImpl listIterator(int index) {
        if (index < 0 || index >= sizeDynamicArray) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
        return new ListIteratorImpl(index);
    }

    private class ListIteratorImpl implements ListIterator<V> {
        private int currentIndex;
        private int iteratorModificationsCount = DynamicArray.this.modificationsCount;
        private int lastIndexReturned = -1;


        ListIteratorImpl(int index) {
            currentIndex = index;
        }

        public boolean hasNext() {
            return currentIndex != sizeDynamicArray;
        }

        public V next() {
            checkForModification();
            if (currentIndex >= DynamicArray.this.sizeDynamicArray) {
                throw new NoSuchElementException("No such elements");
            }
            lastIndexReturned = currentIndex++;
            return DynamicArray.this.dynamicArray[lastIndexReturned];
        }

        public boolean hasPrevious() {
            return currentIndex - 1 >= 0;
        }

        public V previous() {
            checkForModification();
            if (currentIndex - 1 < 0) {
                throw new NoSuchElementException("No such elements");
            }
            lastIndexReturned = --currentIndex;
            return DynamicArray.this.dynamicArray[lastIndexReturned];
        }

        public int nextIndex() {
            return currentIndex;
        }

        public int previousIndex() {
            return currentIndex - 1;
        }

        public void remove() {
            checkForModification();
            if (lastIndexReturned < 0) {
                throw new IllegalStateException("IllegalStateException: there are no returned elements");
            }
            DynamicArray.this.remove(lastIndexReturned);
            currentIndex--;
            lastIndexReturned = -1;
            iteratorModificationsCount = DynamicArray.this.modificationsCount;
        }

        public void set(V e) {
            checkForModification();
            if (lastIndexReturned < 0) {
                throw new IllegalStateException("IllegalStateException: there are no returned elements");
            }
            DynamicArray.this.set(lastIndexReturned, e);
            iteratorModificationsCount = DynamicArray.this.modificationsCount;
        }

        public void add(V e) {
            checkForModification();
            DynamicArray.this.add(currentIndex, e);
            currentIndex++;
            iteratorModificationsCount = DynamicArray.this.modificationsCount;
            lastIndexReturned = -1;
        }

        final void checkForModification() {
            if (iteratorModificationsCount != modificationsCount) {
                throw new ConcurrentModificationException("ConcurrentModificationException: the array was changed");
            }
        }
    }
}