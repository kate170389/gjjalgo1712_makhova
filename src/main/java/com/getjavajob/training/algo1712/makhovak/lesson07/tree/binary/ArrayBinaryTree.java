package com.getjavajob.training.algo1712.makhovak.lesson07.tree.binary;

import com.getjavajob.training.algo1712.makhovak.lesson07.tree.Node;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

/**
 * Concrete implementation of a binary tree using a node-based, array structure
 *
 * @param <E> element
 */
public class ArrayBinaryTree<E> extends AbstractBinaryTree<E> {

    private int size;
    private NodeImpl<E>[] arrayBinaryTree = new NodeImpl[1];
    private int h;


    protected NodeImpl<E> validate(Node<E> n) throws IllegalArgumentException {
        if (n == null || !(n instanceof NodeImpl)) {
            throw new IllegalArgumentException("IllegalArgumentException: the node is not an instance of supported");
        }
        return (NodeImpl<E>) n;
    }

    public int indexOf(Node<E> node) {
        for (int index = 0; index < arrayBinaryTree.length; index++) {
            if (node == arrayBinaryTree[index]) {
                return index;
            }
        }
        return -1;
    }

    private void growCapacityArray() {
        int newCapacity = arrayBinaryTree.length + (2 << h);
        arrayBinaryTree = Arrays.copyOf(arrayBinaryTree, newCapacity);
        h++;
    }

    @Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        NodeImpl<E> node = validate(p);
        return arrayBinaryTree[indexLeft(node)];
    }

    @Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        NodeImpl<E> node = validate(p);
        return arrayBinaryTree[indexRight(node)];
    }

    private int indexLeft(Node<E> p) {
        return indexOf(p) * 2 + 1;
    }

    private int indexRight(Node<E> p) {
        return indexOf(p) * 2 + 2;
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        if (left(node) == null) {
            Node<E> leftNode = arrayBinaryTree[indexLeft(node)] = new NodeImpl<>(e);
            size++;
            if (indexLeft(leftNode) >= arrayBinaryTree.length) {
                growCapacityArray();
            }
            return leftNode;
        } else {
            throw new IllegalArgumentException("node already has a left child");
        }
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        if (right(node) == null) {
            Node<E> rightNode = arrayBinaryTree[indexRight(node)] = new NodeImpl<>(e);
            size++;
            if (indexLeft(rightNode) >= arrayBinaryTree.length) {
                growCapacityArray();
            }
            return rightNode;
        } else {
            throw new IllegalArgumentException("node already has a right child");
        }
    }

    @Override
    public Node<E> root() {
        return arrayBinaryTree[0];
    }

    @Override
    public Node<E> parent(Node<E> n) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        if (isRoot(node)) {
            return null;
        }
        return arrayBinaryTree[(indexOf(node) - 1) / 2];
    }

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        if (!isEmpty()) {
            throw new IllegalStateException("IllegalStateException: root already exists");
        }
        arrayBinaryTree[0] = new NodeImpl<>(e);
        growCapacityArray();
        size++;
        return arrayBinaryTree[0];
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        if (left(node) == null) {
            return addLeft(node, e);
        }
        if (right(node) == null) {
            return addRight(node, e);
        }
        return null;
    }

    @Override
    public E set(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        E prev = node.element;
        node.element = e;
        return prev;
    }

    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        if (isInternal(n)) {
            return null;
        }
        E prev = n.getElement();
        if (isRoot(n)) {
            arrayBinaryTree[0] = null;
            return prev;
        }
        if (childrenNumber(parent(n)) == 1) {
            int newCapacity = arrayBinaryTree.length - (int) Math.pow(2, h);
            arrayBinaryTree = Arrays.copyOf(arrayBinaryTree, newCapacity);
            h--;
        }
        arrayBinaryTree[indexOf(n)] = null;
        size--;
        return prev;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<E> iterator() {
        return super.iterator();
    }

    /**
     * @return an iterable collection of nodes of the tree in pre-order
     */
    @Override
    public Collection<Node<E>> nodes() {
        return preOrder();
    }

    protected static class NodeImpl<E> implements Node<E> {
        private E element;

        public NodeImpl(E element) {
            this.element = element;
        }

        @Override
        public E getElement() {
            return element;
        }

        public String toString() {
            return element.toString();
        }
    }
}