package com.getjavajob.training.algo1712.makhovak.lesson09;

import java.util.Arrays;
import java.util.NavigableSet;
import java.util.TreeSet;

public class CityStorage {
    private NavigableSet<String> tree = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);

    public void add(String val) {
        tree.add(val);
    }

    public void add(String[] val) {
        tree.addAll(Arrays.asList(val));
    }

    public NavigableSet<String> rangeOfCities(String inputString, String[] cities) {
        int index = inputString.length() - 1;
        char c = inputString.charAt(index);
        String condition;
        if (c == Character.MAX_VALUE) {
            if (inputString.charAt(0) == Character.MAX_VALUE) {
                return tree.tailSet(inputString, true);
            } else {
                while (c == Character.MAX_VALUE) {
                    index--;
                    c = inputString.charAt(index);
                }
                c++;
                condition = inputString.substring(0, index) + c;
            }
        } else {
            c++;
            condition = inputString.substring(0, index) + c;
        }
        return tree.subSet(inputString, true, condition, false);
    }

    public NavigableSet<String> getTree() {
        return tree;
    }
}