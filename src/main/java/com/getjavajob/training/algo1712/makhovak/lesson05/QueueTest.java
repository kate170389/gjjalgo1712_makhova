package com.getjavajob.training.algo1712.makhovak.lesson05;

import java.util.ArrayDeque;
import java.util.Queue;

import static com.getjavajob.training.util.Assert.assertEquals;

public class QueueTest {
    public static void main(String[] args) {
        testAdd();
        testElement();
        testOffer();
        testPeek();
        testPoll();
        testRemove();
    }

    private static void testRemove() {
        Queue<String> queue = new ArrayDeque<>();
        queue.add("one");
        queue.add("two");
        queue.add("three");
        String[] array = {"two", "three"};
        assertEquals("QueueTest.testRemove", "one", queue.remove());
        assertEquals("QueueTest.testRemove", array, queue.toArray());
    }

    private static void testPoll() {
        Queue<String> queue = new ArrayDeque<>();
        queue.add("one");
        queue.add("two");
        queue.add("three");
        Queue<String> test = new ArrayDeque<>();
        String[] array = {"two", "three"};
        assertEquals("QueueTest.testPoll", "one", queue.poll());
        assertEquals("QueueTest.testPollNull", null, test.poll());
        assertEquals("QueueTest.testPoll", array, queue.toArray());
    }

    private static void testPeek() {
        Queue<String> queue = new ArrayDeque<>();
        queue.add("one");
        queue.add("two");
        queue.add("three");
        Queue<String> test = new ArrayDeque<>();
        assertEquals("QueueTest.testPeek", "one", queue.peek());
        assertEquals("QueueTest.testPeekNull", null, test.peek());
    }

    private static void testOffer() {
        Queue<String> queue = new ArrayDeque<>();
        queue.add("one");
        queue.add("two");
        queue.add("three");
        String[] test = {"one", "two", "three", "four"};
        assertEquals("QueueTest.testOfferTrue", true, queue.offer("four"));
        assertEquals("QueueTest.testOffer", test, queue.toArray());
    }

    private static void testElement() {
        Queue<String> queue = new ArrayDeque<>();
        queue.add("one");
        queue.add("two");
        queue.add("three");
        String[] test = {"one", "two", "three"};
        assertEquals("QueueTest.testElement", "one", queue.element());
        assertEquals("QueueTest.testElement", test, queue.toArray());
    }

    private static void testAdd() {
        Queue<String> queue = new ArrayDeque<>();
        queue.add("one");
        queue.add("two");
        assertEquals("QueueTest.testAddTrue", true, queue.add("three"));
        String[] test = {"one", "two", "three"};
        assertEquals("QueueTest.testElement", test, queue.toArray());
    }
}
