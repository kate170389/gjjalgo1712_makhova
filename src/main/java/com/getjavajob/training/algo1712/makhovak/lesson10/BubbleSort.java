package com.getjavajob.training.algo1712.makhovak.lesson10;

public class BubbleSort {
    public static void sort(int[] array) {
        int n = array.length;
        boolean swapper = true;
        while (swapper) {
            swapper = false;
            for (int i = 1; i < n; i++) {
                if (array[i - 1] > array[i]) {
                    int c = array[i - 1];
                    array[i - 1] = array[i];
                    array[i] = c;
                    swapper = true;
                }
            }
            n--;
        }
    }
}