package com.getjavajob.training.algo1712.makhovak.lesson01;

import static com.getjavajob.training.util.Assert.assertEquals;

public class Task7Test {
    public static void main(String[] args) {
        testSwapTwoVarsBitwiseSolution1();
        testSwapTwoVarsBitwiseSolution2();
        testSwapTwoVarsArithmeticSolution1();
        testSwapTwoVarsArithmeticSolution2();
    }

    public static void testSwapTwoVarsBitwiseSolution1() {
        Task7 test = new Task7(34, -3);
        test.swapTwoVarsBitwiseSolution1();
        assertEquals("Task7Test.testSwapTwoVarsBitwiseSolution1.var1", -3, test.var1);
        assertEquals("Task7Test.testSwapTwoVarsBitwiseSolution1.var2", 34, test.var2);
    }

    public static void testSwapTwoVarsBitwiseSolution2() {
        Task7 test = new Task7(6, 18);
        test.swapTwoVarsBitwiseSolution2();
        assertEquals("Task7Test.testSwapTwoVarsBitwiseSolution2.var1", 18, test.var1);
        assertEquals("Task7Test.testSwapTwoVarsBitwiseSolution2.var2", 6, test.var2);
    }

    public static void testSwapTwoVarsArithmeticSolution1() {
        Task7 test = new Task7(34, 89);
        test.swapTwoVarsArithmeticSolution1();
        assertEquals("Task7Test.testSwapTwoVarsArithmeticSolution1.var1", 89, test.var1);
        assertEquals("Task7Test.testSwapTwoVarsArithmeticSolution1.var2", 34, test.var2);
    }

    public static void testSwapTwoVarsArithmeticSolution2() {
        Task7 test = new Task7(-34, 89);
        test.swapTwoVarsArithmeticSolution2();
        assertEquals("Task7Test.testSwapTwoVarsArithmeticSolution2.var1", 89, test.var1);
        assertEquals("Task7Test.testSwapTwoVarsArithmeticSolution2.var2", -34, test.var2);
    }
}